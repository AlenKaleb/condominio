var LIBS = {
	// Chart libraries
	plot: [
		"../painel/libs/misc/flot/jquery.flot.min.js",
		"../painel/libs/misc/flot/jquery.flot.pie.min.js",
		"../painel/libs/misc/flot/jquery.flot.stack.min.js",
		"../painel/libs/misc/flot/jquery.flot.resize.min.js",
		"../painel/libs/misc/flot/jquery.flot.curvedLines.js",
		"../painel/libs/misc/flot/jquery.flot.tooltip.min.js",
		"../painel/libs/misc/flot/jquery.flot.categories.min.js"
	],
	chart: [
		'../painel/libs/misc/echarts/build/dist/echarts-all.js',
		'../painel/libs/misc/echarts/build/dist/theme.js',
		'../painel/libs/misc/echarts/build/dist/jquery.echarts.js'
	],
	circleProgress: [
		"../painel/libs/bower/jquery-circle-progress/dist/circle-progress.js"
	],
	sparkline: [
		"../painel/libs/misc/jquery.sparkline.min.js"
	],
	maxlength: [
		"../painel/libs/bower/bootstrap-maxlength/src/bootstrap-maxlength.js"
	],
	tagsinput: [
		"../painel/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
		"../painel/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
	],
	TouchSpin: [
		"../painel/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
		"../painel/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
	],
	selectpicker: [
		"../painel/libs/bower/bootstrap-select/dist/css/bootstrap-select.min.css",
		"../painel/libs/bower/bootstrap-select/dist/js/bootstrap-select.min.js"
	],
	filestyle: [
		"../painel/libs/bower/bootstrap-filestyle/src/bootstrap-filestyle.min.js"
	],
	timepicker: [
		"../painel/libs/bower/bootstrap-timepicker/js/bootstrap-timepicker.js"
	],
	datetimepicker: [
		"../painel/libs/bower/moment/moment.js",
		"../painel/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
		"../painel/libs/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"
	],
	select2: [
		"../painel/libs/bower/select2/dist/css/select2.min.css",
		"../painel/libs/bower/select2/dist/js/select2.full.min.js"
	],
	vectorMap: [
		"../painel/libs/misc/jvectormap/jquery-jvectormap.css",
		"../painel/libs/misc/jvectormap/jquery-jvectormap.min.js",
		"../painel/libs/misc/jvectormap/maps/jquery-jvectormap-us-mill.js",
		"../painel/libs/misc/jvectormap/maps/jquery-jvectormap-world-mill.js",
		"../painel/libs/misc/jvectormap/maps/jquery-jvectormap-africa-mill.js"
	],
	summernote: [
		"../painel/libs/bower/summernote/dist/summernote.css",
		"../painel/libs/bower/summernote/dist/summernote.min.js"
	],
	DataTable: [
		"../painel/libs/misc/datatables/datatables.min.css",
		"../painel/libs/misc/datatables/datatables.min.js"
	],
	fullCalendar: [
		"../painel/libs/bower/moment/moment.js",
		"../painel/libs/bower/fullcalendar/dist/fullcalendar.min.css",
		"../painel/libs/bower/fullcalendar/dist/fullcalendar.min.js"
	],
	dropzone: [
		"../painel/libs/bower/dropzone/dist/min/dropzone.min.css",
		"../painel/libs/bower/dropzone/dist/min/dropzone.min.js"
	],
	counterUp: [
		"../painel/libs/bower/waypoints/lib/jquery.waypoints.min.js",
		"../painel/libs/bower/counterup/jquery.counterup.min.js"
	],
	others: [
		"../painel/libs/bower/switchery/dist/switchery.min.css",
		"../painel/libs/bower/switchery/dist/switchery.min.js",
		"../painel/libs/bower/lightbox2/dist/css/lightbox.min.css",
		"../painel/libs/bower/lightbox2/dist/js/lightbox.min.js"
	]
};