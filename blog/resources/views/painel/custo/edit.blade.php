@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Edição de Custo</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações da <code>custo</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formCusto" class="form-horizontal" method="post" action="{{ url("custo/update/{$custo->id}") }}">
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option {{ ($condominio->id == $custo->condominio_id?"selected":null) }} value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="dataRegistro" class="col-sm-3 control-label">Data de Registro:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" id="dataRegistro" name="dataRegistro" value="{{ $dataRegistro }}" readonly="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Nome:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome" value="{{ $custo->nome }}" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="categoria" class="col-sm-3 control-label">Categoria:</label>
                            <div class="col-sm-4">
                                <select id="categoria" name="categoria" class="form-control" >
                                    <option selected value="">Selecione uma categoria</option>
                                    <option {{ ($custo->categoria == 'F'?"selected":null) }} value="F">Fixo</option>
                                    <option {{ ($custo->categoria == 'V'?"selected":null) }} value="V">Variavel</option>
                                    <option {{ ($custo->categoria == 'I'?"selected":null) }} value="I">Impostos</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div id="divItens">
                            <div id="divItem">
                                <div class="col-sm-12">
                                    <div class="panel panel-inverse">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Item Custo</h4>
                                        </div>

                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label id="labelNomeItem" for="nomeItem" class="col-sm-2 control-label">Nome:</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="nomeItem" name="nomeItem[]" placeholder="Informe o nome" maxlength="100">
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-5">
                                                    <label id="labelNumeroDocumentoItem" for="numeroDocumentoItem" class="col-sm-4 control-label">Num do doc:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="numeroDocumentoItem" name="numeroDocumentoItem[]" placeholder="Informe o numero do documento" maxlength="20">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <label id="labelValorItem" for="valorItem" class="col-sm-3 control-label">Valor:</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="valorItem" name="valorItem[]" placeholder="Informe o valor" maxlength="13">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label id="labelDataVencimentoItem" for="dataVencimentoItem" class="col-sm-3 control-label">Data de Vencimento:</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="ace-icon fa fa-calendar"></i>
                                                            </span>
                                                        <input type="text" class="form-control" id="dataVencimentoItem" name="dataVencimentoItem[]" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label id="labelDataPagamentoItem" for="dataPagamentoItem" class="col-sm-3 control-label">Data de Pagamento:</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="ace-icon fa fa-calendar"></i>
                                                            </span>
                                                        <input type="text" class="form-control" id="dataPagamentoItem" name="dataPagamentoItem[]" value="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label id="labelFormaPagamentoItem" for="formaPagamentoItem" class="col-sm-3 control-label">Forma de Pagamento:</label>
                                                <div class="col-xs-3">
                                                    <div class="radio radio-primary">
                                                        <input name="formaPagamentoItem[]" class="formaPagamento" type="radio" id="dinheiroItem" value="D">
                                                        <label id="labelDinheiroItem" for="dinheiroItem">Dinheiro</label>
                                                    </div>
                                                    <div class="radio radio-info">
                                                        <input name="formaPagamentoItem[]" class="formaPagamento" type="radio" id="cartaoDebitoItem" value="CD">
                                                        <label id="labelCartaoDebitoItem" for="cartaoDebitoItem">Cartão de debito</label>
                                                    </div>
                                                    <div class="radio radio-info">
                                                        <input name="formaPagamentoItem[]" class="formaPagamento" type="radio" id="boletoItem" value="B">
                                                        <label id="labelBoletoItem" for="boletoItem">Boleto</label>
                                                    </div>
                                                    <div class="radio radio-info">
                                                        <input name="formaPagamentoItem[]" class="formaPagamento" type="radio" id="debitoContaItem" value="DB">
                                                        <label id="labelDebitoContaItem" for="debitoContaItem">Débito na conta</label>
                                                    </div>
                                                    <div class="radio radio-info">
                                                        <input name="formaPagamentoItem[]" class="formaPagamento" type="radio" id="transferenciaItem" value="T">
                                                        <label id="labelTransferenciaItem" for="</label>">Transferência</label>
                                                    </div>
                                                </div><!-- .col -->
                                            </div><!-- .form-group -->

                                            <div class="form-group">
                                                <label id="labelObservacaoItem" for="observacaoItem" class="col-sm-3 control-label">Observação:</label>
                                                <div class="col-sm-6">
                                                    <textarea class="form-control" rows="5" id="observacaoItem" name="observacaoItem[]" placeholder="Informe a observação" maxlength="300"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4 col-sm-offset-3">
                                                    <button id="removerItem" name="removerItem[]" type="button" class="btn btn-danger removerItem">Remover</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- END column -->
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="panel panel-inverse">
                                <div class="panel-footer">
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-3">
                                            <button id="adicionarItem" name="adicionarItem[]" type="button" class="btn btn-info">Adicionar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="observacao" class="col-sm-3 control-label">Observação:</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" id="observacao" name="observacao" placeholder="Informe a observação"><?= $custo->observacao ?></textarea>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskMoney.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            carregarItens();
            $('select').select2();

            function carregarItens(){
                var count = -1;
                $('#divItem').prop('hidden',true);

                @foreach($itensCusto as $item)
                    <?php
                        if(!empty($item->observacao)):
                            $textoObservacao = nl2br($item->observacao);
                            $textoObservacao = str_replace('<br />','\\',$textoObservacao);
                        else:
                            $textoObservacao = null;
                        endif;

                        if(!empty($item->data_vencimento)):
                            $dataVencimentoArr = explode('-',$item->data_vencimento);
                            $dataVencimento = "{$dataVencimentoArr[2]}/{$dataVencimentoArr[1]}/{$dataVencimentoArr[0]}";
                        endif;

                        if(!empty($item->data_pagamento)):
                            $dataPagamentoArr = explode('-',$item->data_pagamento);
                            $dataPagamento = "{$dataPagamentoArr[2]}/{$dataPagamentoArr[1]}/{$dataPagamentoArr[0]}";
                        endif;

                         if(!empty($item->valor)):
                            $valor = number_format($item->valor,2,',','.');
                        else:
                            $valor = null;
                        endif;
                    ?>
                    adicionarItem("{{ $item->nome }}","{{ $item->numero_documento }}","{{ $valor }}","{{ $dataVencimento }}","{{ $dataPagamento }}","{{ $item->forma_pagamento }}","<?= $textoObservacao ?>");
                @endforeach

                function adicionarItem(nome,numeroDocumento,valor,dataVencimento,dataPagamento,formaPagamento,observacao){
                    count += 1;
                    var divItem = $("#divItem").clone().removeAttr('id').attr('id','divItem'+count).prop('hidden',false);
                    divItem.find("label[id='labelNomeItem']").removeAttr('id').removeAttr('for').attr('id','labelNomeItem' + count).attr('for','nomeItem'+count);
                    divItem.find("input[id='nomeItem']").removeAttr('id').removeAttr('name').attr('id','nomeItem' + count).attr('name','nomeItem['+count+']').attr('maxlength',100).val(nome).prop('required',true).attr('data-msg-required','Ops! Nome Obrigatório');
                    divItem.find("label[id='labelNumeroDocumentoItem']").removeAttr('id').removeAttr('for').attr('id','labelNumeroDocumentoItem' + count).attr('for','numeroDocumentoItem'+count);
                    divItem.find("input[id='numeroDocumentoItem']").removeAttr('id').removeAttr('name').attr('id','numeroDocumentoItem' + count).attr('name','numeroDocumentoItem['+count+']').attr('maxlength',20).prop('required',true).attr('data-msg-required','Ops! Número Obrigatório').val(numeroDocumento);
                    divItem.find("label[id='labelValorItem']").removeAttr('id').removeAttr('for').attr('id','labelValorItem' + count).attr('for','valorItem'+count);
                    divItem.find("input[id='valorItem']").removeAttr('id').removeAttr('name').attr('id','valorItem' + count).attr('name','valorItem['+count+']').attr('maxlength',13).maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true}).prop('required',true).attr('data-msg-required','Ops! Valor Obrigatório').val(valor);
                    divItem.find("label[id='labelDataVencimentoItem']").removeAttr('id').removeAttr('for').attr('id','labelDataVencimentoItem' + count).attr('for','dataVencimentoItem'+count);
                    divItem.find("input[id='dataVencimentoItem']").removeAttr('id').removeAttr('name').attr('id','dataVencimentoItem' + count).attr('name','dataVencimentoItem['+count+']').attr('maxlength',10).mask('99/99/9999').datepicker({
                        format: 'dd/mm/yyyy',
                        language: 'pt-BR'
                    }).prop('required',true).attr('data-msg-required','Ops! Data de Vencimento Obrigatório').val(dataVencimento);
                    divItem.find("label[id='labelDataPagamentoItem']").removeAttr('id').removeAttr('for').attr('id','labelDataPagamentoItem' + count).attr('for','dataPagamentoItem'+count);
                    divItem.find("input[id='dataPagamentoItem']").removeAttr('id').removeAttr('name').attr('id','dataPagamentoItem' + count).attr('name','dataPagamentoItem['+count+']').attr('maxlength',10).mask('99/99/9999').datepicker({
                        format: 'dd/mm/yyyy',
                        language: 'pt-BR'
                    }).val(dataPagamento);
                    divItem.find("label[id='labelFormaPagamentoItem']").removeAttr('id').removeAttr('for').attr('id','labelFormaPagamentoItem' + count).attr('for','formaPagamentoItem'+count);
                    divItem.find("label[id='labelDinheiroItem']").removeAttr('id').removeAttr('for').attr('id','labelDinheiroItem' + count).attr('for','dinheiroItem'+count);
                    divItem.find("input[id='dinheiroItem']").removeAttr('id').removeAttr('name').attr('id','dinheiroItem' + count).attr('name','formaPagamentoItem['+count+']');
                    divItem.find("label[id='labelCartaoDebitoItem']").removeAttr('id').removeAttr('for').attr('id','labelCartaoDebitoItem' + count).attr('for','cartaoDebitoItem'+count);
                    divItem.find("input[id='cartaoDebitoItem']").removeAttr('id').removeAttr('name').attr('id','cartaoDebitoItem' + count).attr('name','formaPagamentoItem['+count+']');
                    divItem.find("label[id='labelBoletoItem']").removeAttr('id').removeAttr('for').attr('id','labelBoletoItem' + count).attr('for','boletoItem'+count);
                    divItem.find("input[id='boletoItem']").removeAttr('id').removeAttr('name').attr('id','boletoItem' + count).attr('name','formaPagamentoItem['+count+']');
                    divItem.find("label[id='labelDebitoContaItem']").removeAttr('id').removeAttr('for').attr('id','labelDebitoContaItem' + count).attr('for','debitoContaItem'+count);
                    divItem.find("input[id='debitoContaItem']").removeAttr('id').removeAttr('name').attr('id','debitoContaItem' + count).attr('name','formaPagamentoItem['+count+']');
                    divItem.find("label[id='labelTransferenciaItem']").removeAttr('id').removeAttr('for').attr('id','labelTransferenciaItem' + count).attr('for','transferenciaItem'+count);
                    divItem.find("input[id='transferenciaItem']").removeAttr('id').removeAttr('name').attr('id','transferenciaItem' + count).attr('name','formaPagamentoItem['+count+']');
                    divItem.find("label[id='labelObservacaoItem']").removeAttr('id').removeAttr('for').attr('id','labelObservacaoItem' + count).attr('for','observacaoItem['+count+']');
                    divItem.find("textarea[id='observacaoItem']").removeAttr('id').removeAttr('name').attr('id','observacaoItem' + count).attr('name','observacaoItem['+count+']').attr('maxlength',300).val(observacao);
                    divItem.find("button[id='removerItem']").removeAttr('id').removeAttr('name').attr('id','removerItem-' + count).attr('name','removerItem['+count+']');
                    divItem.find("input[name='formaPagamentoItem["+count+"]']").prop('required',true).attr('data-msg-required','Ops! Forma de Pagamento Obrigatório').removeAttr('class').attr('class','formaPagamento'+count);
                    $("#divItens").append(divItem);

                    $(".formaPagamento"+count).change(function(){
                        if(formaPagamento == this.value){
                            $(this).prop('checked',true);
                        }
                    }).change();

                    $(".removerItem").click(function(){
                        var count = this.id.split('-');
                        removerItem(count[1]);
                    });

                }

                function removerItem(count){
                    $("#divItem" + count).remove();
                }

                $("#adicionarItem").click(function(){
                    adicionarItem();
                });
            }

            $('#formCusto').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    condominio: {
                        required: true
                    },
                    nome: {
                        required: true,
                        maxlength: 100
                    },
                    categoria: {
                        required: true
                    },
                    observacao: {
                        required: true,
                        maxlength: 300
                    }
                },
                messages: {
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    nome: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    categoria: {
                        required: "Ops! Campo Obrigatório"
                    },
                    observacao: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection