@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Custos</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>custo</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Custo <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('custo/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Novo Custo</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>Custo</th>
                                        <th>Data de Registro</th>
                                        <th>Valor</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('custo/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="custo" name="custo" placeholder="Custo" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'custo',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="data_registro" name="data_registro" placeholder="Data de Registro" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="valor" name="valor" placeholder="Valor" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($custos as $custo)
                                        <?php
                                            if($custo->categoria == 'F'):
                                                $categoria = "Fixo";
                                            elseif($custo->categoria == 'V'):
                                                $categoria = "Variavel";
                                            elseif($custo->categoria == 'I'):
                                                $categoria = "Impostos";
                                            endif;

                                            if(!empty($custo->data_registro)):
                                                $dataRegistroArr = explode('-',$custo->data_registro);
                                                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                            else:
                                                $dataRegistro = "Data não identificada!";
                                            endif;
                                            $itensCustos = \App\ItemCusto::select('id','nome','data_vencimento','data_pagamento','numero_documento','forma_pagamento','valor','observacao')->where('custo_id',$custo->id)->get();
                                        ?>
                                        <tr>
                                            <td>{{ $custo->nomeCondominio }}</td>
                                            <td>{{ $custo->nome }}</td>
                                            <td>{{ $dataRegistro }}</td>
                                            <td></td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $custo->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $custo->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Custo</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $custo->nomeCondominio }}</b></li>
                                                                    <li class="list-group-item">Nome: <b>{{ $custo->nome }}</b></li>
                                                                    <li class="list-group-item">Data de Registro: <b>{{ $dataRegistro }} </b></li>
                                                                    <li class="list-group-item">Observação: <b><?= nl2br($custo->observacao) ?> </b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("custo/edit/{$custo->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $custo->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7">
                                                <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                                    @foreach($custo->itens as $key => $item)

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading-{{ $item->id }}">
                                                                <a class="accordion-toggle" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $item->id }}" aria-expanded="false" aria-controls="collapse-{{ $item->id }}">
                                                                    <h4 class="panel-title">{{ $item->nome }}</h4>
                                                                    <i class="fa acc-switch"></i>
                                                                </a>
                                                            </div>
                                                            <div id="collapse-{{ $item->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $item->id }}">
                                                                <div class="panel-body">
                                                                    <?php
                                                                        if(!empty($item->data_vencimento)):
                                                                            $dataVencimentoArr = explode('-',$item->data_vencimento);
                                                                            $dataVencimento = "{$dataVencimentoArr[2]}/{$dataVencimentoArr[1]}/{$dataVencimentoArr[0]}";
                                                                        endif;

                                                                        if(!empty($item->data_pagamento)):
                                                                            $dataPagamentoArr = explode('-',$item->data_pagamento);
                                                                            $dataPagamento = "{$dataPagamentoArr[2]}/{$dataPagamentoArr[1]}/{$dataPagamentoArr[0]}";
                                                                        endif;

                                                                        if(trim($item->forma_pagamento) == 'D'):
                                                                            $formaPagamento = "Dinheiro";
                                                                        elseif($item->forma_pagamento == 'CD'):
                                                                            $formaPagamento = "Cartão de debito";
                                                                        elseif(trim($custo->forma_pagamento) == 'B'):
                                                                            $formaPagamento = "Boleto";
                                                                        elseif($item->forma_pagamento == 'DB'):
                                                                            $formaPagamento = "Debito na conta";
                                                                        elseif($item->forma_pagamento == 'T'):
                                                                            $formaPagamento = "Transferencia";
                                                                        else:
                                                                            $formaPagamento = "Não identificada";
                                                                        endif;
                                                                    ?>
                                                                    <p>
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item">Condominio: <b>{{ $custo->condominio->nome }}</b></li>
                                                                            <li class="list-group-item">Nome: <b>{{ $item->nome }}</b></li>
                                                                            <li class="list-group-item">Numero do Documento: <b>{{ $item->numero_documento }}</b></li>
                                                                            <li class="list-group-item">Data de Registro: <b>{{ $dataRegistro }}</b></li>
                                                                            <li class="list-group-item">Data de Pagamento: <b>{{ $dataPagamento }}</b></li>
                                                                            <li class="list-group-item">Data de Vencimento: <b>{{ $dataVencimento }}</b></li>
                                                                            <li class="list-group-item">Forma de Recebimento: <b>{{ $formaPagamento }}</b></li>
                                                                            <li class="list-group-item">Valor: R$ <b>{{ number_format($item->valor,2,',','.') }}</b></li>
                                                                            <li class="list-group-item">Observação: <b><?= nl2br($item->observacao) ?></b></li>
                                                                        </ul>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div><!-- panel-group -->
                                            </td>
                                        </tr>
                                        <div id="modal-delete{{ $custo->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Custo - {{ $custo->nome }}</h4>
                                                            Deseja remover o custo <b>{{ $custo->nome }}</b> da sua base de dados?
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{ url("custo/delete/{$custo->id}") }}">
                                                            <button class="btn btn-sm btn-info">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($custos[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                        $custo = FILTER_INPUT(INPUT_GET,'custo',FILTER_DEFAULT);
                        $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
                        $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $custos->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $custos->currentPage() }} de {{ $custos->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($custos->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$custos->url(1)}&condominio={$condominio}&custo={$custo}&data_registro={$dataRegistro}&valor={$valor}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($custos->previousPageUrl())?"{$custos->previousPageUrl()}&condominio={$condominio}&custo={$custo}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($custos->lastPage() == $custos->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($custos->nextPageUrl())?"{$custos->nextPageUrl()}&condominio={$condominio}&custo={$custo}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($custos->lastPage())?"{$custos->url($custos->lastPage())}&condominio={$condominio}&custo={$custo}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection