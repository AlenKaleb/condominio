@extends('painel.template.app')
@section('content')
    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="widget p-md clearfix">
                            <div class="pull-left">
                                <h3 class="widget-title">Bem vindo a SAC!</h3>
                                <small class="text-color">Sistema de administração de condomínio</small>
                            </div>
                            <!--<span class="pull-right fz-lg fw-500 counter" data-plugin="counterUp"></span>-->
                        </div><!-- .widget -->
                    </div>

                    <div class="col-md-12">
                        <div class="widget p-lg">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="m-b-lg">Notificações</h4>
                                    <p class="m-b-lg docs">
                                        As 10 ultimas notificações estão aqui!!
                                    </p>
                                </div>
                                    @if(!empty($notificas['notificas'][0]))
                                        @foreach($notificas['notificas'] as $key => $notifica)
                                            <?php
                                                if(!empty($notifica->data_registro)):
                                                    $dataRegistroArr = explode('-',$notifica->data_registro);
                                                    $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                                else:
                                                    $dataRegistro = "Data não identificada!";
                                                endif;
                                            ?>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="alert alert-info alert-custom alert-dismissible">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="alert-title"><b>{{ $notifica->titulo }}</b> - <b>{{ $dataRegistro }}</b></h4>
                                                    <p>
                                                        Titulo: <b>{{ $notifica->titulo }}</b> <br />
                                                        Condomínio: <b>{{ $notifica->nomeCondominio }}</b> <br />
                                                        @if(!empty($notifica->nomeMorador))
                                                            Morador: <b>{{ $notifica->nomeMorador }}</b> <br />
                                                        @endif
                                                        Data de Registro: <b>{{ $dataRegistro }}</b> <br />
                                                        Descrição: <b><?= nl2br($notifica->descricao) ?></b></p>
                                                </div>
                                            </div><!-- END column -->
                                        @endforeach
                                    @else
                                        <div class="alert alert-info alert-custom alert-dismissible">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="alert-title">Você está sem notificação no momento!</h4>
                                            <p>Tudo limpo!!</p>
                                        </div>
                                    @endif
                            </div><!-- .row -->
                        </div><!-- .widget -->
                    </div>

                </div><!-- .row -->

            </section><!-- #dash-content -->
        </div><!-- .wrap -->

    </main>
    <!--========== END app main -->
@endsection