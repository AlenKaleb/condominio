@extends('painel.template.app')
@section('content')
        <!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
    <div class="navbar-search-inner">
        <form action="#">
            <span class="search-icon"><i class="fa fa-search"></i></span>
            <input class="search-field" type="search" placeholder="search..."/>
        </form>
        <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <i class="fa fa-close"></i>
        </button>
    </div>
    <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->
<?php
    setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
    date_default_timezone_set( 'America/Sao_Paulo' );
    $totalCusto = 0;
    $totalRecebimento = 0;
    $totalDiferenca = 0;
    if(!empty($custos[0])):
        foreach($custos as $custo):
            $mesCusto[$custo->mes] = $custo->valor;
            $totalCusto += $mesCusto[$custo->mes];
        endforeach;
    endif;
    if(!empty($recebimentos[0])):
        foreach($recebimentos as $recebimento):
            $mesRecebimento[$recebimento->mes] = $recebimento->valor;
            $totalRecebimento += $mesRecebimento[$recebimento->mes];
        endforeach;
    endif;
    for($i = 1; $i < 13; $i++):
        $diferenca[$i] = ((!empty($mesRecebimento[$i])?$mesRecebimento[$i]:0) - (!empty($mesCusto[$i])?$mesCusto[$i]:0));
        $totalDiferenca += $diferenca[$i];
    endfor;
      // dd(number_format($mesCusto[12],2,'.',''));
?>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
    <div class="wrap">
        <section class="app-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="widget">
                        <header class="widget-header">
                            <h4 class="widget-title">Gráfico de Custos</h4>
                        </header><!-- .widget-header -->
                        <hr class="widget-separator">
                        <div class="widget-body">
                            <div id="chart-financeiro" data-plugin="chart" data-options="{
                                  tooltip : {
                                    trigger: 'axis'
                                  },
                                  legend: {
                                    data:['Recebimento','Custo','Diferenca']
                                  },
                                  calculable : true,
                                  xAxis : [
                                    {
                                      type : 'category',
                                      data : ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
                                    }
                                  ],
                                  yAxis : [
                                    {
                                      type : 'value'
                                    }
                                  ],
                                  series : [
                                    {
                                      name:'Recebimento',
                                      type:'bar',
                                      data:[{{ (!empty($mesRecebimento[1])?number_format($mesRecebimento[1],2,'.',''):0)  }}, {{ (!empty($mesRecebimento[2])?number_format($mesRecebimento[2],2,'.',''):0)  }}, {{ (!empty($mesRecebimento[3])?number_format($mesRecebimento[3],2,'.',''):0)  }}, {{ (!empty($mesRecebimento[4])?number_format($mesRecebimento[4],2,'.',''):0)  }}, {{ (!empty($mesRecebimento[5])?number_format($mesRecebimento[5],2,'.',''):0) }}, {{ (!empty($mesRecebimento[6])?number_format($mesRecebimento[6],2,'.',''):0) }}, {{ (!empty($mesRecebimento[7])?number_format($mesRecebimento[7],2,'.',''):0) }}, {{ (!empty($mesRecebimento[8])?number_format($mesRecebimento[8],2,'.',''):0) }}, {{ (!empty($mesRecebimento[9])?number_format($mesRecebimento[9],2,'.',''):0) }}, {{ (!empty($mesRecebimento[10])?number_format($mesRecebimento[10],2,'.',''):0) }}, {{ (!empty($mesRecebimento[11])?number_format($mesRecebimento[11],2,'.',''):0) }}, {{ (!empty($mesRecebimento[12])?number_format($mesRecebimento[12],2,'.',''):0) }}],
                                      markPoint : {
                                        data : [
                                          {type : 'max', name: 'Max',value : {{ (!empty($totalRecebimento)?$totalRecebimento:0) }}},
                                          {type : 'min', name: 'Min',value : 0}
                                        ]
                                      },
                                      markLine : {
                                        data : [
                                          {type : 'average', name: 'Total'}
                                        ]
                                      }
                                    },
                                    {
                                      name:'Custo',
                                      type:'bar',
                                      data:[{{ (!empty($mesCusto[1])?number_format($mesCusto[1],2,'.',''):0)  }}, {{ (!empty($mesCusto[2])?number_format($mesCusto[2],2,'.',''):0)  }}, {{ (!empty($mesCusto[3])?number_format($mesCusto[3],2,'.',''):0)  }}, {{ (!empty($mesCusto[4])?number_format($mesCusto[4],2,'.',''):0)  }}, {{ (!empty($mesCusto[5])?number_format($mesCusto[5],2,'.',''):0) }}, {{ (!empty($mesCusto[6])?number_format($mesCusto[6],2,'.',''):0) }}, {{ (!empty($mesCusto[7])?number_format($mesCusto[7],2,'.',''):0) }}, {{ (!empty($mesCusto[8])?number_format($mesCusto[8],2,'.',''):0) }}, {{ (!empty($mesCusto[9])?number_format($mesCusto[9],2,'.',''):0) }}, {{ (!empty($mesCusto[10])?number_format($mesCusto[10],2,'.',''):0) }}, {{ (!empty($mesCusto[11])?number_format($mesCusto[11],2,'.',''):0) }}, {{ (!empty($mesCusto[12])?number_format($mesCusto[12],2,'.',''):0) }}],
                                      markPoint : {
                                        data : [
                                          {name : 'Max', value : {{ (!empty($totalCusto)?number_format($totalCusto,2,'.',''):0) }}, xAxis: 7, yAxis: 183, symbolSize:18},
                                          {name : 'Min', value : 0, xAxis: 11, yAxis: 3}
                                        ]
                                      },
                                      markLine : {
                                        data : [
                                          {type : 'average', name : 'Total'}
                                        ]
                                      }
                                    },
                                    {
                                      name:'Diferenca',
                                      type:'bar',
                                      data:[{{ (!empty($diferenca[1])?number_format($diferenca[1],2,'.',''):0)  }}, {{ (!empty($diferenca[2])?number_format($diferenca[2],2,'.',''):0)  }}, {{ (!empty($diferenca[3])?number_format($diferenca[3],2,'.',''):0)  }}, {{ (!empty($diferenca[4])?number_format($diferenca[4],2,'.',''):0)  }}, {{ (!empty($diferenca[5])?number_format($diferenca[5],2,'.',''):0) }}, {{ (!empty($diferenca[6])?number_format($diferenca[6],2,'.',''):0) }}, {{ (!empty($diferenca[7])?number_format($diferenca[7],2,'.',''):0) }}, {{ (!empty($diferenca[8])?number_format($diferenca[8],2,'.',''):0) }}, {{ (!empty($diferenca[9])?number_format($diferenca[9],2,'.',''):0) }}, {{ (!empty($diferenca[10])?number_format($diferenca[10],2,'.',''):0) }}, {{ (!empty($diferenca[11])?number_format($diferenca[11],2,'.',''):0) }}, {{ (!empty($diferenca[12])?number_format($diferenca[12],2,'.',''):0) }}],
                                      markPoint : {
                                        data : [
                                          {name : 'Max', value : {{ (!empty($totalDiferenca)?number_format($totalDiferenca,2,'.',''):0) }}, xAxis: 7, yAxis: 183, symbolSize:18},
                                          {name : 'Min', value : 0, xAxis: 11, yAxis: 3}
                                        ]
                                      },
                                      markLine : {
                                        data : [
                                          {type : 'average', name : 'Total'}
                                        ]
                                      }
                                    }
                                  ]
                                }" style="height:300px">
                            </div>
                        </div><!-- .widget-body -->
                    </div><!-- .widget -->
                </div><!-- END column -->
                <div class="col-lg-12">
                    <div class="widget p-md">
                        <h4 class="m-b-lg">Tabela de Lucros / Custos / Diferença</h4>
                        <p class="m-b-lg docs">
                            Tabela de Diferença<code>Q</code>.
                        </p>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Mês</th>
                                    <th>Recebimento</th>
                                    <th>Custo</th>
                                    <th>Diferença</th>
                                </tr>
                                @for($i = 1; $i < 13; $i++)
                                    <?php
                                        $diferenca[$i] = ((!empty($mesRecebimento[$i])?$mesRecebimento[$i]:0) - (!empty($mesCusto[$i])?$mesCusto[$i]:0));
                                    ?>
                                        <tr><td><b>{{ ($i == 1?"JANEIRO":($i == 2?"FEVEREIRO":($i == 3?"MARÇO":($i == 4?"ABRIL":($i == 5?"MAIO":($i == 6?"JUNHO":($i == 7?"JULHO":($i == 8?"AGOSTO":($i == 9?"SETEMBRO":($i == 10?"OUTUBRO":($i == 11?"NOVEMBRO":($i == 12?"DEZEMBRO":"NÃO IDENTIFICADO")))))))))))) }}</b></td>
                                        <td>{{ (!empty($mesRecebimento[$i])?number_format($mesRecebimento[$i],2,',','.'):number_format(0,2,',','.')) }}</td>
                                        <td>{{ (!empty($mesCusto[$i])?number_format($mesCusto[$i],2,',','.'):number_format(0,2,',','.')) }}</td>
                                        <td>{{ (!empty($diferenca[$i])?number_format($diferenca[$i],2,',','.'):0) }}</td>
                                    </tr>
                                @endfor
                            </table>
                        </div>
                    </div><!-- .widget -->
                </div><!-- END column -->
            </div><!-- END row -->
        </section><!-- .app-content -->
    </div><!-- .wrap -->
    <!-- APP FOOTER -->
    <div class="wrap p-t-0">
        <footer class="app-footer">
            <div class="clearfix">
                <ul class="footer-menu pull-right">
                    <li><a href="javascript:void(0)">Careers</a></li>
                    <li><a href="javascript:void(0)">Privacy Policy</a></li>
                    <li><a href="javascript:void(0)">Feedback <i class="fa fa-angle-up m-l-md"></i></a></li>
                </ul>
                <div class="copyright pull-left">Copyright RaThemes 2016 &copy;</div>
            </div>
        </footer>
    </div>
    <!-- /#app-footer -->
</main>
<!--========== END app main -->

@endsection