@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Multas</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Segue abaixo as informações das <code>multa</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Multa <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('multa/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Nova Multa</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>Multa</th>
                                        <th>Data de Registro</th>
                                        <th>Valor</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('multa/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="multa" name="multa" placeholder="Multa" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'multa',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="data_registro" name="data_registro" placeholder="Data de Registro" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="valor" name="valor" placeholder="Valor" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($multas as $key => $multa)
                                        <?php
                                            if(!empty($multa->data_registro)):
                                                $dataRegistroArr = explode('-',$multa->data_registro);
                                                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                            else:
                                                $dataRegistro = "Data não identificada!";
                                            endif;

                                        if(!empty($multa->data_pagamento)):
                                            $dataPagamentoArr = explode('-',$multa->data_pagamento);
                                            $dataPagamento = "{$dataPagamentoArr[2]}/{$dataPagamentoArr[1]}/{$dataPagamentoArr[0]}";
                                        else:
                                            $dataPagamento = "Data não identificada!";
                                        endif;

                                        if(trim($multa->forma_pagamento) == 'D'):
                                            $formaPagamento = "Dinheiro";
                                        elseif($multa->forma_pagamento == 'CD'):
                                            $formaPagamento = "Cartão de debito";
                                        elseif(trim($multa->forma_pagamento) == 'B'):
                                            $formaPagamento = "Boleto";
                                        elseif($multa->forma_pagamento == 'DB'):
                                            $formaPagamento = "Debito na conta";
                                        elseif($multa->forma_pagamento == 'T'):
                                            $formaPagamento = "Transferencia";
                                        endif;
                                        ?>
                                        <tr>
                                            <td>{{ $multa->nomeCondominio }}</td>
                                            <td>{{ $multa->titulo }}</td>
                                            <td>{{ $dataRegistro }}</td>
                                            <td>{{ number_format($multa->valor,2,',','.') }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $multa->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $multa->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Multa</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $multa->nomeCondominio }}</b></li>
                                                                    <li class="list-group-item">Titulo: <b>{{ $multa->titulo }}</b></li>
                                                                    @if(!empty($multa->idMorador))
                                                                        <li class="list-group-item">Morador: <b>{{ $multa->nomeMorador }}</b></li>
                                                                    @endif
                                                                    <li class="list-group-item">Numero do Documento: <b>{{ $multa->numero_documento }}</b></li>
                                                                    <li class="list-group-item">Data de Registro: <b>{{ $dataRegistro }}</b></li>
                                                                    <li class="list-group-item">Data de Pagamento: <b>{{ $dataPagamento }}</b></li>
                                                                    <li class="list-group-item">Forma de Multa: <b>{{ $formaPagamento }}</b></li>
                                                                    <li class="list-group-item">Valor: <b>{{ $multa->valor }}</b></li>
                                                                    <li class="list-group-item">Observação: <b>{{ $multa->motivo }}</b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("multa/edit/{$multa->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $multa->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $multa->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Multa - {{ $multa->titulo }}</h4>
                                                            Deseja remover o multa <b>{{ $multa->titulo }}</b> da sua base de dados?
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{ url("multa/delete/{$multa->id}") }}">
                                                            <button class="btn btn-sm btn-info">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($multas[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                        $multa = FILTER_INPUT(INPUT_GET,'multa',FILTER_DEFAULT);
                        $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
                        $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $multas->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $multas->currentPage() }} de {{ $multas->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($multas->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$multas->url(1)}&condominio={$condominio}&multa={$multa}&data_registro={$dataRegistro}&valor={$valor}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($multas->previousPageUrl())?"{$multas->previousPageUrl()}&condominio={$condominio}&multa={$multa}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($multas->lastPage() == $multas->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($multas->nextPageUrl())?"{$multas->nextPageUrl()}&condominio={$condominio}&multa={$multa}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($multas->lastPage())?"{$multas->url($multas->lastPage())}&condominio={$condominio}&multa={$multa}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection