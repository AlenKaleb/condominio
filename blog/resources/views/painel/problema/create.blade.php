@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Cadastro de Problema</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações da <code>problema</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formProblema" class="form-horizontal" method="post" action="{{ url('problema/store') }}">
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="morador" class="col-sm-3 control-label">Morador:</label>
                            <div class="col-sm-4">
                                <select id="morador" name="morador" class="form-control" >
                                    <option selected value="">Selecione um morador</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="dataRegistro" class="col-sm-3 control-label">Data de Registro:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" id="dataRegistro" name="dataRegistro" value="{{ date('d/m/Y') }}" readonly="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Nome:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="observacao" class="col-sm-3 control-label">Descrição:</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" id="descricao" name="descricao" placeholder="Informe a observação" maxlength="300"></textarea>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();
            $("select[id=condominio]").change(function () {
                $("select[id=condominio] option:selected").each(function () {
                    $.get("{{ url('condominio/condominioMoradores') }}",
                            {
                                condominio: $('#condominio').val()
                            },
                            function (valor) {
                                $("select[id=morador]").html(valor);
                                $("#morador").change();
                            }
                    );
                });
            }).change();

            $('#formProblema').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    condominio: {
                        required: true
                    },
                    nome: {
                        required: true
                    },
                    descricao: {
                        required: true
                    }
                },
                messages: {
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    nome: {
                        required: "Ops! Campo Obrigatório"
                    },
                    descricao: {
                        required: "Ops! Campo Obrigatório"
                    }
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection