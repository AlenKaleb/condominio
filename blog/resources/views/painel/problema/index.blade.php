@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Problemas</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>problema</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Problema <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('problema/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Novo Problema</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>Problema</th>
                                        <th>Data de Registro</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('problema/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="problema" name="problema" placeholder="Problema" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'problema',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="data_registro" name="data_registro" placeholder="Data de Registro" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($problemas as $key => $problema)
                                        <?php
                                            if(!empty($problema->data_registro)):
                                                $dataRegistroArr = explode('-',$problema->data_registro);
                                                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                            else:
                                                $dataRegistro = "Data não identificada!";
                                            endif;
                                        ?>
                                        <tr>
                                            <td>{{ $problema->nomeCondominio }}</td>
                                            <td>{{ $problema->titulo }}</td>
                                            <td>{{ $dataRegistro }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $problema->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $problema->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Problema</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $problema->nomeCondominio }}</b></li>
                                                                    <li class="list-group-item">Data de Registro: <b>{{ $dataRegistro }}</b></li>
                                                                    <li class="list-group-item">Titulo: <b>{{ $problema->titulo }}</b></li>
                                                                    @if(!empty($problema->idMorador))
                                                                        <li class="list-group-item">Morador: <b>{{ $problema->nomeMorador }}</b></li>
                                                                    @endif
                                                                    <li class="list-group-item">Descrição: <b>{{ $problema->descricao }}</b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("problema/edit/{$problema->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $problema->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $problema->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Problema - {{ $problema->titulo }}</h4>
                                                            Deseja remover o problema <b>{{ $problema->titulo }}</b> da sua base de dados?
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{ url("problema/delete/{$problema->id}") }}">
                                                            <button class="btn btn-sm btn-info">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($problemas[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                        $problema = FILTER_INPUT(INPUT_GET,'problema',FILTER_DEFAULT);
                        $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
                        $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $problemas->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $problemas->currentPage() }} de {{ $problemas->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($problemas->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$problemas->url(1)}&condominio={$condominio}&problema={$problema}&data_registro={$dataRegistro}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($problemas->previousPageUrl())?"{$problemas->previousPageUrl()}&condominio={$condominio}&problema={$problema}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($problemas->lastPage() == $problemas->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($problemas->nextPageUrl())?"{$problemas->nextPageUrl()}&condominio={$condominio}&problema={$problema}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($problemas->lastPage())?"{$problemas->url($problemas->lastPage())}&condominio={$condominio}&problema={$problema}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection