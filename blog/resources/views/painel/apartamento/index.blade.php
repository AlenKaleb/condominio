@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Apartamentos</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>apartamento</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Apartamento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('apartamento/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Novo Apartamento</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Condominio</th>
                                        <th>Quadra</th>
                                        <th>Conjunto</th>
                                        <th>Bloco</th>
                                        <th>Apartamento</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('apartamento/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="quadra" name="quadra" placeholder="Quadra" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="conjunto" name="conjunto" placeholder="Conjunto" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="bloco" name="bloco" placeholder="Bloco" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="apartamento" name="apartamento" placeholder="Apartamento" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'apartamento',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($apartamentos as $key => $apartamento)
                                        <tr>
                                            <td>{{ $apartamento->nome }}</td>
                                            <td>{{ $apartamento->quadra }}</td>
                                            <td>{{ $apartamento->conjunto }}</td>
                                            <td>{{ $apartamento->bloco }}</td>
                                            <td>{{ $apartamento->numero }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $apartamento->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $apartamento->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Apartamento</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $apartamento->nome }}</b></li>
                                                                    <li class="list-group-item">Quadra: <b>{{ $apartamento->quadra }}</b></li>
                                                                    <li class="list-group-item">Conjunto: <b>{{ $apartamento->conjunto }}</b></li>
                                                                    <li class="list-group-item">Bloco: <b>{{ $apartamento->bloco }}</b></li>
                                                                    <li class="list-group-item">Apartamento: <b>{{ $apartamento->numero }}</b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("apartamento/edit/{$apartamento->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $apartamento->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $apartamento->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Apartamento - {{ $apartamento->numero }}</h4>
                                                            @if($apartamento->morador()->count() > 0)
                                                                <b class="text text-danger">Acesso Negado!</b>, Este apartamento está relacionado a outros registros do sistema.
                                                            @else
                                                                Deseja remover o apartamento <b>{{ $apartamento->numero }}</b> da sua base de dados?
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        @if($apartamento->morador()->count() > 0)
                                                            <button class="btn btn-sm btn-info" disabled="">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        @else
                                                            <a href="{{ url("apartamento/delete/{$apartamento->id}") }}">
                                                                <button class="btn btn-sm btn-info">
                                                                    <i class="ace-icon fa fa-check"></i>
                                                                    Remover
                                                                </button>
                                                            </a>
                                                        @endif
                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($apartamentos[0]->id))
                                        <tr>
                                            <td colspan="9" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                        $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
                        $conjunto = FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT);
                        $bloco = FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT);
                        $apartamento = FILTER_INPUT(INPUT_GET,'apartamento',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $apartamentos->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $apartamentos->currentPage() }} de {{ $apartamentos->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($apartamentos->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$apartamentos->url(1)}&condominio={$condominio}&quadra={$quadra}&conjunto={$conjunto}&bloco={$bloco}&apartamento={$apartamento}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($apartamentos->previousPageUrl())?"{$apartamentos->previousPageUrl()}&condominio={$condominio}&quadra={$quadra}&conjunto={$conjunto}&bloco={$bloco}&apartamento={$apartamento}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($apartamentos->lastPage() == $apartamentos->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($apartamentos->nextPageUrl())?"{$apartamentos->nextPageUrl()}&condominio={$condominio}&quadra={$quadra}&conjunto={$conjunto}&bloco={$bloco}&apartamento={$apartamento}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($apartamentos->lastPage())?"{$apartamentos->url($apartamentos->lastPage())}&condominio={$condominio}&quadra={$quadra}&conjunto={$conjunto}&bloco={$bloco}&apartamento={$apartamento}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection