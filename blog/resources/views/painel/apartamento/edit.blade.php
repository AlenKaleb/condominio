@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Cadastro de Apartamento</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações da <code>apartamento</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formApartamento" class="form-horizontal" method="post" action="{{ url("apartamento/update/{$apartamento->id}") }}">
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option {{ ($condominio->id == $apartamento->condominio_id?'selected':null) }} value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Quadra:</label>
                            <div class="col-sm-4">
                                <select id="quadra" name="quadra" class="form-control" >
                                    <option selected value="">Selecione uma quadra</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="conjunto" class="col-sm-3 control-label">Conjunto:</label>
                            <div class="col-sm-4">
                                <select id="conjunto" name="conjunto" class="form-control" >
                                    <option selected value="">Selecione uma conjunto</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="lote" class="col-sm-3 control-label">Lote:</label>
                            <div class="col-sm-4">
                                <select id="lote" name="lote" class="form-control" >
                                    <option selected value="">Selecione um lote</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="bloco" class="col-sm-3 control-label">Bloco:</label>
                            <div class="col-sm-4">
                                <select id="bloco" name="bloco" class="form-control" >
                                    <option selected value="">Selecione um bloco</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="apartamento" class="col-sm-3 control-label">Apartamento:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="apartamento" name="apartamento" maxlength="100" placeholder="Informe o apartamento" value="{{ $apartamento->numero }}">
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();
            $("select[id=condominio]").change(function () {
                $("select[id=condominio] option:selected").each(function () {
                    $.get("{{ url('condominio/condominios') }}",
                            {
                                condominio: $('#condominio').val(),
                                quadraCondominio: "{{ $apartamento->quadra_id }}",
                            },
                            function (valor) {
                                $("select[id=quadra]").html(valor);
                                $("#quadra").change();
                            }
                    );
                });
            }).change();

            $("select[id=quadra]").change(function () {
                $("select[id=quadra] option:selected").each(function () {
                    $.get("{{ url('condominio/quadras') }}",
                            {
                                quadra: $('#quadra').val(),
                                conjuntoCondominio: "{{ $apartamento->conjunto_id }}",
                            },
                            function (valor) {
                                $("select[id=conjunto]").html(valor);
                                $("#conjunto").change();
                            }
                    );
                });
            });

            $("select[id=conjunto]").change(function () {
                $("select[id=conjunto] option:selected").each(function () {
                    $.get("{{ url('condominio/conjuntos') }}",
                            {
                                conjunto: $('#conjunto').val(),
                                loteCondominio: "{{ $apartamento->lote_id }}",
                            },
                            function (valor) {
                                $("select[id=lote]").html(valor);
                                $("#lote").change();
                            }
                    );
                });
            });

            $("select[id=lote]").change(function () {
                $("select[id=lote] option:selected").each(function () {
                    $.get("{{ url('condominio/lotes') }}",
                            {
                                lote: $('#lote').val(),
                                blocoCondominio: "{{ $apartamento->bloco_id }}",
                            },
                            function (valor) {
                                $("select[id=bloco]").html(valor);
                                $("#bloco").change();
                            }
                    );
                });
            });

            $('#formApartamento').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    condominio: {
                        required: true
                    },
                    quadra: {
                        required: true
                    },
                    conjunto: {
                        required: true
                    },
                    lote: {
                        required: true
                    },
                    bloco: {
                        required: true
                    },
                    apartamento: {
                        required: true,
                        minlength: 100
                    }
                },
                messages: {
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    quadra: {
                        required: "Ops! Campo Obrigatório"
                    },
                    conjunto: {
                        required: "Ops! Campo Obrigatório"
                    },
                    lote: {
                        required: "Ops! Campo Obrigatório"
                    },
                    bloco: {
                        required: "Ops! Campo Obrigatório"
                    },
                    apartamento: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }

                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection