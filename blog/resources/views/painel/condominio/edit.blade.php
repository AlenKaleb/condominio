@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <link href="{{ url('painel/libs/bower/boostrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Edição de Condominio</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações do seu <code>condominio</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formCondominio" class="form-horizontal" method="post" action="{{ url("condominio/update/{$condominio->id}") }}">
                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Nome:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="nome" name="nome" maxlength="100" placeholder="Informe o nome" value="{{ $condominio->nome }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="funcionario" class="col-sm-3 control-label">Funcionário</label>
                            <div class="col-sm-4">
                                <select id="funcionario" name="funcionario" class="form-control" >
                                    <option selected value="">Selecione um funcionário</option>
                                    @foreach($condominio->funcionarios()->where('cargo','S')->get() as $funcionario)
                                        <option {{ ($funcionario->id == $condominio->funcionario_id?'selected':null) }} value="{{ $funcionario->id }}">{{ $funcionario->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="cnpj" class="col-sm-3 control-label">Cnpj:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="cnpj" name="cnpj" placeholder="Informe o cnpj" value="{{ $condominio->cnpj }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dataRegistro" class="col-sm-3 control-label">Data de Registro:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="dataRegistro" name="data_registro" placeholder="Informe a data de registro" value="{{ $dataRegistroFormatado }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cep" class="col-sm-3 control-label">Cep:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="cep" name="cep" maxlength="20" placeholder="Informe o cep" value="{{ $condominio->cep }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pais" class="col-sm-3 control-label">Pais</label>
                            <div class="col-sm-4">
                                <select id="pais" name="pais" class="form-control" >
                                    <option selected value="">Selecione um pais</option>
                                    @foreach($paises as $pais)
                                        <option {{ ($pais->id == $condominio->pais_id?"selected":null) }} value="{{ $pais->id }}">{{ $pais->nome_pt }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="estado" class="col-sm-3 control-label">Estado</label>
                            <div class="col-sm-4">
                                <select id="estado" name="estado" class="form-control" >
                                    <option selected value="">Selecione um estado</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="cidade" class="col-sm-3 control-label">Cidade</label>
                            <div class="col-sm-4">
                                <select id="cidade" name="cidade" class="form-control" >
                                    <option selected value="">Selecione uma cidade</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="quadra" class="col-sm-3 control-label">Quadra:</label>
                            <i id="novaQuadra" class="fa fa-plus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <i id="removerQuadra" class="fa fa-minus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-send"></i>
                                    </span>
                                    <input type="text" class="form-control" id="inputQuadra0" name="quadra[0]" maxlength="100" placeholder="Informe a quadra" value="{{ (!empty($condominio->quadras[0])?$condominio->quadras[0]->quadra:null) }}">
                                </div>
                            </div>
                        </div>

                        <div id="divQuadras">
                            <div id="divQuadra" class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="quadra"> Quadra:</label>
                                <div class="col-sm-4">
                                    <div class="clearfix">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="ace-icon fa fa-send"></i>
                                            </span>
                                            <input id="inputQuadra" name="quadra[]" maxlength="100" class="form-control" type="text" placeholder="Informe a quadra" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bairro" class="col-sm-3 control-label">Bairro:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="bairro" name="bairro" maxlength="100" placeholder="Informe o bairro" value="{{ $condominio->bairro }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="complemento" class="col-sm-3 control-label">Complemento:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="complemento" name="complemento" maxlength="100" placeholder="Informe o complemento" {{ $condominio->complemento }}>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();
            $('#cnpj').mask('99.999.999/9999-99');
            $('#dataRegistro').mask('99/99/9999');
            $("#dataRegistro").datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });
            carregarQuadra();

            $("#cep").change(function () {
                var cep_code = $(this).val();
                if (cep_code.length <= 0)
                    return;
                $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", {code: cep_code},
                        function (result) {
                            if (result.status != 1) {
                                alert(result.message || "Houve um erro desconhecido");
                                return;
                            }
                            $("input#cep").val(result.code);
                            $("input#bairro").val(result.district);
                        });
            });

            $("select[id=pais]").change(function () {
                $("select[id=pais] option:selected").each(function () {
                    $.get("{{ url('endereco/paises') }}",
                            {
                                pais: $('#pais').val(),
                                estadoEndereco: "{{ $condominio->estado_id }}"
                            },
                            function (valor) {
                                $("select[id=estado]").html(valor);
                                $("#estado").change();
                            }
                    );
                });
            }).change();

            $("select[id=estado]").change(function () {
                $("select[id=estado] option:selected").each(function () {
                    $.get("{{ url('endereco/cidades') }}",
                            {
                                estado: $('#estado').val(),
                                cidadeEndereco: "{{ $condominio->cidade_id }}"
                            },
                            function (valor) {
                                $("select[id=cidade]").html(valor);
                                $("#cidade").change();
                            }
                    );
                });
            });

            function carregarQuadra() {
                var count = 1;
                $("#divQuadra").hide();
                $("#inputQuadra").prop('disabed',true);

                $('#inputQuadra0').attr('maxlength',100);

                @foreach($condominio->quadras as $key => $quadra)
                    @if($key > 0)
                        adicionarQuadra("{{ $quadra->quadra }}");
                    @endif
                @endforeach

                function adicionarQuadra(quadra){
                    var divQuadra = $("#divQuadra").clone().prop('disabed',false).show();
                    divQuadra.removeAttr('id').attr('id', "divQuadra" + count);
                    divQuadra.find("input[id='inputQuadra']").removeAttr('id').removeAttr('name').attr('id', 'inputQuadra' + count).attr('name', "quadra[" + count + "]").attr('maxlength',100).val(quadra);
                    divQuadra.appendTo('#divQuadras');
                    count += 1;
                }

                function removerQuadra(){
                    if(count > 0){
                        count -= 1;
                        $("#divQuadra"+count).remove();
                    }
                }

                $("#novaQuadra").click(function () {
                    var quadra = "";
                    adicionarQuadra(quadra);
                });

                $("#removerQuadra").click(function () {
                    removerQuadra();
                });
            }

            $('#formCondominio').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    nome: {
                        required: true,
                        maxlength: 100
                    },
                    cnpj: {
                        required: true,
                        maxlength: 20,
                        remote: {
                            url: "{{ url('condominio/verificarCnpj') }}",
                            type: "get",
                            data: {
                                cnpj: function () {
                                    return $("#cnpj").val();
                                },
                                cnpjAntigo: '<?= (!empty($condominio->cnpj)?trim($condominio->cnpj):null) ?>'
                            }
                        }
                    },
                    data_registro: {
                        required: true,
                        maxlength: 10
                    },
                    cep: {
                        required: true,
                        maxlength: 10
                    },
                    pais: {
                        required: true
                    },
                    estado: {
                        required: true
                    },
                    cidade: {
                        required: true
                    },
                    bairro: {
                        required: true,
                        maxlength: 100
                    },
                    complemento: {
                        maxlength: 100
                    }
                },
                messages: {
                    nome: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    cnpj: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! ",
                        remote: "Este cnpj é invalido ou já existe"
                    },
                    data_registro: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    cep: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    pais: {
                        required: "Ops! Campo Obrigatório"
                    },
                    estado: {
                        required: "Ops! Campo Obrigatório"

                    },
                    cidade: {
                        required: "Ops! Campo Obrigatório"
                    },
                    bairro: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    complemento: {
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }

                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection