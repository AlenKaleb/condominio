@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Funcionarios</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>funcionario</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Funcionario <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('funcionario/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Novo Funcionario</code>
                            </a>
                        </div>
                            <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>CPF</th>
                                        <th>Cargo</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('funcionario/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="nome" name="nome" placeholder="Nome do funcionario" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="cpf" name="cpf" placeholder="Cpf do funcionario" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <select id="cargo" name="cargo" class="form-control" >
                                                    <option selected value="">Selecione um cargo</option>
                                                    <option value="A">Administrador</option>
                                                    <option value="S">Sindico</option>
                                                    <option value="P">Porteiro</option>
                                                </select>
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <a href="#modal-filtro" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Filtrar
                                                    </button>
                                                </a>
                                                <div id="modal-filtro" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Filtro</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <div class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <label id="labelDataHoraIda" class="col-sm-3 control-label no-padding-right" for="dataHoraIda"> Data de Registro </label>
                                                                        <div class="col-sm-6">
                                                                            <div class="clearfix">
                                                                                <div class="input-group">
                                                                                    <div class="input-group">
                                                                                        <input class="form-control col-xs-10 data" id="data_registro" name="data_registro" type="text" value="{{ FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT) }}" />
                                                                                            <span class="input-group-addon">
                                                                                                <i class="fa fa-calendar bigger-110"></i>
                                                                                            </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-info" type="submit">
                                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                                    Filtrar
                                                                </button>
                                                                <button class="btn btn-sm btn-danger pull-right" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($funcionarios as $key => $funcionario)
                                        <tr>
                                            <td>{{ $funcionario->nome }}</td>
                                            <td>{{ $funcionario->cpf }}</td>
                                            <td>{{ ($funcionario->cargo == 'P'?"Porteiro":($funcionario->cargo == 'A'?"Administrador":($funcionario->cargo == 'S'?"Síndico":null))) }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $funcionario->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $funcionario->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Funcionario</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Nome: <b>{{ $funcionario->nome }}</b></li>
                                                                    <li class="list-group-item">CPF: <b>{{ $funcionario->cpf }}</b></li>
                                                                    <li class="list-group-item">RG: <b>{{ $funcionario->rg }}</b></li>
                                                                    <li class="list-group-item">Cargo: <b>{{ (!empty($funcionario->cargo) && $funcionario->cargo == "S"?"Síndico":(!empty($funcionario->cargo) && $funcionario->cargo == "P"?"Porteiro":(!empty($funcionario->cargo) && $funcionario->cargo == "A"?"Administrador":"Não Identificado"))) }}</b></li>
                                                                    <li class="list-group-item">Salario: <b>{{ $funcionario->salario }}</b></li>
                                                                    <li class="list-group-item">Email: <b>{{ (!empty($funcionario->user)?$funcionario->user->email:null) }}</b></li>
                                                                    <li class="list-group-item">Condominio: <b>{{  (!empty($funcionario->condominio)?$funcionario->condominio->nome:null) }}</b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("funcionario/edit/{$funcionario->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $funcionario->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $funcionario->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Funcionario - {{ $funcionario->nome }}</h4>
                                                            @if($funcionario->condominios()->count() > 0 || $funcionario->quadras()->count() > 0 || $funcionario->conjuntos()->count() > 0 || $funcionario->lotes()->count() > 0 || $funcionario->blocos()->count() > 0)
                                                                <b class="text text-danger">Acesso Negado!</b>, Este funcionário está relacionado a outros registros do sistema.
                                                            @else
                                                                Deseja remover o funcionario <b>{{ $funcionario->nome }}</b> da sua base de dados?
                                                            @endif
                                                        </div>
                                                    </div>



                                                    <div class="modal-footer">
                                                        @if($funcionario->condominios()->count() > 0 || $funcionario->quadras()->count() > 0 || $funcionario->conjuntos()->count() > 0 || $funcionario->lotes()->count() > 0 || $funcionario->blocos()->count() > 0)
                                                            <button class="btn btn-sm btn-info" disabled="">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        @else
                                                            <a href="{{ url("funcionario/delete/{$funcionario->id}") }}">
                                                                <button class="btn btn-sm btn-info">
                                                                    <i class="ace-icon fa fa-check"></i>
                                                                    Remover
                                                                </button>
                                                            </a>
                                                        @endif

                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($funcionarios[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $nome = FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT);
                        $cnpj = FILTER_INPUT(INPUT_GET,'cnpj',FILTER_DEFAULT);
                        $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $funcionarios->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $funcionarios->currentPage() }} de {{ $funcionarios->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($funcionarios->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$funcionarios->url(1)}&nome={$nome}&cnpj={$cnpj}&data_registro={$dataRegistro}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($funcionarios->previousPageUrl())?"{$funcionarios->previousPageUrl()}&nome={$nome}&cnpj={$cnpj}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($funcionarios->lastPage() == $funcionarios->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($funcionarios->nextPageUrl())?"{$funcionarios->nextPageUrl()}&nome={$nome}&cnpj={$cnpj}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($funcionarios->lastPage())?"{$funcionarios->url($funcionarios->lastPage())}&nome={$nome}&cnpj={$cnpj}&data_registro={$dataRegistro}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <!-- endbuild -->

    <script>
        $('select').select2();
    </script>

@endsection