@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Edição de Funcionário</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações do <code>funcionário</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formFuncionario" name="formFuncionario" method="post" action="{{ url("funcionario/update/{$funcionario->id}") }}" class="form-horizontal">
                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Status:</label>
                            <div class="col-xs-3">
                                <div class="radio radio-primary">
                                    <input name="status" type="radio" id="radio-ati" value="A" {{ ($funcionario->user->status == 'A'?"checked":null) }}>
                                    <label for="radio-ati">Ativo</label>
                                </div>
                                <div class="radio radio-info">
                                    <input name="status" type="radio" id="radio-ina" value="I" {{ ($funcionario->user->status == 'I'?"checked":null) }}>
                                    <label for="radio-ina">Inativo</label>
                                </div>
                            </div><!-- .col -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Nome:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome" value="{{ $funcionario->nome }}" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">CPF:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Informe o cpf" value="{{ $funcionario->cpf }}" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">RG:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="rg" name="rg" placeholder="Informe o rg" value="{{ $funcionario->rg }}" maxlength="15">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cargo" class="col-sm-3 control-label">Cargo:</label>
                            <div class="col-sm-4">
                                <select id="cargo" name="cargo" class="form-control" >
                                    <option value="">Selecione um cargo</option>
                                    <option {{ ($funcionario->cargo == 'A'?"selected":null) }} value="A">Administrador</option>
                                    <option {{ ($funcionario->cargo == 'S'?"selected":null) }} value="S">Sindico</option>
                                    <option {{ ($funcionario->cargo == 'P'?"selected":null) }} value="P">Porteiro</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option {{ ($funcionario->condominio_id == $condominio->id?"selected":null) }} value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->
                        <div class="form-group">
                            <label for="telefone" class="col-sm-3 control-label">Telefone:</label>
                            <i id="novoTelefone" class="fa fa-plus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <i id="removerTelefone" class="fa fa-minus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-phone"></i>
                                    </span>
                                    <input type="text" class="form-control" id="inputTelefone0" name="telefone[0]" placeholder="Informe o telefone" value="{{ (!empty($funcionario->telefones[0])?$funcionario->telefones[0]->telefone:null) }}" maxlength="20">
                                </div>
                            </div>
                        </div>

                        <div id="divTelefones">
                            <div id="divTelefone" class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="telefone"> Telefone:</label>
                                <div class="col-sm-4">
                                    <div class="clearfix">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="ace-icon fa fa-phone"></i>
                                            </span>
                                            <input id="inputTelefone" name="telefone[]" class="form-control maskTelefone" type="text" placeholder="Informe o telefone" maxlength="20" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Salario:</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="salario" name="salario" placeholder="Informe o salario" value="{{ $salario }}" maxlength="13">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label for="email" class="col-sm-3 control-label">Email:</label>
                            <div class="col-sm-5">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Informe o email" value="{{ $funcionario->user->email }}" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Senha:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Informe o password" maxlength="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">Confirmar senha:</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" id="confirmarPassword" name="confirmarPassword" placeholder="Informe novamente o password" maxlength="100">
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskMoney.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();

            $("#cpf").mask("999.999.999-99");
            $("#inputTelefone0").mask("(99) 9999?9-9999");

            $("#salario").maskMoney({symbol:'R$ ',
                showSymbol:true, thousands:'.', decimal:',', symbolStay: true});

            carregarTelefone();

            function carregarTelefone() {
                var count = 1;
                $("#divTelefone").hide();
                $("#inputTelefone").prop('disabed',true);

                @foreach($funcionario->telefones as $key => $telefone)
                    @if($key > 0)
                        adicionarTelefone("{{ $telefone->telefone }}");
                    @endif
                @endforeach

                function adicionarTelefone(telefone){
                    var divTelefone = $("#divTelefone").clone().prop('disabed',false).show();
                    divTelefone.removeAttr('id').attr('id', "divTelefone" + count);
                    divTelefone.find("input[id='inputTelefone']").removeAttr('id').removeAttr('name').attr('id', 'inputTelefone' + count).attr('name', "telefone[" + count + "]").attr('data-msg-required','Ops! Campo Obrigatório').prop('required',true).val(telefone).mask("(99) 9999?9-9999");
                    divTelefone.appendTo('#divTelefones');
                    count += 1;
                }

                function removerTelefone(){
                    if(count > 0){
                        count -= 1;
                        $("#divTelefone"+count).remove();
                    }
                }

                $("#novoTelefone").click(function () {
                    var telefone = "";
                    adicionarTelefone(telefone);
                });

                $("#removerTelefone").click(function () {
                    removerTelefone();
                });
            }

            $('#formFuncionario').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    nome: {
                        required: true,
                        maxlength: 100
                    },
                    cpf: {
                        required: true,
                        maxlength: 20,
                        remote: {
                            url: "{{ url('funcionario/verificarCpf') }}",
                            type: "get",
                            data: {
                                cpf: function () {
                                    return $("#cpf").val();
                                },
                                cpfAntigo: '<?= (!empty($funcionario->cpf)?trim($funcionario->cpf):null) ?>'
                            }
                        }
                    },
                    rg: {
                        required: true,
                        maxlength: 15
                    },
                    "telefone[0]": {
                        required: true,
                        maxlength: 15

                    },
                    cargo: {
                        required: true
                    },
                    condominio: {
                        required: true
                    },
                    salario: {
                        required: true,
                        maxlength: 16
                    },
                    email: {
                        required: true,
                        maxlength: 100,
                        email: true,
                        remote: {
                            url: "{{ url('usuario/emailExist') }}",
                            type: "get",
                            data: {
                                email: function() {
                                    return $( "#email" ).val();
                                },
                                emailAntigo: '<?= (!empty($funcionario->user)?trim($funcionario->user->email):null) ?>'
                            }
                        }
                    },
                    password: {
                        maxlength: 10
                    },
                    confirmarPassword: {
                        maxlength: 10
                    }
                },
                messages: {
                    nome: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    cpf: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! ",
                        remote: "Este cpf é invalido ou já existe"
                    },
                    rg: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    "telefone[0]": {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    cargo: {
                        required: "Ops! Campo Obrigatório"
                    },
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    salario: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    email: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! ",
                        remote: "Este email já existe"
                    },
                    password: {
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    confirmarPassword: {
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>
@endsection