@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Edição de Lote</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações da <code>lote</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formLote" class="form-horizontal" method="post" action="{{ url("lote/update/{$lote->id}") }}">
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option {{ ($condominio->id == $lote->condominio_id?'selected':null) }} value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Quadra:</label>
                            <div class="col-sm-4">
                                <select id="quadra" name="quadra" class="form-control" >
                                    <option selected value="">Selecione uma quadra</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="conjunto" class="col-sm-3 control-label">Conjunto:</label>
                            <div class="col-sm-4">
                                <select id="conjunto" name="conjunto" class="form-control" >
                                    <option selected value="">Selecione uma conjunto</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="funcionario" class="col-sm-3 control-label">Funcionário</label>
                            <div class="col-sm-4">
                                <select id="funcionario" name="funcionario" class="form-control" >
                                    <option selected value="">Selecione um funcionário</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="lote" class="col-sm-3 control-label">Lote:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="lote" name="lote" placeholder="Informe o lote" maxlength="100" value="{{ $lote->lote }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="bloco" class="col-sm-3 control-label">Bloco:</label>
                            <i id="novoBloco" class="fa fa-plus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <i id="removerBloco" class="fa fa-minus-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-send"></i>
                                    </span>
                                    <input type="text" class="form-control" id="inputBloco0" name="bloco[0]" placeholder="Informe a bloco" maxlength="100" value="{{ (!empty($lote->blocos[0])?$lote->blocos[0]->bloco:null) }}">
                                </div>
                            </div>
                        </div>

                        <div id="divBlocos">
                            <div id="divBloco" class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="bloco"> Bloco:</label>
                                <div class="col-sm-4">
                                    <div class="clearfix">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="ace-icon fa fa-send"></i>
                                            </span>
                                            <input id="inputBloco" name="bloco[]" class="form-control" type="text" placeholder="Informe a bloco" maxlength="100" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();
            $("select[id=condominio]").change(function () {
                $("select[id=condominio] option:selected").each(function () {
                    $.get("{{ url('condominio/condominioFuncionarios') }}",
                            {
                                condominio: $('#condominio').val(),
                                moradorCondominio: "{{ $lote->funcionario_id }}"
                            },
                            function (valor) {
                                $("select[id=funcionario]").html(valor);
                                $("#funcionario").change();
                            }
                    );
                    $.get("{{ url('condominio/condominios') }}",
                            {
                                condominio: $("#condominio").val(),
                                quadraCondominio: "{{ $lote->quadra_id }}"
                            },
                            function (valor) {
                                $("select[id=quadra]").html(valor);
                                $("#quadra").change();
                            }
                    );
                });
            }).change();

            $("select[id=quadra]").change(function () {
                $("select[id=quadra] option:selected").each(function () {
                    $.get("{{ url('condominio/quadras') }}",
                            {
                                quadra: $("#quadra").val(),
                                conjuntoCondominio: "{{ $lote->conjunto_id }}"
                            },
                            function (valor) {
                                $("select[id=conjunto]").html(valor);
                                $("#conjunto").change();
                            }
                    );
                });
            });

            carregarBloco();

            function carregarBloco() {
                var count = 1;
                $("#divBloco").hide();
                $("#inputBloco").prop('disabed',true);

                $("#inputBloco0").attr('maxlength',100);

                @foreach($lote->blocos as $key => $bloco)
                    @if($key > 0)
                        adicionarBloco("{{ $bloco->bloco }}");
                    @endif
                @endforeach

                function adicionarBloco(bloco){
                        var divBloco = $("#divBloco").clone().prop('disabed',false).show();
                        divBloco.removeAttr('id').attr('id', "divBloco" + count);
                        divBloco.find("input[id='inputBloco']").removeAttr('id').removeAttr('name').attr('id', 'inputBloco' + count).attr('name', "bloco[" + count + "]").attr('data-msg-required','Ops! Campo Obrigatório').attr('maxlength',100).prop('required',true).val(bloco);
                        divBloco.appendTo('#divBlocos');
                        count += 1;
                    }

                function removerBloco(){
                    if(count > 0){
                        count -= 1;
                        $("#divBloco"+count).remove();
                    }
                }

                $("#novoBloco").click(function () {
                    var bloco = "";
                    adicionarBloco(bloco);
                });

                $("#removerBloco").click(function () {
                    removerBloco();
                });
            }

            $('#formLote').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    condominio: {
                        required: true
                    },
                    quadra: {
                        required: true
                    },
                    conjunto: {
                        required: true
                    },
                    lote: {
                        required: true,
                        maxlength: 100
                    }
                },
                messages: {
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    quadra: {
                        required: "Ops! Campo Obrigatório"
                    },
                    conjunto: {
                        required: "Ops! Campo Obrigatório"
                    },
                    lote: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }

                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection