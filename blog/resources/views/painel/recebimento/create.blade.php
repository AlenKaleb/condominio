@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <link href="{{ url('painel/libs/bower/boostrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="col-md-12">
            <div class="widget">
                <header class="widget-header">
                    <h4 class="widget-title">Formulário de Cadastro de Recebimento</h4>
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                    <div class="m-b-lg">
                        <small>
                            Insira abaixo as informações da <code>recebimento</code>.
                        </small>
                    </div>

                    <div align="center">
                        @if($errors->any())
                            <ul id="errors" align="center" class="alert alert-warning">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <form id="formRecebimento" class="form-horizontal" method="post" action="{{ url('recebimento/store') }}">
                        <div class="form-group">
                            <label for="condominio" class="col-sm-3 control-label">Condominio:</label>
                            <div class="col-sm-4">
                                <select id="condominio" name="condominio" class="form-control" >
                                    <option selected value="">Selecione um condominio</option>
                                    @foreach($condominios as $condominio)
                                        <option value="{{ $condominio->id }}">{{ $condominio->nome }}</option>
                                    @endforeach
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="morador" class="col-sm-3 control-label">Morador:</label>
                            <div class="col-sm-4">
                                <select id="morador" name="morador" class="form-control" >
                                    <option selected value="">Selecione um morador</option>
                                </select>
                            </div><!-- END column -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="dataRegistro" class="col-sm-3 control-label">Data de Registro:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" id="dataRegistro" name="dataRegistro" value="{{ date('d/m/Y') }}" readonly="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="dataPagamento" class="col-sm-3 control-label">Data de Recebimento:</label>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="ace-icon fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" id="data" name="data" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="forma" class="col-sm-3 control-label">Forma de Recebimento:</label>
                            <div class="col-xs-3">
                                <div class="radio radio-primary">
                                    <input name="forma" type="radio" id="dinheiro" value="D">
                                    <label for="dinheiro">Dinheiro</label>
                                </div>
                                <div class="radio radio-info">
                                    <input name="forma" type="radio" id="cartaoDebito" value="CD">
                                    <label for="cartaoDebito">Cartão de debito</label>
                                </div>
                                <div class="radio radio-info">
                                    <input name="forma" type="radio" id="boleto" value="B">
                                    <label for="boleto">Boleto</label>
                                </div>
                                <div class="radio radio-info">
                                    <input name="forma" type="radio" id="debitoConta" value="DB">
                                    <label for="debitoConta">Débito na conta</label>
                                </div>
                                <div class="radio radio-info">
                                    <input name="forma" type="radio" id="transferencia" value="T">
                                    <label for="transferencia">Transferência</label>
                                </div>
                            </div><!-- .col -->
                        </div><!-- .form-group -->

                        <div class="form-group">
                            <label for="nome" class="col-sm-3 control-label">Nome:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome" maxlength="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="numeroDocumento" class="col-sm-3 control-label">Numero do documento:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="numeroDocumento" name="numeroDocumento" placeholder="Informe o numero do documento" maxlength="20">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="valor" class="col-sm-3 control-label">Valor:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="valor" name="valor" placeholder="Informe o valor" maxlength="13">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="observacao" class="col-sm-3 control-label">Observação:</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" id="observacao" name="observacao" placeholder="Informe a observação" maxlength="300"></textarea>
                            </div>
                        </div>

                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <button type="submit" class="btn btn-success">Gravar</button>
                            </div>
                        </div>
                    </form>
                </div><!-- .widget-body -->
            </div><!-- .widget -->
        </div><!-- END column -->
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskMoney.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $('select').select2();

            $('#data').mask('99/99/9999');
            $("#data").datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR',
            });

            $("#valor").maskMoney({symbol:'R$ ',
                showSymbol:true, thousands:'.', decimal:',', symbolStay: true});

            $("select[id=condominio]").change(function () {
                $("select[id=condominio] option:selected").each(function () {
                    $.get("{{ url('condominio/condominioMoradores') }}",
                            {
                                condominio: $('#condominio').val()
                            },
                            function (valor) {
                                $("select[id=morador]").html(valor);
                                $("#morador").change();
                            }
                    );
                });
            }).change();

            $('#formRecebimento').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: true,
                ignore: "",
                rules: {
                    condominio: {
                        required: true
                    },
                    nome: {
                        required: true,
                        maxlength: 100
                    },
                    numeroDocumento: {
                        required: true,
                        maxlength: 20
                    },
                    dataPagamento: {
                        required: true,
                        maxlength: 10
                    },
                    forma: {
                        required: true
                    },
                    valor: {
                        required: true,
                        maxlength: 16
                    }
                },
                messages: {
                    condominio: {
                        required: "Ops! Campo Obrigatório"
                    },
                    nome: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    numeroDocumento: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    dataPagamento: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    },
                    forma: {
                        required: "Ops! Campo Obrigatório"
                    },
                    valor: {
                        required: "Ops! Campo Obrigatório",
                        maxlength: "Limite máximo é de {0} caracteres permitido! "
                    }
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>

@endsection