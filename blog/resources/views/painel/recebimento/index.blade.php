@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Recebimentos</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>recebimento</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Recebimento <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('recebimento/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Novo Recebimento</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>Recebimento</th>
                                        <th>Data de Registro</th>
                                        <th>Valor</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('recebimento/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="recebimento" name="recebimento" placeholder="Recebimento" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'recebimento',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="data_registro" name="data_registro" placeholder="Data de Registro" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="valor" name="valor" placeholder="Valor" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($recebimentos as $key => $recebimento)
                                        <?php
                                            if(!empty($recebimento->data_registro)):
                                                $dataRegistroArr = explode('-',$recebimento->data_registro);
                                                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                            else:
                                                $dataRegistro = "Data não identificada!";
                                            endif;

                                        if(!empty($recebimento->data)):
                                            $dataArr = explode('-',$recebimento->data);
                                            $data = "{$dataArr[2]}/{$dataArr[1]}/{$dataArr[0]}";
                                        else:
                                            $data = "Data não identificada!";
                                        endif;

                                        if(trim($recebimento->forma) == 'D'):
                                            $forma = "Dinheiro";
                                        elseif($recebimento->forma == 'CD'):
                                            $forma = "Cartão de debito";
                                        elseif(trim($recebimento->forma) == 'B'):
                                            $forma = "Boleto";
                                        elseif($recebimento->forma == 'DB'):
                                            $forma = "Debito na conta";
                                        elseif($recebimento->forma == 'T'):
                                            $forma = "Transferencia";
                                        endif;
                                        ?>
                                        <tr>
                                            <td>{{ $recebimento->nomeCondominio }}</td>
                                            <td>{{ $recebimento->titulo }}</td>
                                            <td>{{ $dataRegistro }}</td>
                                            <td>{{ number_format($recebimento->valor,2,',','.') }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $recebimento->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $recebimento->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Recebimento</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $recebimento->nomeCondominio }}</b></li>
                                                                    <li class="list-group-item">Titulo: <b>{{ $recebimento->titulo }}</b></li>
                                                                    @if(!empty($recebimento->idMorador))
                                                                        <li class="list-group-item">Morador: <b>{{ $recebimento->nomeMorador }}</b></li>
                                                                    @endif
                                                                    <li class="list-group-item">Numero do Documento: <b>{{ $recebimento->numero_documento }}</b></li>
                                                                    <li class="list-group-item">Data de Registro: <b>{{ $dataRegistro }}</b></li>
                                                                    <li class="list-group-item">Data de Pagamento: <b>{{ $data }}</b></li>
                                                                    <li class="list-group-item">Forma de Recebimento: <b>{{ $forma }}</b></li>
                                                                    <li class="list-group-item">Valor: <b>{{ $recebimento->valor }}</b></li>
                                                                    <li class="list-group-item">Observação: <b>{{ $recebimento->observacao }}</b></li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("recebimento/edit/{$recebimento->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $recebimento->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $recebimento->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Recebimento - {{ $recebimento->titulo }}</h4>
                                                            Deseja remover o recebimento <b>{{ $recebimento->titulo }}</b> da sua base de dados?
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <a href="{{ url("recebimento/delete/{$recebimento->id}") }}">
                                                            <button class="btn btn-sm btn-info">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        </a>
                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($recebimentos[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                        $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                        $recebimento = FILTER_INPUT(INPUT_GET,'recebimento',FILTER_DEFAULT);
                        $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
                        $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $recebimentos->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $recebimentos->currentPage() }} de {{ $recebimentos->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($recebimentos->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$recebimentos->url(1)}&condominio={$condominio}&recebimento={$recebimento}&data_registro={$dataRegistro}&valor={$valor}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($recebimentos->previousPageUrl())?"{$recebimentos->previousPageUrl()}&condominio={$condominio}&recebimento={$recebimento}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($recebimentos->lastPage() == $recebimentos->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($recebimentos->nextPageUrl())?"{$recebimentos->nextPageUrl()}&condominio={$condominio}&recebimento={$recebimento}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($recebimentos->lastPage())?"{$recebimentos->url($recebimentos->lastPage())}&condominio={$condominio}&recebimento={$recebimento}&data_registro={$dataRegistro}&valor={$valor}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection