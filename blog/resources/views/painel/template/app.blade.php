<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">
    <title>SAC - Condominio</title>

    <link rel="stylesheet" href="{{ url('painel/libs/bower/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('painel/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css') }}">
    <!-- build:css ../assets/css/app.min.css -->
    <link rel="stylesheet" href="{{ url('painel/libs/bower/animate.css/animate.min.css') }}">

    <link rel="stylesheet" href="{{ url('painel/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ url('painel/assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('painel/assets/css/core.css') }}">
    <link rel="stylesheet" href="{{ url('painel/assets/css/app.css') }}">
    <!-- endbuild -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
    <script src="{{ url('painel/libs/bower/breakpoints.js/dist/breakpoints.min.js') }}"></script>
    <script>
        Breakpoints();
    </script>
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary">
<!--============= start main area -->

<?php
    $restricaoAcesso = new \App\RestricaoAcesso();
    $notificas = \App\Notifica::paginate(3);
?>

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">

    <!-- navbar header -->
    <div class="navbar-header">
        <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>

        <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="zmdi zmdi-hc-lg zmdi-more"></span>
        </button>

        <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="zmdi zmdi-hc-lg zmdi-search"></span>
        </button>

        <a href="{{ url('inicio') }}" class="navbar-brand">
            <span class="brand-icon"><i class="fa fa-gg"></i></span>
            <span class="brand-name">SAC</span>
        </a>
    </div><!-- .navbar-header -->

    <div class="navbar-container container-fluid">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
                <li class="hidden-float hidden-menubar-top">
                    <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
                        <span class="hamburger-box"><span class="hamburger-inner"></span></span>
                    </a>
                </li>
                <li>
                    <h5 class="page-title hidden-menubar-top hidden-float">Inicio</h5>
                </li>
            </ul>

            <ul class="nav navbar-toolbar navbar-toolbar-right navbar-right">
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="zmdi zmdi-hc-lg zmdi-notifications"></i></a>
                    <div class="media-group dropdown-menu animated flipInY">
                        @foreach($notificas as $notifica)
                            <?php
                                if(!empty($notifica->data_registro)):
                                    $dataRegistroArr = explode('-',$notifica->data_registro);
                                    $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
                                else:
                                    $dataRegistro = "Data não identificada!";
                                endif;
                            ?>
                            <a href="javascript:void(0)" class="media-group-item">
                                <div class="media">
                                    <!--<div class="media-left">
                                        <div class="avatar avatar-xs avatar-circle">
                                            <img src="../assets/images/221.jpg" alt="">
                                            <i class="status status-online"></i>
                                        </div>
                                    </div>-->
                                    <div class="media-body">
                                        <h5 class="media-heading">{{ $notifica->titulo }}</h5>
                                        <small class="media-meta"><h5 class="media-heading">{{ $dataRegistro }}</h5></small>
                                    </div>
                                </div>
                            </a><!-- .media-group-item -->
                        @endforeach
                    </div>
                </li>

                <!--<li class="nav-item dropdown hidden-float">
                    <a href="javascript:void(0)" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
                        <i class="zmdi zmdi-hc-lg zmdi-search"></i>
                    </a>
                </li>-->
            </ul>
        </div>
    </div><!-- navbar-container -->
</nav>
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
    <div class="app-user">
        <div class="media">
            <div class="media-left">

            </div>
            <div class="media-body">
                <?php
                    $usuario = (!empty(session('usuario')->funcionario->id)?session('usuario')->funcionario()->first():(!empty(session('usuario')->morador->id)?session('usuario')->morador()->first():null));
                ?>
                <div class="foldable">
                    <h5><a href="javascript:void(0)" class="username">{{ (!empty($usuario->nome)?$usuario->nome:null) }}</a></h5>
                    <ul>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <small>{{ (!empty($usuario->cargo) && $usuario->cargo == "S"?"Síndico":(!empty($usuario->cargo) && $usuario->cargo == "M"?"Morador":(!empty($usuario->cargo) && $usuario->cargo == "P"?"Porteiro":(!empty($usuario->cargo) && $usuario->cargo == "A"?"Administrador":(!empty(session('usuario')->morador->id)?"Morador":"Não Identificado"))))) }}</small>
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu animated flipInY">
                                <li>
                                    <a class="text-color" href="{{ url('inicio') }}">
                                        <span class="m-r-xs"><i class="fa fa-home"></i></span>
                                        <span>Inicio</span>
                                    </a>
                                </li>
                                <!--<li>
                                    <a class="text-color" href="profile.html">
                                        <span class="m-r-xs"><i class="fa fa-user"></i></span>
                                        <span>Dados Pessoais</span>
                                    </a>
                                </li>-->
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a class="text-color" href="{{ url('logout') }}">
                                        <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                                        <span>Sair</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- .media-body -->
        </div><!-- .media -->
    </div><!-- .app-user -->

    <div class="menubar-scroll">
        <div class="menubar-scroll-inner">
            <ul class="app-menu">
                <li class="has-submenu">
                    <a href="javascript:void(0)" class="submenu-toggle">
                        <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
                        <span class="menu-text">Inicio</span>
                        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                    </a>
                    <ul class="submenu">
                        <li><a href="{{ url('inicio') }}"><span class="menu-text">Inicio</span></a></li>
                    </ul>
                </li>
                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Condominios</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('condominio/index') }}"><span class="menu-text">Condominios</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Quadras</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('quadra/index') }}"><span class="menu-text">Quadras</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Conjuntos</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('conjunto/index') }}"><span class="menu-text">Conjuntos</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Lotes</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('lote/index') }}"><span class="menu-text">Lotes</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Blocos</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('bloco/index') }}"><span class="menu-text">Blocos</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('condominio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Apartamentos</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('apartamento/index') }}"><span class="menu-text">Apartamentos</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('taxa')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Taxas</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('taxa/index') }}"><span class="menu-text">Taxas</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('recebimento')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Recebimentos</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('recebimento/index') }}"><span class="menu-text">Recebimentos</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('funcionario')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Funcionários</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('funcionario/index') }}"><span class="menu-text">Funcionários</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('custo')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Custos</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('custo/index') }}"><span class="menu-text">Custos</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('problema')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Problemas</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('problema/index') }}"><span class="menu-text">Problemas</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('morador')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Moradores</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('morador/index') }}"><span class="menu-text">Moradores</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('visitante')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Visitantes</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('visitante/index') }}"><span class="menu-text">Visitantes</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('notifica')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Notificações</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('notifica/index') }}"><span class="menu-text">Notificacações</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('multa')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-storage zmdi-hc-lg"></i>
                            <span class="menu-text">Multas</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('multa/index') }}"><span class="menu-text">Multas</span></a></li>
                        </ul>
                    </li>
                @endif

                @if($restricaoAcesso->validarPermissao('relatorio')):
                    <li class="has-submenu">
                        <a href="javascript:void(0)" class="submenu-toggle">
                            <i class="menu-icon zmdi zmdi-chart zmdi-hc-lg"></i>
                            <span class="menu-text">Relatorios</span>
                            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
                        </a>
                        <ul class="submenu">
                            <li><a href="{{ url('relatorio/financeiro') }}"><span class="menu-text">Financeiro</span></a></li>
                        </ul>
                    </li>
                @endif
            </ul><!-- .app-menu -->
        </div><!-- .menubar-scroll-inner -->
    </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
    <div class="navbar-search-inner">
        <form action="#">
            <span class="search-icon"><i class="fa fa-search"></i></span>
            <input class="search-field" type="search" placeholder="search..."/>
        </form>
        <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <i class="fa fa-close"></i>
        </button>
    </div>
    <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

@yield('content')

<!-- build:js ../assets/js/core.min.js -->
<script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
{{--<script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>--}}
<script src="{{ asset('painel/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
<script src="{{ asset('painel/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js') }}"></script>
<script src="{{ asset('painel/libs/bower/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('painel/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
<script src="{{ asset('painel/libs/bower/PACE/pace.min.js') }}"></script>
<!-- endbuild -->

<!-- build:js ../assets/js/app.min.js -->
<script src="{{ asset('painel/assets/js/library.js') }}"></script>
<script src="{{ asset('painel/assets/js/plugins.js') }}"></script>
<script src="{{ asset('painel/assets/js/app.js') }}"></script>
<!-- endbuild -->
<script src="{{ asset('painel/libs/bower/moment/moment.js') }}"></script>

</body>
</html>