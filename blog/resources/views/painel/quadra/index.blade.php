@extends('painel.template.app')
@section('content')

    <link href="{{ url('painel/libs/bower/select2/dist/css/select2.min.css') }}" rel="stylesheet" />

    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="widget">
            <header class="widget-header">
                <h4 class="widget-title">Quadras</h4>
            </header><!-- .widget-header -->
            <hr class="widget-separator">
            <div class="widget-body">
                <div class="m-b-lg">
                    <small>
                        Insira abaixo as informações do seu <code>quadra</code>.
                    </small>
                </div>
                <div class="row">
                    <div class="col-xs-10 col-xs-offset-1">
                        @if(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT) ||  FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT) || FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT))
                            <div class="alert {{ (base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == "true" || base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == "true"?"alert-info":"alert-danger") }}">
                                <button class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>
                                @if(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'true')
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> cadastrado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'cadastrar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi cadastrado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'true')
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> alterado com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'alterar',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi alterado!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'true')
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> excluido com sucesso!
                                @elseif(base64_decode(FILTER_INPUT(INPUT_GET,'excluir',FILTER_DEFAULT)) == 'false')
                                    <b>Erro inesperado!</b> <br />
                                    Quadra <b>{{ base64_decode(FILTER_INPUT(INPUT_GET,'nomeC',FILTER_DEFAULT)) }}</b> não foi excluido!
                                @endif;
                            </div>
                        @endif
                        <div>
                            <a href="{{ url('quadra/create') }}" class="group">
                                <span class="fa fa-plus-circle"></span>
                                <code>Nova Quadra</code>
                            </a>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="simple-table" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="detail-col">Nome</th>
                                        <th>Quadra</th>
                                        <th>Ver</th>
                                        <th>Alterar</th>
                                        <th>Excluir </th>
                                        <th></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <form action="{{ url('quadra/index') }}" method="get">
                                        <tr>
                                            <td>
                                                <input type="text" id="condominio" name="condominio" placeholder="Condominio" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>
                                                <input type="text" id="quadra" name="quadra" placeholder="Quadra" class="form-control col-xs-10 col-md-12" value="{{ FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT) }}" />
                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>

                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-info" type="submit">
                                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                                    Filtrar
                                                </button>
                                            </td>
                                        </tr>
                                        <input type="hidden" name="page" class="col-xs-10 col-md-12" value="1" />
                                    </form>
                                    @foreach($quadras as $key => $quadra)
                                        <tr>
                                            <td>{{ $quadra->condominio->nome }}</td>
                                            <td>{{ $quadra->quadra }}</td>
                                            <td>
                                                <a href="#modal-visualizacao{{ $quadra->id }}" role="button" data-toggle="modal">
                                                    <button class="btn btn-sm btn-info" type="button">
                                                        <i class="ace-icon fa fa-check bigger-110"></i>
                                                        Ver
                                                    </button>
                                                </a>
                                                <div id="modal-visualizacao{{ $quadra->id }}" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                <h3 class="smaller lighter blue no-margin">Quadra</h3>
                                                            </div>

                                                            <div class="modal-body">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">Condominio: <b>{{ $quadra->condominio->nome }}</b></li>
                                                                    <li class="list-group-item">Quadra: <b>{{ $quadra->quadra }}</b></li>
                                                                    <li class="list-group-item">Conjuntos:
                                                                        @foreach($quadra->conjuntos as $key => $conjunto)
                                                                            <b>{{ $conjunto->conjunto }}</b>{{ ($key == $quadra->conjuntos->count() - 1?".":",") }}
                                                                        @endforeach
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                                    <i class="ace-icon fa fa-times"></i>
                                                                    Fechar
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="{{ url("quadra/edit/{$quadra->id}") }}">
                                                        <button class="btn btn-sm btn-warning">
                                                            <i class="ace-icon fa fa-pencil bigger-120"></i>
                                                            Editar
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="hidden-sm hidden-xs btn-group">
                                                    <a href="#modal-delete{{ $quadra->id }}" role="button" data-toggle="modal">
                                                        <button class="btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                                            Remover
                                                        </button>
                                                    </a>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <div id="modal-delete{{ $quadra->id }}" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h3 class="smaller lighter blue no-margin">Janela de Exclusão</h3>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class="well well-lg">
                                                            <h4 class="blue">Quadra - {{ $quadra->nome }}</h4>
                                                            @if($quadra->conjuntos()->count() > 0)
                                                                <b class="text text-danger">Acesso Negado!</b>, Esta quadra está relacionada a outros registros do sistema.
                                                            @else
                                                                Deseja remover a quadra <b>{{ $quadra->nome }}</b> da sua base de dados?
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        @if($quadra->conjuntos()->count() > 0)
                                                            <button class="btn btn-sm btn-info" disabled="">
                                                                <i class="ace-icon fa fa-check"></i>
                                                                Remover
                                                            </button>
                                                        @else
                                                            <a href="{{ url("quadra/delete/{$quadra->id}") }}">
                                                                <button class="btn btn-sm btn-info">
                                                                    <i class="ace-icon fa fa-check"></i>
                                                                    Remover
                                                                </button>
                                                            </a>
                                                        @endif

                                                        <button class="btn btn-sm btn-danger" data-dismiss="modal">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Fechar
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div>
                                    @endforeach
                                    @if(empty($quadras[0]->id))
                                        <tr>
                                            <td colspan="8" class="text-center">
                                                Ops! tabela vazia
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.span -->
                        </div><!-- /.row -->

                        <?php
                            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
                            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
                        ?>

                        <div class="message-footer clearfix">
                            <div class="pull-left"> Total de {{ $quadras->total() }} registros </div>

                            <div class="pull-right">
                                <div class="inline middle"> pagina {{ $quadras->currentPage() }} de {{ $quadras->lastPage() }} </div>

                                &nbsp; &nbsp;
                                <ul class="pagination middle">
                                    @if($quadras->currentPage() == 1)
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ "{$quadras->url(1)}&condominio={$condominio}&quadra={$quadra}" }}">
                                                <i class="ace-icon fa fa-step-backward middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($quadras->previousPageUrl())?"{$quadras->previousPageUrl()}&condominio={$condominio}&quadra={$quadra}":null) }}">
                                                <i class="ace-icon fa fa-caret-left bigger-140 middle"></i>
                                            </a>
                                        </li>
                                    @endif

                                    @if($quadras->lastPage() == $quadras->currentPage())
                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </span>
                                        </li>

                                        <li class="disabled">
                                            <span>
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </span>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ (!empty($quadras->nextPageUrl())?"{$quadras->nextPageUrl()}&condominio={$condominio}&quadra={$quadra}":null) }}">
                                                <i class="ace-icon fa fa-caret-right bigger-140 middle"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="{{ (!empty($quadras->lastPage())?"{$quadras->url($quadras->lastPage())}&condominio={$condominio}&quadra={$quadra}":null) }}">
                                                <i class="ace-icon fa fa-step-forward middle"></i>
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
    </main>

    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- endbuild -->

@endsection