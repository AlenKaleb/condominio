@extends('painel.template.app')
@section('content')

    <div style="padding-top: 100px" class="simple-page-form animated flipInY col-xs-4 col-xs-offset-4" id="login-form">
        <h4 class="form-title m-b-xl text-center">Painel de Acesso</h4>
        <form id="formLogin" name="formLogin" method="post" action="{{ url('login') }}">
            <div class="form-group{{ $errors->has('inativo') ? ' has-error' : '' }}">
                @if ($errors->has('inativo'))
                    <div class="alert alert-danger">
                        <span class="help-block">
                            {{ $errors->first('inativo') }}
                        </span>
                    </div>
                @endif
            </div>
            <div class="form-group">
                <input id="email" name="email" type="email" class="form-control" placeholder="Email">
            </div>

            <div class="form-group">
                <input id="senha" name="password" type="password" class="form-control" placeholder="Password">
            </div>

            <div class="form-group m-b-xl">
                <div class="checkbox checkbox-primary">
                    <input type="checkbox" id="keep_me_logged_in"/>
                    <label for="keep_me_logged_in">Lembrar senha</label>
                </div>
            </div>
            {{ csrf_field() }}
            <input type="submit" class="btn btn-primary" value="Entrar">
        </form>
    </div><!-- #login-form -->


    <script src="{{ asset('painel/libs/bower/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('painel/libs/bower/jquery-validate/dist/js/jquery.validate.min.js') }}"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($) {
            $('#formLogin').validate({
                errorElement: 'div',
                errorClass: 'help-block',
                focusInvalid: false,
                ignore: "",
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{ url('usuario/emailExist') }}",
                            type: "get",
                            data: {
                                email: function() {
                                    return $( "#email" ).val();
                                },
                                login: function() {
                                    return true;
                                }
                            }
                        }
                    },
                    password: {
                        required: true
                    }
                },

                messages: {
                    email: {
                        required: "Ops! Parece que esqueceu de informar o email",
                        email: "Ops! email invalido",
                        remote: "Este email não existe"
                    },
                    password: {
                        required: "Ops! Parece que esqueceu de informar a senha"
                    }
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                },

                success: function (e) {
                    $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                    $(e).remove();
                },

                errorPlacement: function (error, element) {
                    if (element.is('input[type=checkbox]') || element.is('input[type=radio]')) {
                        var controls = element.closest('div[class*="col-"]');
                        if (controls.find(':checkbox,:radio').length > 1) controls.append(error);
                        else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                    }
                    else if (element.is('.select2')) {
                        error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
                    }
                    else if (element.is('.chosen-select')) {
                        error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
                    }
                    else error.insertAfter(element.parent());
                }
            });
        });
    </script>
@endsection