<?php

namespace App;

use App\Http\Controllers\Lote\LoteController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Conjunto extends Model
{
    protected $fillable = ['id','quadra_id','conjunto','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'conjuntos';
    protected $primaryKey = 'id';

    public function quadra(){
        return $this->belongsTo('\App\Quadra','quadra_id');
    }

    public function funcionario(){
        return $this->belongsTo('\App\Funcionario','funcionario_id');
    }

    public function lotes(){
        return $this->hasMany('\App\Lote','conjunto_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
            $conjunto = FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT);
            $conjuntos = \App\Conjunto::select('conjuntos.id','conjuntos.conjunto','quadras.quadra','quadras.condominio_id','conjuntos.quadra_id','condominios.nome')->leftJoin('quadras','quadras.id','=','conjuntos.quadra_id')->leftJoin('condominios','quadras.condominio_id','=','condominios.id')->where('conjuntos.conjunto','LIKE',"%{$conjunto}%")->where('quadras.quadra','LIKE',"%{$quadra}%")->where('condominios.nome','LIKE',"%{$condominio}%")->orderBy('conjuntos.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['conjuntos' => $conjuntos,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @return mixed
     */
    public function conjuntos(){
        $conjunto_id = (FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT):null);
        $loteCondominio = FILTER_INPUT(INPUT_GET,'loteCondominio',FILTER_DEFAULT);
        $lotes = \App\Lote::select('id','lote')->where('conjunto_id',$conjunto_id)->get();
        $loteArr[0] = '<option selected value=""> Selecione um lote </option>';
        foreach ($lotes as $key => $lote):
            $key++;
            if($loteCondominio == $lote->id):
                $loteArr[$key] = '<option selected value="' . $lote->id . '">' . $lote->lote . '</option>';
            else:
                $loteArr[$key] = '<option value="' . $lote->id . '">' . $lote->lote . '</option>';
            endif;
        endforeach;
        return $loteArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $condominios = \App\Condominio::select('id','nome')->get();
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        if(is_array($input['conjunto'])):
            foreach($input['conjunto'] as $inputConjunto):
                if(!empty($inputConjunto)):
                    $conjunto = new Conjunto();
                    $conjunto->quadra_id = $input['quadra_id'];
                    $conjunto->conjunto = $inputConjunto;
                    $conjunto->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['conjunto']);
            try {
                $conjunto = new Conjunto();
                $conjunto->funcionario_id = $input['funcionario'];
                $conjunto->quadra_id = $input['quadra'];
                $conjunto->conjunto = $input['conjunto'];
                $conjunto->save();

                $request->request->add(['conjunto_id' => $conjunto->id]);
                $loteController = new LoteController();
                $loteController->store($request);
                $cadastrar = base64_encode('true');
            }catch (PDOException $e){
                $cadastrar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
        endif;
    }

    public function editar($id)
    {
        try{
            $condominios = \App\Condominio::select('id','nome')->get();
            $conjunto = \App\Conjunto::select('conjuntos.id','conjunto','condominio_id','quadra_id','conjuntos.funcionario_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('conjuntos.id',$id)->first();
            return ['condominios' => $condominios,'conjunto' => $conjunto];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return redirect("/inicio?index={$index}");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conjunto  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id = null)
    {
        $input = $request->all();

        if(is_array($input['conjunto'])):
            \App\Conjunto::where('quadra_id',$input['quadra_id'])->delete();
            foreach($input['conjunto'] as $inputConjunto):
                if(!empty($inputConjunto)):
                    $conjunto = new Conjunto();
                    $conjunto->quadra_id = $input['quadra_id'];
                    $conjunto->conjunto = $inputConjunto;
                    $conjunto->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['conjunto']);
            try {
                $conjunto = \App\Conjunto::find($id);
                $conjunto->id = $id;
                $conjunto->funcionario_id = $input['funcionario'];
                $conjunto->quadra_id = $input['quadra'];
                $conjunto->conjunto = $input['conjunto'];
                $conjunto->save();

                $request->request->add(['conjunto_id' => $id]);
                $loteController = new LoteController();
                $loteController->update($request);
                $alterar = base64_encode('true');
            }catch (PDOException $e){
                $alterar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'alterar' => $alterar];
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $conjunto = \App\Conjunto::select('conjunto')->where('id',$id)->first();
        $nomeBase = base64_encode($conjunto->conjunto);
        try {
            $lotes = \App\Lote::where('conjunto_id',$id)->count();
            if($lotes == 0):
                \App\Conjunto::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
