<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Taxa extends Model
{
    protected $fillable = ['id','condominio_id','nome','data_registro','valor','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'taxas';
    protected $primaryKey = 'id';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'taxa',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('dataFuncionario')->cargo == 'S'):
                $taxas = \App\Taxa::select('taxas.id','taxas.nome','taxas.condominio_id','taxas.data_registro','taxas.valor')->leftJoin('condominios','condominios.id','taxas.condominio_id')->where('taxas.nome','LIKE',"%{$nome}%")->where('taxas.data_registro','LIKE',"%{$dataRegistro}%")->where('taxas.valor','LIKE',"%{$valor}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('taxas.id','desc')->paginate(10);
            else:
                $taxas = \App\Taxa::select('taxas.id','taxas.nome','taxas.condominio_id','taxas.data_registro','taxas.valor')->leftJoin('condominios','condominios.id','taxas.condominio_id')->where('taxas.nome','LIKE',"%{$nome}%")->where('taxas.data_registro','LIKE',"%{$dataRegistro}%")->where('taxas.valor','LIKE',"%{$valor}%")->orderBy('taxas.id','desc')->paginate(10);
            endif;
            $index = base64_encode('true');
            return ['taxas' => $taxas,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->cargo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['taxa']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;

            if(!empty($input['valor'])):
                $valor = str_replace(array('R$','.',','),array('','','.'),$input['valor']);
            else:
                $valor = null;
            endif;

            $taxa = new Taxa();
            $taxa->condominio_id = $input['condominio'];
            $taxa->data_registro = $dataRegistro;
            $taxa->nome = $input['taxa'];
            $taxa->valor = $valor;
            $taxa->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $taxa = \App\Taxa::select('id','condominio_id','nome','data_registro','valor')->where('id',$id)->first();
            if(!empty($taxa->data_registro)):
                $dataRegistroArr = explode('-',$taxa->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;

            if(!empty($taxa->valor)):
                $valor = number_format($taxa->valor,2,',','.');
            else:
                $valor = null;
            endif;

            return ['condominios' => $condominios,'taxa' => $taxa,'valor' => $valor,'dataRegistro' => $dataRegistro];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['taxa']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;

            if(!empty($input['valor'])):
                $valor = str_replace(array('R$','.',','),array('','','.'),$input['valor']);
            else:
                $valor = null;
            endif;

            $taxa = \App\Taxa::find($id);
            $taxa->id = $id;
            $taxa->condominio_id = $input['condominio'];
            $taxa->data_registro = $dataRegistro;
            $taxa->nome = $input['taxa'];
            $taxa->valor = $valor;
            $taxa->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $taxa = \App\Taxa::select('nome')->where('id',$id)->first();
        $nomeBase = base64_encode($taxa->nome);
        try {
            \App\Taxa::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
