<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDOException;

class Recebimento extends Model
{
    protected $fillable = ['id','condominio_id','morador_id','titulo','numero_documento','forma','data_registro','data','valor','observacao','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'recebimentos';
    protected $primaryKey = 'id';

    public function condominio(){
        return $this->belongsTo('\App\Condominio','condominio_id');
    }

    public function morador(){
        return $this->belongsTo('\App\Morador','morador_id');
    }

    public function recebimentosAnual(){
        $ano = date('Y');
        return \App\Recebimento::select(
            DB::raw('MONTH(data) AS mes'),DB::raw('SUM(valor) AS valor'))->where('data','LIKE',"{$ano}-%-%")->groupby(DB::raw('MONTH(data)'))
            ->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = session('id');
        try{
            $nome = FILTER_INPUT(INPUT_GET,'recebimento',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('tipo') == 'M'):
                $recebimentos = \App\Recebimento::select('recebimentos.id','titulo','numero_documento','condominio_id','condominios.nome AS nomeCondominio','recebimentos.data_registro','data','forma','valor','observacao')->leftJoin('condominios','condominios.id','recebimentos.condominio_id')->leftJoin('moradors','moradors.id','recebimentos.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('recebimentos.data_registro','LIKE',"%{$dataRegistro}%")->where('valor','LIKE',"%{$valor}%")->where('recebimentos.morador_id',"{$id}")->orderBy('recebimentos.id','desc')->paginate(10);
            elseif(!empty(session('dataFuncionario')) && session('dataFuncionario')->cargo == 'S'):
                $recebimentos = \App\Recebimento::select('recebimentos.id','titulo','numero_documento','condominio_id','condominios.nome AS nomeCondominio','recebimentos.data_registro','data','forma','valor','observacao')->leftJoin('condominios','condominios.id','recebimentos.condominio_id')->where('titulo','LIKE',"%{$nome}%")->where('recebimentos.data_registro','LIKE',"%{$dataRegistro}%")->where('valor','LIKE',"%{$valor}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('recebimentos.id','desc')->paginate(10);
            elseif(!empty(session('dataFuncionario')) && session('dataFuncionario')->cargo == 'A'):
                $recebimentos = \App\Recebimento::select('recebimentos.id','titulo','numero_documento','condominio_id','condominios.nome AS nomeCondominio','recebimentos.data_registro','data','forma','valor','observacao')->leftJoin('condominios','condominios.id','recebimentos.condominio_id')->where('titulo','LIKE',"%{$nome}%")->where('recebimentos.data_registro','LIKE',"%{$dataRegistro}%")->where('valor','LIKE',"%{$valor}%")->orderBy('recebimentos.id','desc')->paginate(10);
            endif;
            $index = base64_encode('true');
            return ['recebimentos' => $recebimentos,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->tipo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;

        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            if(!empty($input['data'])):
                $dataArr = explode('/',$input['data']);
                $data = "{$dataArr[2]}-{$dataArr[1]}-{$dataArr[0]}";
            else:
                $data = null;
            endif;

            if(!empty($input['valor'])):
                $valor = str_replace(array('R$','.',','),array('','','.'),$input['valor']);
            else:
                $valor = null;
            endif;

            $recebimento = new Recebimento();
            $recebimento->condominio_id = $input['condominio'];
            $recebimento->morador_id = $input['morador'];
            $recebimento->data_registro = $dataRegistro;
            $recebimento->data = $data;
            $recebimento->titulo = $input['nome'];
            $recebimento->numero_documento = $input['numeroDocumento'];
            $recebimento->forma = $input['forma'];
            $recebimento->valor = $valor;
            $recebimento->observacao = $input['observacao'];
            $recebimento->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $recebimento = \App\Recebimento::where('id',$id)->first();
            if(!empty($recebimento->data_registro)):
                $dataRegistroArr = explode('-',$recebimento->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;
            if(!empty($recebimento->data)):
                $dataArr = explode('-',$recebimento->data);
                $data = "{$dataArr[2]}/{$dataArr[1]}/{$dataArr[0]}";
            else:
                $data = null;
            endif;
            if(!empty($recebimento->valor)):
                $valor = number_format($recebimento->valor,2,',','.');
            else:
                $valor = null;
            endif;
            return ['condominios' => $condominios,'recebimento' => $recebimento,'dataRegistro' => $dataRegistro,'data' => $data,'valor' => $valor];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            if(!empty($input['data'])):
                $dataArr = explode('/',$input['data']);
                $data = "{$dataArr[2]}-{$dataArr[1]}-{$dataArr[0]}";
            else:
                $data = null;
            endif;
            if(!empty($input['valor'])):
                $valor = str_replace(array('R$','.',','),array('','','.'),$input['valor']);
            else:
                $valor = null;
            endif;
            $recebimento = \App\Recebimento::find($id);
            $recebimento->id = $id;
            $recebimento->condominio_id = $input['condominio'];
            $recebimento->morador_id = $input['morador'];
            $recebimento->data_registro = $dataRegistro;
            $recebimento->data = $data;
            $recebimento->titulo = $input['nome'];
            $recebimento->numero_documento = $input['numeroDocumento'];
            $recebimento->forma = $input['forma'];
            $recebimento->valor = $valor;
            $recebimento->observacao = $input['observacao'];
            $recebimento->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $recebimento = \App\Recebimento::select('titulo')->where('id',$id)->first();
        $nomeBase = base64_encode($recebimento->titulo);
        try {
            \App\Recebimento::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
