<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Problema extends Model
{
    protected $fillable = ['id','condominio_id','morador_id','titulo','data_registro','descricao','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'problemas';
    protected $primaryKey = 'id';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'problema',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('dataFuncionario')->cargo == 'S'):
                $problemas = \App\Problema::select('problemas.id','titulo','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','problemas.data_registro','descricao')->leftJoin('condominios','condominios.id','problemas.condominio_id')->leftJoin('moradors','moradors.id','problemas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('problemas.data_registro','LIKE',"%{$dataRegistro}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('problemas.id','desc')->paginate(10);
            else:
                $problemas = \App\Problema::select('problemas.id','titulo','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','problemas.data_registro','descricao')->leftJoin('condominios','condominios.id','problemas.condominio_id')->leftJoin('moradors','moradors.id','problemas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('problemas.data_registro','LIKE',"%{$dataRegistro}%")->orderBy('problemas.id','desc')->paginate(10);
            endif;

            $index = base64_encode('true');
            return ['problemas' => $problemas,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->cargo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $problema = new Problema();
            $problema->condominio_id = $input['condominio'];
            $problema->morador_id = $input['morador'];
            $problema->data_registro = $dataRegistro;
            $problema->titulo = $input['nome'];
            $problema->descricao = $input['descricao'];
            $problema->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $problema = \App\Problema::select('problemas.id','titulo','condominio_id','morador_id','problemas.data_registro','descricao')->leftJoin('condominios','condominios.id','problemas.condominio_id')->leftJoin('moradors','moradors.id','problemas.morador_id')->where('problemas.id',$id)->first();
            if(!empty($problema->data_registro)):
                $dataRegistroArr = explode('-',$problema->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;
            return ['condominios' => $condominios,'problema' => $problema,'dataRegistro' => $dataRegistro];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $problema = \App\Problema::find($id);
            $problema->id = $id;
            $problema->condominio_id = $input['condominio'];
            $problema->morador_id = $input['morador'];
            $problema->data_registro = $dataRegistro;
            $problema->titulo = $input['nome'];
            $problema->descricao = $input['descricao'];
            $problema->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $problema = \App\Problema::select('titulo')->where('id',$id)->first();
        $nomeBase = base64_encode($problema->titulo);
        try {
            \App\Problema::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
