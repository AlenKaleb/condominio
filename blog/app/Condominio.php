<?php

namespace App;

use App\Http\Controllers\Quadra\QuadraController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

final class Condominio extends Model
{
    protected $fillable = ['id','nome','data_registro','cnpj','cep','funcionario_id','cidade_id','bairro','complemento','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'condominios';
    protected $primaryKey = 'id';

    public function funcionario(){
        return $this->belongsTo('\App\Funcionario','funcionario_id');
    }

    public function funcionarios(){
        return $this->hasMany('\App\Funcionario','condominio_id');
    }

    public function cidade(){
        return $this->belongsTo('\App\Cidade','cidade_id');
    }

    public function quadras(){
        return $this->hasMany('\App\Quadra','condominio_id');
    }

    public function validateCnpj($cnpj){
        $validar = new ValidaCpfCnpj($cnpj);
        if($validar->valida() != 1):
            return false;
        else:
            return true;
        endif;
    }

    public function cnpjExist($cnpj){
        $cnpjExist = \App\Condominio::where('cnpj',$cnpj)->first();
        if(!empty($cnpjExist->cnpj)):
            return false;
        else:
            return true;
        endif;
    }

    public function verificarCnpj(){
        $cnpj = FILTER_INPUT(INPUT_GET,'cnpj',FILTER_DEFAULT);
        $cnpjAntigo = FILTER_INPUT(INPUT_GET,'cnpjAntigo',FILTER_DEFAULT);
        if($this->validateCnpj($cnpj) && $this->cnpjExist($cnpj) || $cnpjAntigo == $cnpj):
            echo "true";
        else:
            echo "false";
        endif;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT);
            $cnpj = FILTER_INPUT(INPUT_GET,'cnpj',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistroFormatado = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistroFormatado = null;
            endif;
            $condominios = \App\Condominio::select('condominios.id','condominios.nome','cnpj','cep','funcionario_id','pais_id','estado_id','cidade_id','bairro','complemento')->leftJoin('cidades','cidades.id','condominios.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('condominios.nome','LIKE',"%{$nome}%")->where('cnpj','LIKE',"%{$cnpj}%")->where('data_registro','LIKE',"%{$dataRegistroFormatado}%")->orderBy('condominios.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['condominios' => $condominios,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @return mixed
     */
    public function condominios(){
        $condominio_id = (FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT):null);
        $quadraCondominio = FILTER_INPUT(INPUT_GET,'quadraCondominio',FILTER_DEFAULT);
        $quadras = \App\Quadra::select('id','quadra')->where('condominio_id',$condominio_id)->get();
        $quadraArr[0] = '<option selected value=""> Selecione uma quadra </option>';
        foreach ($quadras as $key => $quadra):
            $key++;
            if($quadraCondominio == $quadra->id):
                $quadraArr[$key] = '<option selected value="' . $quadra->id . '">' . $quadra->quadra . '</option>';
            else:
                $quadraArr[$key] = '<option value="' . $quadra->id . '">' . $quadra->quadra . '</option>';
            endif;
        endforeach;
        return $quadraArr;
    }

    /**
     * @return mixed
     */
    public function condominioMoradores(){
        $condominio_id = (FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT):null);
        $moradorCondominio = FILTER_INPUT(INPUT_GET,'moradorCondominio',FILTER_DEFAULT);
        $moradores = \App\Morador::select('moradors.id','moradors.nome')->leftJoin('apartamentos','apartamentos.id','moradors.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('condominios.id',$condominio_id)->get();
        $moradorArr[0] = '<option selected value=""> Selecione uma morador </option>';
        foreach ($moradores as $key => $morador):
            $key++;
            if($moradorCondominio == $morador->id):
                $moradorArr[$key] = '<option selected value="' . $morador->id . '">' . $morador->nome . '</option>';
            else:
                $moradorArr[$key] = '<option value="' . $morador->id . '">' . $morador->nome . '</option>';
            endif;
        endforeach;
        return $moradorArr;
    }

    /**
     * @return mixed
     */
    public function condominioFuncionarios(){
        $condominio_id = (FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT):null);
        $funcionarioCondominio = FILTER_INPUT(INPUT_GET,'funcionarioCondominio',FILTER_DEFAULT);
        $funcionarios = \App\Funcionario::select('id','nome')->where('condominio_id',$condominio_id)->get();
        $funcionarioArr[0] = '<option selected value=""> Selecione um funcionario </option>';
        foreach ($funcionarios as $key => $funcionario):
            $key++;
            if($funcionarioCondominio == $funcionario->id):
                $funcionarioArr[$key] = '<option selected value="' . $funcionario->id . '">' . $funcionario->nome . '</option>';
            else:
                $funcionarioArr[$key] = '<option value="' . $funcionario->id . '">' . $funcionario->nome . '</option>';
            endif;
        endforeach;
        return $funcionarioArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $paises = \App\Pais::select('id','nome_pt')->get();
        $funcionarios = \App\Funcionario::select('id','nome')->where('cargo','S')->get();
        return ['paises' => $paises,'funcionarios' => $funcionarios];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {

            $dataRegistroArr = explode('/',$input['data_registro']);
            $dataRegistroFormatado = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";

            $condominio = new Condominio();
            $condominio->nome = $input['nome'];
            $condominio->cnpj = $input['cnpj'];
            $condominio->data_registro = $dataRegistroFormatado;
            $condominio->cep = $input['cep'];
            $condominio->funcionario_id = $input['funcionario'];
            $condominio->cidade_id = $input['cidade'];
            $condominio->bairro = $input['bairro'];
            $condominio->complemento = $input['complemento'];
            $condominio->save();

            $request->request->add(['condominio' => $condominio->id]);

            $quadraController = new QuadraController();
            $quadraController->store($request);

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $paises = \App\Pais::select('id','nome_pt')->get();
        $condominio = \App\Condominio::select('condominios.id','condominios.nome','cnpj','data_registro','cep','funcionario_id','pais_id','estado_id','cidade_id','bairro','complemento')->leftJoin('cidades','cidades.id','condominios.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('condominios.id',$id)->first();
        $dataRegistroArr = explode('-',$condominio->data_registro);
        $dataRegistroFormatado = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
        return ['paises' => $paises,'condominio' => $condominio,'dataRegistroFormatado' => $dataRegistroFormatado];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try{
            $dataRegistroArr = explode('/',$input['data_registro']);
            $dataRegistroFormatado = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";

            \App\Condominio::select('id')->where('id',$id)->update(['nome' => $input['nome'],'cnpj' => $input['cnpj'],'data_registro' => $dataRegistroFormatado,'cep' => $input['cep'],'funcionario_id' => $input['funcionario'],'cidade_id' => $input['cidade'],'bairro' => $input['bairro'],'complemento' => $input['complemento']]);

            $request->request->add(['condominio' => $id]);
            $quadraController = new QuadraController();
            $quadraController->update($request);

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $condominio = \App\Condominio::select('nome')->where('id',$id)->first();
        $nomeBase = base64_encode($condominio->nome);
        try{
            $quadras = \App\Quadra::where('condominio_id',$id)->count();
            $recebimentos = \App\Recebimento::where('condominio_id',$id)->count();
            $custos = \App\Custo::where('condominio_id',$id)->count();
            $problemas = \App\Problema::where('condominio_id',$id)->count();
            $multas = \App\Multa::where('condominio_id',$id)->count();
            $taxas = \App\Taxa::where('condominio_id',$id)->count();
            $funcionarios = \App\Funcionario::where('condominio_id',$id)->count();
            $notificacoes = \App\Notifica::where('condominio_id',$id)->count();
            if($quadras == 0 && $recebimentos == 0 && $custos == 0 && $problemas == 0|| $multas == 0 && $taxas == 0 && $funcionarios == 0 && $notificacoes == 0):
                \App\Condominio::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }

}
