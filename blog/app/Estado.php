<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $fillable = ['id','pais_id','nome','sigla','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'estados';
    protected $primaryKey = 'id';

    public function pais(){
        return $this->belongsTo('\App\Pais','pais_id');
    }

}
