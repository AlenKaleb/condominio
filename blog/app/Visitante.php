<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Visitante extends Model
{
    protected $fillable = ['id','apartamento_id','nome','data_hora','cpf','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'visitantes';
    protected $primaryKey = 'id';

//    public function apartamento(){
//        return $this->hasOne('\App\Apartamento','funcionario_id');
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT);
            $cpf = FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT);
            if(session('dataFuncionario')->cargo == 'S' || session('dataFuncionario')->cargo == 'P'):
                $visitantes = \App\Visitante::select('visitantes.id','visitantes.nome AS nomeVisitante','data_hora','cpf','visitantes.apartamento_id','condominios.nome AS nomeCondominio','apartamentos.numero')->leftJoin('apartamentos','apartamentos.id','visitantes.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('visitantes.nome','LIKE',"%{$nome}%")->where('cpf','LIKE',"%{$cpf}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('visitantes.id','desc')->paginate(10);
            else:
                $visitantes = \App\Visitante::select('visitantes.id','visitantes.nome AS nomeVisitante','data_hora','cpf','visitantes.apartamento_id','condominios.nome AS nomeCondominio','apartamentos.numero')->leftJoin('apartamentos','apartamentos.id','visitantes.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('visitantes.nome','LIKE',"%{$nome}%")->where('cpf','LIKE',"%{$cpf}%")->orderBy('visitantes.id','desc')->paginate(10);
            endif;
            $index = base64_encode('true');
            return ['visitantes' => $visitantes,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        try{
            if(session('dataFuncionario')->cargo == 'S' || session('dataFuncionario')->cargo == 'P'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            return ['condominios' => $condominios];
        }catch (PDOException $e){
            $index = base64_encode('false');;
            return ['index' => $index];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $visitante = new Visitante();
            $visitante->apartamento_id = $input['apartamento'];
            $visitante->nome = $input['nome'];
            $visitante->data_hora = date('Y-m-d H:i:s');
            $visitante->cpf = $input['cpf'];
            $visitante->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S' || session('dataFuncionario')->cargo == 'P'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $visitante = \App\Visitante::select('visitantes.id','visitantes.nome','visitantes.data_hora','cpf','apartamento_id','condominio_id','quadra_id','conjunto_id','lote_id','bloco_id','apartamento_id')->leftJoin('apartamentos','apartamentos.id','visitantes.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('visitantes.id',$id)->first();
            if(!empty($visitante->data_hora)):
                $dataHoraArr = explode(' ',$visitante->data_hora);
                $dataArr = explode('-',$dataHoraArr[0]);
                $dataHora = "{$dataArr[2]}/{$dataArr[1]}/{$dataArr[0]}";
            else:
                $dataHora = null;
            endif;
            $index = base64_encode('true');
            return ['condominios' => $condominios,'visitante' => $visitante,'dataHora' => $dataHora,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $visitante = \App\Visitante::find($id);
            $visitante->id = $id;
            $visitante->apartamento_id = $input['apartamento'];
            $visitante->nome = $input['nome'];
            $visitante->cpf = $input['cpf'];
            $visitante->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function excluir($id)
    {
        $visitante = \App\Visitante::select('id','nome')->where('id',$id)->first();
        $nomeBase = base64_encode($visitante->nome);
        try {
            \App\Visitante::where('id',$visitante->id)->delete();

            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
