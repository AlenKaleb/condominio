<?php

namespace App;

use App\Http\Controllers\Telefone\TelefoneController;
use App\Http\Controllers\User\UserController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

final class Funcionario extends Model
{
    protected $fillable = ['id','user_id','nome','cpf','rg','cargo','salario','condominio_id','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'funcionarios';
    protected $primaryKey = 'id';

    public function user(){
        return $this->hasOne('\App\User','usuario_id')->where('tipo', 'F');
    }

    public function condominio(){
        return $this->hasOne('\App\Condominio','funcionario_id')->where('funcionario_id','<>',null);
    }

    public function condominios(){
        return $this->belongsTo('\App\Condominio','funcionario_id')->where('funcionario_id','<>',null);
    }

    public function quadras(){
        return $this->hasMany('\App\Quadra','funcionario_id');
    }

    public function conjuntos(){
        return $this->hasMany('\App\Conjunto','funcionario_id');
    }

    public function lotes(){
        return $this->hasMany('\App\Lote','funcionario_id');
    }

    public function blocos(){
        return $this->hasMany('\App\Bloco','funcionario_id');
    }

    public function telefones(){
        return $this->hasMany('\App\Telefone','usuario_id')->where('usuario_tipo', 'F');
    }

    public function validateCpf($cpf){
        $validar = new ValidaCpfCnpj($cpf);
        if($validar->valida() != 1):
            return false;
        else:
            return true;
        endif;
    }

    public function cpfExist($cpf){
        $cpfExist = \App\Funcionario::where('cpf',$cpf)->first();
        if(!empty($cpfExist->cpf)):
            return false;
        else:
            return true;
        endif;
    }

    public function verificarCpf(){
        $cpf = FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT);
        $cpfAntigo = FILTER_INPUT(INPUT_GET,'cpfAntigo',FILTER_DEFAULT);
        if($this->validateCpf($cpf) && $this->cpfExist($cpf) || $cpfAntigo == $cpf):
            echo "true";
        else:
            echo "false";
        endif;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT);
            $cpf = FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT);
            $rg = FILTER_INPUT(INPUT_GET,'rg',FILTER_DEFAULT);
            $cargo = FILTER_INPUT(INPUT_GET,'cargo',FILTER_DEFAULT);
            $salario = FILTER_INPUT(INPUT_GET,'salario',FILTER_DEFAULT);
            $funcionarios = \App\Funcionario::select('funcionarios.id','funcionarios.nome','cpf','cargo','rg','salario')->leftJoin('condominios','condominios.id','funcionarios.condominio_id')->where('funcionarios.nome','LIKE',"%{$nome}%")->where('cpf','LIKE',"%{$cpf}%")->where('rg','LIKE',"%{$rg}%")->where('cargo','LIKE',"%{$cargo}%")->where('salario','LIKE',"%{$salario}%")->orderBy('funcionarios.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['funcionarios' => $funcionarios,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $condominios = \App\Condominio::select('id','nome')->get();
        return ['condominios' => $condominios];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $userController = new UserController();
            $user_id = $userController->store($request);

            if(!empty($input['salario'])):
                $salario = str_replace(array('R$','.',','),array('','','.'),$input['salario']);
            else:
                $salario = null;
            endif;

            $funcionario = new Funcionario();
            $funcionario->user_id = $user_id;
            $funcionario->condominio_id = $input['condominio'];
            $funcionario->nome = $input['nome'];
            $funcionario->cpf = $input['cpf'];
            $funcionario->rg = $input['rg'];
            $funcionario->cargo = $input['cargo'];
            $funcionario->salario = $salario;
            $funcionario->save();

            \App\User::where('id',$user_id)->update(['usuario_id' => $funcionario->id,'tipo' => "F"]);

            $telefoneController = new TelefoneController();
            $telefoneController->store($request,$funcionario->id,'F');
            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        try{
            $condominios = \App\Condominio::select('id','nome')->get();
            $funcionario = \App\Funcionario::where('id',$id)->first();
            if(!empty($funcionario->salario)):
                $salario = number_format($funcionario->salario,2,',','.');
            endif;
            $index = base64_encode('true');
            return ['condominios' => $condominios,'funcionario' => $funcionario,'salario' => $salario,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $input = $request->all();

            if(!empty($input['salario'])):
                $salario = str_replace(array('R$','.',','),array('','','.'),$input['salario']);
            else:
                $salario = null;
            endif;

            $funcionario = \App\Funcionario::find($id);
            $funcionario->id = $id;
            $funcionario->condominio_id = $input['condominio'];
            $funcionario->nome = $input['nome'];
            $funcionario->cpf = $input['cpf'];
            $funcionario->rg = $input['rg'];
            $funcionario->cargo = $input['cargo'];
            $funcionario->salario = $salario;
            $funcionario->save();

            $telefoneController = new TelefoneController();
            $telefoneController->update($request,$funcionario->id,'F');

            $userController = new UserController();
            $userController->update($request,$funcionario->user_id);
            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function excluir($id)
    {
        $funcionario = \App\Funcionario::select('nome','user_id')->where('id',$id)->first();
        $nomeBase = base64_encode($funcionario->nome);
        try {
            $telefoneController = new TelefoneController();
            $telefoneController->destroy($funcionario->id,'F');

            \App\Funcionario::where('id',$id)->delete();

            $userController = new UserController();
            $userController->destroy($funcionario->user_id);
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
