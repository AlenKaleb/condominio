<?php

namespace App;

use App\Http\Controllers\Conjunto\ConjuntoController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

final class Quadra extends Model
{
    protected $fillable = ['id','condominio_id','funcionario_id','quadra','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'quadras';
    protected $primaryKey = 'id';

    public function condominio(){
        return $this->belongsTo('\App\Condominio','condominio_id');
    }

    public function funcionario(){
        return $this->belongsTo('\App\Funcionario','funcionario_id');
    }

    public function conjuntos(){
        return $this->hasMany('App\Conjunto','quadra_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
            $quadras = \App\Quadra::select('quadras.id','quadra','condominio_id','nome')->leftJoin('condominios','condominios.id','=','quadras.condominio_id')->where('quadra','LIKE',"%{$quadra}%")->where('nome','LIKE',"%{$condominio}%")->orderBy('quadras.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['quadras' => $quadras,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return redirect("/inicio?index={$index}");
        }
    }

    /**
     * @return mixed
     */
    public function quadras(){
        $quadra_id = (FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT):null);
        $conjuntoCondominio = FILTER_INPUT(INPUT_GET,'conjuntoCondominio',FILTER_DEFAULT);
        $cojuntos = \App\Conjunto::select('id','conjunto')->where('quadra_id',$quadra_id)->get();
        $conjuntoArr[0] = '<option selected value=""> Selecione um conjunto </option>';
        foreach ($cojuntos as $key => $cojunto):
            $key++;
            if($conjuntoCondominio == $cojunto->id):
                $conjuntoArr[$key] = '<option selected value="' . $cojunto->id . '">' . $cojunto->conjunto . '</option>';
            else:
                $conjuntoArr[$key] = '<option value="' . $cojunto->id . '">' . $cojunto->conjunto . '</option>';
            endif;
        endforeach;
        return $conjuntoArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $condominios = \App\Condominio::select('id','nome')->get();
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        if(is_array($input['quadra'])):
            foreach($input['quadra'] as $inputQuadra):
                if(!empty($inputQuadra)):
                    $quadra = new Quadra();
                    $quadra->condominio_id = $input['condominio'];
                    $quadra->quadra = $inputQuadra;
                    $quadra->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['quadra']);
            try {
                $quadra = new Quadra();
                $quadra->funcionario_id = $input['funcionario'];
                $quadra->condominio_id = $input['condominio'];
                $quadra->quadra = $input['quadra'];
                $quadra->save();

                $request->request->add(['quadra_id' => $quadra->id]);
                $conjuntoController = new ConjuntoController();
                $conjuntoController->store($request);
                $cadastrar = base64_encode('true');
            }catch (PDOException $e){
                $cadastrar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
        endif;
    }

    public function editar($id)
    {
        try{
            $condominios = \App\Condominio::select('id','nome')->get();
            $quadra = \App\Quadra::select('id','quadra','condominio_id','funcionario_id')->where('id',$id)->first();
            return ['condominios' => $condominios,'quadra' => $quadra];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @throws \Exception
     */
    public function alterar(Request $request, $id = null)
    {
        $input = $request->all();
        if(is_array($input['quadra'])):
            \App\Quadra::where('condominio_id',$input['condominio'])->delete();
            foreach($input['quadra'] as $inputQuadra):
                if(!empty($inputQuadra)):
                    $quadra = new Quadra();
                    $quadra->condominio_id = $input['condominio'];
                    $quadra->quadra = $inputQuadra;
                    $quadra->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['quadra']);
            try {
                $quadra = \App\Quadra::find($id);
                $quadra->id = $id;
                $quadra->condominio_id = $input['condominio'];
                $quadra->funcionario_id = $input['funcionario'];
                $quadra->quadra = $input['quadra'];
                $quadra->save();

                $request->request->add(['quadra_id' => $quadra->id]);
                $conjuntoController = new ConjuntoController();
                $conjuntoController->update($request);
                $alterar = base64_encode('true');
            }catch (PDOException $e){
                $alterar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'alterar' => $alterar];
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $quadra = \App\Quadra::select('quadra')->where('id',$id)->first();
        $nomeBase = base64_encode($quadra->quadra);
        try {
            $conjuntos = \App\Conjunto::where('quadra_id',$id)->count();
            if($conjuntos == 0):
                \App\Quadra::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }

}
