<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Notifica extends Model
{
    protected $fillable = ['id','condominio_id','morador_id','titulo','data_registro','descricao','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'notificas';
    protected $primaryKey = 'id';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'notifica',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('dataFuncionario')->cargo == 'S'):
                $notificas = \App\Notifica::select('notificas.id','titulo','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','notificas.data_registro','descricao')->leftJoin('condominios','condominios.id','notificas.condominio_id')->leftJoin('moradors','moradors.id','notificas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('notificas.data_registro','LIKE',"%{$dataRegistro}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('notificas.id','desc')->paginate(10);
            else:
                $notificas = \App\Notifica::select('notificas.id','titulo','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','notificas.data_registro','descricao')->leftJoin('condominios','condominios.id','notificas.condominio_id')->leftJoin('moradors','moradors.id','notificas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('notificas.data_registro','LIKE',"%{$dataRegistro}%")->orderBy('notificas.id','desc')->paginate(10);
            endif;
            $index = base64_encode('true');
            return ['notificas' => $notificas,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->cargo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $notifica = new Notifica();
            $notifica->condominio_id = $input['condominio'];
            $notifica->morador_id = $input['morador'];
            $notifica->data_registro = $dataRegistro;
            $notifica->titulo = $input['nome'];
            $notifica->descricao = $input['descricao'];
            $notifica->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $notifica = \App\Notifica::select('notificas.id','titulo','condominio_id','morador_id','notificas.data_registro','descricao')->leftJoin('condominios','condominios.id','notificas.condominio_id')->leftJoin('moradors','moradors.id','notificas.morador_id')->where('notificas.id',$id)->first();
            if(!empty($notifica->data_registro)):
                $dataRegistroArr = explode('-',$notifica->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;
            return ['condominios' => $condominios,'notifica' => $notifica,'dataRegistro' => $dataRegistro];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $notifica = \App\Notifica::find($id);
            $notifica->id = $id;
            $notifica->condominio_id = $input['condominio'];
            $notifica->morador_id = $input['morador'];
            $notifica->data_registro = $dataRegistro;
            $notifica->titulo = $input['nome'];
            $notifica->descricao = $input['descricao'];
            $notifica->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $notifica = \App\Notifica::select('titulo')->where('id',$id)->first();
        $nomeBase = base64_encode($notifica->titulo);
        try {
            \App\Notifica::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
