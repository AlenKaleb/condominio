<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','usuario_id','tipo','email', 'password','remember_token','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function funcionario(){
        return $this->hasOne('App\Funcionario','user_id');
    }

    public function morador(){
        return $this->hasOne('App\Morador','user_id');
    }

    /**
     * @return bool
     */
    public function emailExist(){
        $email = FILTER_INPUT(INPUT_GET,'email',FILTER_DEFAULT);
        $emailAntigo = FILTER_INPUT(INPUT_GET,'emailAntigo',FILTER_DEFAULT);
        $login = FILTER_INPUT(INPUT_GET,'login',FILTER_DEFAULT);
        $emailRes = \App\User::select('email')->where('email',$email)->first();
        if(empty($login)):
            if(!empty($emailRes) && $emailRes->email != $emailAntigo):
                echo "false";
            else:
                echo "true";
            endif;
        else:
            if(!empty($emailRes) && $login == true):
                echo "true";
            else:
                echo "false";
            endif;
        endif;
    }

    /**
     *
     */
    public function validatePassword(){
        $email = FILTER_INPUT(INPUT_GET,'email',FILTER_DEFAULT);
        $senhaAtual = FILTER_INPUT(INPUT_GET,'senhaAtual',FILTER_DEFAULT);
        $senhaRes = DB::select('select email,senha from users where email = :email', ['email' => $email]);
        $senhaBinario = bcrypt($senhaAtual);
        if(!empty($senhaRes) && $senhaRes[0]->senha == $senhaBinario):
            echo "true";
        else:
            echo "false";
        endif;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request){
        $input = $request->all();
        $user = new User();
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->status = $input['status'];
        $user->save();
        return $user->id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id){
        $input = $request->all();

        $user = \App\User::find($id);
        $user->id = $id;
        $user->email = $input['email'];
        if(!empty($input['password']) && $input['password'] == $input['confirmarPassword']):
            $user->password = bcrypt($input['password']);
        endif;
        $user->status = $input['status'];
        $user->save();
        return $user->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id){
        \App\User::where('id',$id)->delete();
    }

}
