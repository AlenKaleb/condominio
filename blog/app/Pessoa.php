<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

abstract class Pessoa extends Model
{

    protected $nome;
    protected $cpf;
    protected $rg;
    protected $email;
    protected $telefone;

    public function getEmail(){
        $this->email = new Email();
        return $this->email;
    }

    public function getTelefone(){
        $this->telefone = new Telefone();
        return $this->telefone;
    }
}
