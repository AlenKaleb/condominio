<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Apartamento extends Model
{
    protected $fillable = ['id','bloco_id','numero','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'apartamentos';
    protected $primaryKey = 'id';

    public function bloco(){
        return $this->belongsTo('\App\Bloco','bloco_id');
    }

    public  function morador(){
        return $this->hasMany('\App\Morador','apartamento_id');
    }

    public function moradorExist(){
        $apartamento = FILTER_INPUT(INPUT_GET,'apartamento',FILTER_DEFAULT);
        $apartamentoAntigo = FILTER_INPUT(INPUT_GET,'apartamentoAntigo',FILTER_DEFAULT);
        $morador = \App\Morador::select('moradors.id')->where('moradors.apartamento_id',$apartamento)->count() > 0;
        if($morador == 0 || $apartamentoAntigo == $apartamento):
            echo "true";
        else:
            echo "false";
        endif;
    }

    public function index(){
        try{
            $apartamento = FILTER_INPUT(INPUT_GET,'apartamento',FILTER_DEFAULT);
            $bloco = FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT);
            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
            $conjunto = FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT);
            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
//            dd(session('dataFuncionario'));
            return $apartamentos = \App\Apartamento::select('apartamentos.id','apartamentos.numero','apartamentos.bloco_id','blocos.bloco','lotes.conjunto_id','conjuntos.quadra_id','conjuntos.conjunto','quadras.quadra','quadras.condominio_id','condominios.nome')->leftJoin('blocos','blocos.id','=','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','=','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','=','conjuntos.quadra_id')->leftJoin('condominios','quadras.condominio_id','=','condominios.id')->where('apartamentos.numero','LIKE',"%{$apartamento}%")->where('blocos.bloco','LIKE',"%{$bloco}%")->where('conjuntos.conjunto','LIKE',"%{$conjunto}%")->where('quadras.quadra','LIKE',"%{$quadra}%")->where('condominios.nome','LIKE',"%{$condominio}%")->orderBy('apartamentos.id','desc')->paginate(10);
        }catch (PDOException $e){
            return $index = base64_encode('false');
        }
    }

    public function blocos(){
        $bloco_id = (FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT):null);
        $apartamentoCondominio = FILTER_INPUT(INPUT_GET,'apartamentoCondominio',FILTER_DEFAULT);
        $apartamentos = \App\Apartamento::select('id','numero')->where('bloco_id',$bloco_id)->get();
        $apartamentoArr[0] = '<option selected value=""> Selecione um bloco </option>';
        foreach ($apartamentos as $key => $apartamento):
            $key++;
            if($apartamentoCondominio == $apartamento->id):
                $apartamentoArr[$key] = '<option selected value="' . $apartamento->id . '">' . $apartamento->numero . '</option>';
            else:
                $apartamentoArr[$key] = '<option value="' . $apartamento->id . '">' . $apartamento->numero . '</option>';
            endif;
        endforeach;
        return $apartamentoArr;
    }

    public function criar(){
        $condominios = \App\Condominio::select('id','nome')->get();
        $funcionarios = \App\Funcionario::select('id','nome')->get();
        return ['condominios' => $condominios,'funcionarios' => $funcionarios];
    }

    public function cadastrar(Request $request){
        $input = $request->all();
        if(is_array($input['apartamento'])):
            foreach($input['apartamento'] as $inputApartamento):
                if(!empty($inputApartamento)):
                    $apartamento = new Apartamento();
                    $apartamento->bloco_id = $input['bloco_id'];
                    $apartamento->numero = $inputApartamento;
                    $apartamento->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['apartamento']);
            try {
                $apartamento = new Apartamento();
                $apartamento->bloco_id = $input['bloco'];
                $apartamento->numero = $input['apartamento'];
                $apartamento->save();
                $cadastrar = base64_encode('true');
            }catch (PDOException $e){
                $cadastrar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
        endif;
    }

    public function editar($id){
        $funcionarios = \App\Funcionario::select('id','nome')->get();
        $condominios = \App\Condominio::select('id','nome')->get();
        $apartamento = \App\Apartamento::select('apartamentos.id','numero','condominio_id','quadra_id','conjunto_id','lote_id','bloco_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('apartamentos.id',$id)->first();
        return ['funcionarios' => $funcionarios,'condominios' => $condominios,'apartamento' => $apartamento];
    }

    public function alterar(Request $request, $id = null)
    {
        $input = $request->all();
        if(is_array($input['apartamento'])):
//            dd($input);
            \App\Apartamento::where('bloco_id',$input['bloco_id'])->delete();
            $this->cadastrar($request);
        else:
            $nomeBase = base64_encode($input['apartamento']);
            try {
                $apartamento = \App\Apartamento::find($id);
                $apartamento->id = $id;
                $apartamento->bloco_id = $input['bloco'];
                $apartamento->numero = $input['apartamento'];
                $apartamento->save();
                $alterar = base64_encode('true');
            }catch (PDOException $e){
                $alterar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'alterar' => $alterar];
        endif;
    }

    public function excluir($id){
        $apartamento = \App\Apartamento::select('numero')->where('id',$id)->first();
        $nomeBase = base64_encode($apartamento->numero);
        try {
            $moradores = \App\Morador::where('apartamento_id',$id)->count();
            if($moradores == 0):
                \App\Apartamento::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }

        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }

}
