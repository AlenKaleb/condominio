<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CondominioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'cnpj' => 'required',
            'cep' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'quadraInicio' => 'required',
            'quadraFim' => 'required',
            'bairro' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nome.required' => 'Campo nome obrigatório',
            'cnpj.required' => 'Campo cnpj obrigatório',
            'cep.required' => 'Campo cep obrigatório',
            'estado.required' => 'Campo estado obrigatório',
            'cidade.required' => 'Campo cidade obrigatório',
            'quadraInicio.required' => 'Campo quadra inicio obrigatório',
            'quadraFim.required' => 'Campo quadra fim obrigatório',
            'bairro.required' => 'Campo bairro obrigatório'
        ];
    }
}
