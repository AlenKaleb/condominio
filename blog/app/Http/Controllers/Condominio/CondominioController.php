<?php

namespace App\Http\Controllers\Condominio;

use App\Condominio;
use App\Http\Controllers\Quadra\QuadraController;
use App\Morador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDOException;

class CondominioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->index();
            if($result['index'] == base64_encode('true')):
                $condominios = $result['condominios'];
                $index = $result['index'];
                return view('painel.condominio.index',compact('condominios','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function condominios(){
        $condominio = new Condominio();
        return $condominio->condominios();
    }

    /**
     * @return mixed
     */
    public function condominioMoradores(){
        $condominio = new Condominio();
        return $condominio->condominioMoradores();
    }

    /**
     * @return mixed
     */
    public function condominioFuncionarios(){
        $condominio = new Condominio();
        return $condominio->condominioFuncionarios();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->criar();
            $paises = $result['paises'];
            $funcionarios = $result['funcionarios'];
            return view('painel.condominio.create',compact('paises','funcionarios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function verificarCnpj(){
        $condominio = new Condominio();
        return $condominio->verificarCnpj();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("condominio/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->editar($id);
            if($result != base64_encode('false')):
                $paises = $result['paises'];
                $condominio = $result['condominio'];
                $dataRegistroFormatado = $result['dataRegistroFormatado'];
                return view('painel.condominio.edit',compact('paises','condominio','dataRegistroFormatado'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("condominio/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $condominio = new Condominio();
            $result = $condominio->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("condominio/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
