<?php

namespace App\Http\Controllers\Telefone;

use App\Telefone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TelefoneController extends Controller
{
    /**
     * @param Request $request
     * @param $usuario_id
     * @param $usuario_tipo
     */
    public function store(Request $request,$usuario_id,$usuario_tipo)
    {
       $telefone = new Telefone();
       $telefone->cadastrar($request,$usuario_id,$usuario_tipo);
    }

    /**
     * @param Request $request
     * @param $usuario_id
     * @param $usuario_tipo
     * @throws \Exception
     */
    public function update(Request $request, $usuario_id,$usuario_tipo)
    {
        $telefone = new Telefone();
        $telefone->alterar($request, $usuario_id,$usuario_tipo);
    }

    /**
     * @param $usuario_id
     * @param $usuario_tipo
     * @throws \Exception
     */
    public function destroy($usuario_id,$usuario_tipo)
    {
        $telefone = new Telefone();
        $telefone->excluir($usuario_id,$usuario_tipo);
    }
}
