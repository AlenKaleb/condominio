<?php

namespace App\Http\Controllers\Conjunto;

use App\Conjunto;
use App\Http\Controllers\Lote\LoteController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConjuntoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $conjunto = new Conjunto();
            $result = $conjunto->index();
            if($result['index'] == base64_encode('true')):
                $conjuntos = $result['conjuntos'];
                $index = $result['index'];
                return view('painel.conjunto.index',compact('conjuntos','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function conjuntos(){
        $conjunto = new Conjunto();
        return $conjunto->conjuntos();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $conjunto = new Conjunto();
            $result = $conjunto->criar();
            $condominios = $result['condominios'];
            return view('painel.conjunto.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $conjunto = new Conjunto();
            $result = $conjunto->cadastrar($request);
            if(!is_array($input['conjunto'])):
                $nomeBase = $result['nomeC'];
                $cadastrar = $result['cadastrar'];
                return redirect("conjunto/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $conjunto = new Conjunto();
            $result = $conjunto->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $conjunto = $result['conjunto'];
                return view('painel.conjunto.edit',compact('condominios','conjunto'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Conjunto  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $conjunto = new Conjunto();
            $result = $conjunto->alterar($request,$id);
            if(!is_array($input['conjunto'])):
                $nomeBase = $result['nomeC'];
                $alterar = $result['alterar'];
                return redirect("conjunto/index?nomeC={$nomeBase}&alterar={$alterar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $conjunto = new Conjunto();
            $result = $conjunto->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("conjunto/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
