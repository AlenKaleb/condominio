<?php

namespace App\Http\Controllers\Apartamento;

use App\Apartamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDOException;

class ApartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $apartamento = new Apartamento();
            if(!empty($index) && $index == 'false'){
                redirect("/inicio?index={$index}");
            }else{
                $apartamentos = $apartamento->index();
                return view('painel.apartamento.index',compact('apartamentos'));
            }
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function bloco(){
        $apartamento = new Apartamento();
        return $apartamento->bloco();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $apartamento = new Apartamento();
            $querys =  $apartamento->criar();
            $condominios = $querys['condominios'];
            return view('painel.apartamento.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function moradorExist(){
        $apartamento = new Apartamento();
        return $apartamento->moradorExist();
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $apartamento = new Apartamento();
            $ressult = $apartamento->cadastrar($request);
            return redirect("apartamento/index?nomeC={$ressult['nomeC']}&cadastrar={$ressult['cadastrar']}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $apartamentoModel = new Apartamento();
            try{
                $querys = $apartamentoModel->editar($id);
                $condominios = $querys['condominios'];
                $apartamento = $querys['apartamento'];
                return view('painel.apartamento.edit',compact('condominios','apartamento'));
            }catch (PDOException $e){
                $index = base64_encode('false');
                return redirect("/inicio?index={$index}");
            }
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Apartamento  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $apartamento = new Apartamento();
            $result = $apartamento->alterar($request,$id);
            if(!is_array($input['apartamento'])):
                return redirect("apartamento/index?nomeC={$result['nomeC']}&alterar={$result['alterar']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $apartamento = new Apartamento();
            $result = $apartamento->excluir($id);
            return redirect("apartamento/index?nomeC={$result['nomeC']}&excluir={$result['excluir']}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
