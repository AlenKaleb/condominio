<?php

namespace App\Http\Controllers\Notifica;

use App\Notifica;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificaController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $notifica = new Notifica();
            $result = $notifica->index();
            if($result['index'] == base64_encode('true')):
                $notificas = $result['notificas'];
                $index = $result['index'];
                return view('painel.notifica.index',compact('notificas','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notifica  $notifica
     * @return \Illuminate\Http\Response
     */
    public function show(Notifica $notifica)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $notifica = new Notifica();
            $result = $notifica->criar();
            $condominios = $result['condominios'];
            return view('painel.notifica.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $notifica = new Notifica();
            $result = $notifica->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("notifica/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $bloco = new Notifica();
            $result = $bloco->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $notifica = $result['notifica'];
                $dataRegistro = $result['dataRegistro'];
                return view('painel.notifica.edit',compact('condominios','notifica','dataRegistro'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $notifica = new Notifica();
            $result = $notifica->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("notifica/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('notifica')):
            $notifica = new Notifica();
            $result = $notifica->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("notifica/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
