<?php

namespace App\Http\Controllers\Bloco;

use App\Bloco;
use App\Http\Controllers\Apartamento\ApartamentoController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlocoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $bloco = new Bloco();
            $result = $bloco->index();
            if($result['index'] == base64_encode('true')):
                $blocos = $result['blocos'];
                $index = $result['index'];
                return view('painel.bloco.index',compact('blocos','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function blocos(){
        $bloco = new Bloco();
        return $bloco->blocos();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bloco = new Bloco();
        $result = $bloco->criar();
        $condominios = $result['condominios'];
        return view('painel.bloco.create',compact('condominios'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $bloco = new Bloco();
            $result = $bloco->cadastrar($request);
            if(!is_array($input['bloco'])):
                $nomeBase = $result['nomeC'];
                $cadastrar = $result['cadastrar'];
                return redirect("bloco/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $bloco = new Bloco();
            $result = $bloco->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $bloco = $result['bloco'];
                return view('painel.bloco.edit',compact('condominios','bloco'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bloco  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $bloco = new Bloco();
            $result = $bloco->alterar($request,$id);
            if(!is_array($input['bloco'])):
                $nomeBase = $result['nomeC'];
                $alterar = $result['alterar'];
                return redirect("bloco/index?nomeC={$nomeBase}&alterar={$alterar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $bloco = new Bloco();
            $result = $bloco->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("bloco/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
