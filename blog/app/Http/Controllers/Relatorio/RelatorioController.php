<?php

namespace App\Http\Controllers\Relatorio;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Custo\CustoController;
use App\Http\Controllers\Recebimento\RecebimentoController;

class RelatorioController extends Controller
{

    public function diferenca(){
        $ano = date('Y');
        return DB::table('recebimentos,item_custos')->select('MONTH(data) AS mes','SUM(recebimentos.valor) - SUM(item_custos.valor) AS diferenca')->where('MONTH(recebimentos.data)','=','MONTH(item_custos.data_vencimento)')->where('item_custos.data_vencimento','LIKE',"{$ano}-%-%")->groupby(DB::raw('MONTH(item_custos.data_vencimento)'))
            ->get();
    }

    public function financeiro(){
        $custoController = new CustoController();
        $custos = $custoController->custoAnual();
        $recebimentoController = new RecebimentoController();
        $recebimentos = $recebimentoController->recebimentoAnual();
        return view('painel.relatorio.financeiro',compact('custos','recebimentos'));
    }


    public function recebimento(){
        $recebimentoController = new RecebimentoController();
        $recebimentos = $recebimentoController->recebimentoAnual();
        return view('painel.relatorio.recebimento',compact('recebimentos'));
    }
}
