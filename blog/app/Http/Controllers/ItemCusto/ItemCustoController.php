<?php

namespace App\Http\Controllers\ItemCusto;

use App\ItemCusto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemCustoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itemCusto = new ItemCusto();
        $itemCusto->cadastrar($request);
    }

    /**
     * @param Request $request
     * @param ItemCusto $itemCusto
     */
    public function update(Request $request, $custo_id)
    {
        $itemCusto = new ItemCusto();
        $itemCusto->alterar($request,$custo_id);
    }

    /**
     * @param $custo_id
     */
    public function destroy($custo_id)
    {
        $itemCusto = new ItemCusto();
        $itemCusto->excluir($custo_id);
    }
}
