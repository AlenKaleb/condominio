<?php

namespace App\Http\Controllers\Cidade;

use App\Cidade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CidadeController extends Controller
{
    public function cidades(){
        $codigo_Estado = (FILTER_INPUT(INPUT_GET,'estado',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'estado',FILTER_DEFAULT):null);
        $cidadeEndereco = FILTER_INPUT(INPUT_GET,'cidadeEndereco',FILTER_DEFAULT);
        $cidades = \App\Cidade::where('estado_id',$codigo_Estado)->get();
        $cidadeArr[0] = '<option selected value=""> Selecione uma cidade </option>';
        foreach ($cidades as $key => $cidade):
            if($cidadeEndereco == $cidade->id):
                $cidadeArr[$key] = '<option selected value="' . $cidade->id . '">' . $cidade->nome . '</option>';
            else:
                $cidadeArr[$key] = '<option value="' . $cidade->id . '">' . $cidade->nome . '</option>';
            endif;
        endforeach;
        return $cidadeArr;
    }
}
