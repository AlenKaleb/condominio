<?php

namespace App\Http\Controllers\Multa;

use App\Multa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $multa = new Multa();
            $result = $multa->index();
            if($result['index'] == base64_encode('true')):
                $multas = $result['multas'];
                $index = $result['index'];
                return view('painel.multa.index',compact('multas','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $multa = new Multa();
            $result = $multa->criar();
            $condominios = $result['condominios'];
            return view('painel.multa.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $multa = new Multa();
            $result = $multa->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("multa/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $bloco = new Multa();
            $result = $bloco->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $multa = $result['multa'];
                $dataRegistro = $result['dataRegistro'];
                $dataPagamento = $result['dataPagamento'];
                return view('painel.multa.edit',compact('condominios','multa','dataRegistro','dataPagamento'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $multa = new Multa();
            $result = $multa->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("multa/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('multa')):
            $multa = new Multa();
            $result = $multa->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("multa/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
