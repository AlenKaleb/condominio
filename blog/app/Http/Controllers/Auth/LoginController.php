<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
   |--------------------------------------------------------------------------
   | Login Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles authenticating users for the application and
   | redirecting them to your home screen. The controller uses a trait
   | to conveniently provide its functionality to your applications.
   |
   */

    use AuthenticatesUsers;

    /**
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        $email = FILTER_INPUT(INPUT_POST,'email',FILTER_DEFAULT);
        $password = FILTER_INPUT(INPUT_POST,'password',FILTER_DEFAULT);
        $usuario = \App\User::where('email',$email)->where('status','A')->first();
//        dd($usuario);
        if(!empty($usuario->id)):
//            dd($usuario->funcionario->cargo);
//            die();
            if (Auth::attempt(['email' => $email, 'password' => $password])):
                session_start();
                session(['usuario' => $usuario]);
                if(!empty($usuario->funcionario)){
                    session(['dataFuncionario' => $usuario->funcionario]);
//                    dd($usuario->funcionario);
                    if($usuario->funcionario->cargo == "P"):
                        $permissao = ['condominio' => false,'morador' => false,'multa' => false,'recebimento' => false,'problema' => false,'custo' => false,'funcionario' => false,'visitante' => true,'taxa' => false,'notifica' => false,'relatorio' => false];
                    elseif($usuario->funcionario->cargo == "S"):
                        $permissao = ['condominio' => false,'morador' => true,'multa' => true,'recebimento' => true,'problema' => true,'custo' => true,'funcionario' => false,'visitante' => true,'taxa' => true,'notifica' => true,'relatorio' => true];
                    elseif($usuario->funcionario->cargo == "A"):
                        $permissao = ['condominio' => true,'morador' => true,'multa' => true,'recebimento' => true,'problema' => true,'custo' => true,'funcionario' => true,'visitante' => true,'taxa' => true,'notifica' => true,'relatorio' => true];
                    endif;
                    session(['id' => $usuario->funcionario->id]);
                    session(['tipo' => 'F']);
                }else{
                    session(['id' => $usuario->morador->id]);
                    session(['tipo' => 'M']);
                    $permissao = ['condominio' => false,'morador' => false,'multa' => false,'recebimento' => ['visualizar' => true],'problema' => false,'custo' => false,'funcionario' => false,'visitante' => true,'taxa' => false,'notifica' => false,'relatorio' => false];
                }

                session(['condominio' => $permissao['condominio']]);
                session(['morador' => $permissao['morador']]);
                session(['multa' => $permissao['multa']]);
                session(['recebimento' => $permissao['recebimento']]);
                session(['problema' => $permissao['problema']]);
                session(['custo' => $permissao['custo']]);
                session(['funcionario' => $permissao['funcionario']]);
                session(['visitante' => $permissao['visitante']]);
                session(['taxa' => $permissao['taxa']]);
                session(['notifica' => $permissao['notifica']]);
                session(['relatorio' => $permissao['relatorio']]);

                return redirect()->intended('/inicio');
            else:
                return Redirect::to('/login')->withErrors(array('password' => 'Ops! Parece que a senha está incorreta'))->withInput(Input::except('password'));
            endif;
        else:
            return Redirect::to('/login')->withErrors(array('inativo' => 'Ops! Esta conta está inativa'))->withInput(Input::except('inativo'));
        endif;
    }

    public function logout(){
        session_start();
        session(['usuario' => null]);
        session(['tipo' => null]);
        session(['condominio' => null]);
        session(['morador' => null]);
        session(['multa' => null]);
        session(['recebimento' => null]);
        session(['problema' => null]);
        session(['custo' => null]);
        session(['funcionario' => null]);
        session(['visitante' => null]);
        session(['taxa' => null]);
        session(['notifica' => null]);
        session(['relatorio' => null]);

        Auth::logout();
        return redirect('/login');
    }
}
