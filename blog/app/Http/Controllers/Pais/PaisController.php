<?php

namespace App\Http\Controllers\Pais;

use App\Pais;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function paises(){
        $codigo_Pais = (FILTER_INPUT(INPUT_GET,'pais',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'pais',FILTER_DEFAULT):null);
        $estadoEndereco = FILTER_INPUT(INPUT_GET,'estadoEndereco',FILTER_DEFAULT);
        $estados = \App\Estado::where('pais_id',$codigo_Pais)->get();
        $estadoArr[0] = '<option selected value=""> Selecione um estado </option>';
        foreach ($estados as $key => $estado):
            $key++;
            if($estadoEndereco == $estado->id):
                $estadoArr[$key] = '<option selected value="' . $estado->id . '">' . $estado->nome . '</option>';
            else:
                $estadoArr[$key] = '<option value="' . $estado->id . '">' . $estado->nome . '</option>';
            endif;
        endforeach;
        return $estadoArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function show(Pais $pais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function edit(Pais $pais)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pais $pais)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pais  $pais
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pais $pais)
    {
        //
    }
}
