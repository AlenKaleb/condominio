<?php

namespace App\Http\Controllers\Custo;

use App\Custo;
use App\Http\Controllers\ItemCusto\ItemCustoController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustoController extends Controller
{
    public function custoAnual(){
        $custo = new Custo();
        return $custo->custosAnual();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $custo = new Custo();
            $result = $custo->index();
            if($result['index'] == base64_encode('true')):
                $custos = $result['custos'];
                $index = $result['index'];
                return view('painel.custo.index',compact('custos','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $condominio = new Custo();
            $result = $condominio->criar();
            $condominios = $result['condominios'];
            return view('painel.custo.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $custo = new Custo();
            $result = $custo->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("custo/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $custo = new Custo();
            $result = $custo->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $custo = $result['custo'];
                $itensCusto = $result['itensCusto'];
                $dataRegistro = $result['dataRegistro'];
                return view('painel.custo.edit',compact('condominios','custo','itensCusto','dataRegistro'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $custo = new Custo();
            $result = $custo->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("custo/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('custo')):
            $custo = new Custo();
            $result = $custo->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("custo/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
