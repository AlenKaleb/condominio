<?php

namespace App\Http\Controllers\Morador;

use App\Http\Controllers\Telefone\TelefoneController;
use App\Http\Controllers\User\UserController;
use App\Morador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MoradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->index();
            if($result['index'] == base64_encode('true')):
                $moradores = $result['moradores'];
                $index = $result['index'];
                return view('painel.morador.index',compact('moradores','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function verificarCpf(){
        $morador = new Morador();
        return $morador->verificarCpf();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->criar();
            $condominios = $result['condominios'];
            return view('painel.morador.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("morador/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $morador = $result['morador'];
                $index = $result['index'];
                return view('painel.morador.edit',compact('condominios','morador','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("morador/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('morador')):
            $morador = new Morador();
            $result = $morador->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("morador/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
