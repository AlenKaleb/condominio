<?php

namespace App\Http\Controllers\Quadra;

use App\Http\Controllers\Conjunto\ConjuntoController;
use App\Quadra;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuadraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $quadra = new Quadra();
            $result = $quadra->index();
            if($result['index'] == base64_encode('true')):
                $quadras = $result['quadras'];
                $index = $result['index'];
                return view('painel.quadra.index',compact('quadras','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function quadras(){
        $quadra = new Quadra();
        return $quadra->quadras();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $quadra = new Quadra();
            $result = $quadra->criar();
            $condominios = $result['condominios'];
            return view('painel.quadra.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $quadra = new Quadra();
            $result = $quadra->cadastrar($request);
            if(!is_array($input['quadra'])):
                $nomeBase = $result['nomeC'];
                $cadastrar = $result['cadastrar'];
                return redirect("quadra/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $quadra = new Quadra();
            $result = $quadra->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $quadra = $result['quadra'];
                return view('painel.quadra.edit',compact('condominios','quadra'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quadra  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $quadra = new Quadra();
            $result = $quadra->alterar($request,$id);
            if(!is_array($input['quadra'])):
                $nomeBase = $result['nomeC'];
                $alterar = $result['alterar'];
                return redirect("quadra/index?nomeC={$nomeBase}&alterar={$alterar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $quadra = new Quadra();
            $result = $quadra->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("quadra/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
