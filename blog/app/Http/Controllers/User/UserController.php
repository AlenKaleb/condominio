<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * @return bool
     */
    public function emailExist(){
        $user = new User();
        return $user->emailExist();
    }

    /**
     *
     */
    public function validatePassword(){
        $user = new User();
        return $user->validatePassword();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $user = new User();
        return $user->cadastrar($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $user = new User();
        return $user->alterar($request,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $user = new User();
        return $user->excluir($id);
    }

}
