<?php

namespace App\Http\Controllers\Lote;

use App\Http\Controllers\Bloco\BlocoController;
use App\Lote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $lote = new Lote();
            $result = $lote->index();
            if($result['index'] == base64_encode('true')):
                $lotes = $result['lotes'];
                $index = $result['index'];
                return view('painel.lote.index',compact('lotes','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @return mixed
     */
    public function lotes(){
        $lote = new Lote();
        return $lote->lotes();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $lote = new Lote();
            $result = $lote->criar();
            $condominios = $result['condominios'];
            return view('painel.lote.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $lote = new Lote();
            $result = $lote->cadastrar($request);
            if(!is_array($input['lote'])):
                $nomeBase = $result['nomeC'];
                $cadastrar = $result['cadastrar'];
                return redirect("lote/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $lote = new Lote();
            $result = $lote->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $lote = $result['lote'];
                return view('painel.lote.edit',compact('condominios','lote'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lote  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $input = $request->all();
            $lote = new Lote();
            $result = $lote->alterar($request,$id);
            if(!is_array($input['lote'])):
                $nomeBase = $result['nomeC'];
                $alterar = $result['alterar'];
                return redirect("lote/index?nomeC={$nomeBase}&alterar={$alterar}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('condominio')):
            $lote = new Lote();
            $result = $lote->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("lote/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
