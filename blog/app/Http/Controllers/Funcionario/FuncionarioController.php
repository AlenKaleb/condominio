<?php

namespace App\Http\Controllers\Funcionario;

use App\Funcionario;
use App\Http\Controllers\Telefone\TelefoneController;
use App\Http\Controllers\User\UserController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FuncionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->index();
            if($result['index'] == base64_encode('true')):
                $funcionarios = $result['funcionarios'];
                $index = $result['index'];
                return view('painel.funcionario.index',compact('funcionarios','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->criar();
            $condominios = $result['condominios'];
            return view('painel.funcionario.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function verificarCpf(){
        $funcionario = new Funcionario();
        return $funcionario->verificarCpf();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("funcionario/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $funcionario = $result['funcionario'];
                $salario = $result['salario'];
                $index = $result['index'];
                return view('painel.funcionario.edit',compact('condominios','funcionario','salario','index'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("funcionario/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('funcionario')):
            $funcionario = new Funcionario();
            $result = $funcionario->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("funcionario/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
