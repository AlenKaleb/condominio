<?php

namespace App\Http\Controllers\Taxa;

use App\Taxa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaxaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->index();
            if($result['index'] == base64_encode('true')):
                $taxas = $result['taxas'];
                $index = $result['index'];
                return view('painel.taxa.index',compact('taxas','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->criar();
            $condominios = $result['condominios'];
            return view('painel.taxa.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("taxa/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $taxa = $result['taxa'];
                $valor = $result['valor'];
                $dataRegistro = $result['dataRegistro'];
                return view('painel.taxa.edit',compact('condominios','taxa','valor','dataRegistro'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("taxa/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if($restricaoAcesso->validarPermissao('taxa')):
            $taxa = new Taxa();
            $result = $taxa->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("taxa/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
