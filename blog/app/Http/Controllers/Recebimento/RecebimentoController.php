<?php

namespace App\Http\Controllers\Recebimento;

use App\Recebimento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecebimentoController extends Controller
{
    public function recebimentoAnual(){
        $recebimento = new Recebimento();
        return $recebimento->recebimentosAnual();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento')) || $restricaoAcesso->validarPermissao('recebimento')['visualizar']):
            $recebimento = new Recebimento();
            $result = $recebimento->index();
            if($result['index'] == base64_encode('true')):
                $recebimentos = $result['recebimentos'];
                $index = $result['index'];
                return view('painel.recebimento.index',compact('recebimentos','index'));
            else:
                return redirect("/inicio?index={$result['index']}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento'))):
            $recebimento = new Recebimento();
            $result = $recebimento->criar();
            $condominios = $result['condominios'];
            return view('painel.recebimento.create',compact('condominios'));
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento'))):
            $recebimento = new Recebimento();
            $result = $recebimento->cadastrar($request);
            $nomeBase = $result['nomeC'];
            $cadastrar = $result['cadastrar'];
            return redirect("recebimento/index?nomeC={$nomeBase}&cadastrar={$cadastrar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    public function edit($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento'))):
            $recebimento = new Recebimento();
            $result = $recebimento->editar($id);
            if($result != base64_encode('false')):
                $condominios = $result['condominios'];
                $recebimento = $result['recebimento'];
                $dataRegistro = $result['dataRegistro'];
                $data = $result['data'];
                $valor = $result['valor'];
                return view('painel.recebimento.edit',compact('condominios','recebimento','dataRegistro','data','valor'));
            else:
                return redirect("/inicio?index={$result}");
            endif;
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento'))):
            $recebimento = new Recebimento();
            $result = $recebimento->alterar($request,$id);
            $nomeBase = $result['nomeC'];
            $alterar = $result['alterar'];
            return redirect("recebimento/index?nomeC={$nomeBase}&alterar={$alterar}");
        else:
            return redirect('/?permissao=false');
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restricaoAcesso = new \App\RestricaoAcesso();
        if(is_bool($restricaoAcesso->validarPermissao('recebimento'))):
            $recebimento = new Recebimento();
            $result = $recebimento->excluir($id);
            $nomeBase = $result['nomeC'];
            $excluir = $result['excluir'];
            return redirect("recebimento/index?nomeC={$nomeBase}&excluir={$excluir}");
        else:
            return redirect('/?permissao=false');
        endif;
    }
}
