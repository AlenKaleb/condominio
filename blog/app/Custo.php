<?php

namespace App;

use App\Http\Controllers\ItemCusto\ItemCustoController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDOException;

class Custo extends Model
{
    protected $fillable = ['id','condominio_id','nome','categoria','data_registro','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'custos';
    protected $primaryKey = 'id';

    public function condominio(){
        return $this->belongsTo('\App\Condominio','condominio_id');
    }

    public function itens(){
        return $this->hasMany('\App\ItemCusto','custo_id');
    }

    public function custosAnual(){
        $ano = date('Y');
        return \App\ItemCusto::select(
            DB::raw('MONTH(data_vencimento) AS mes'),DB::raw('SUM(valor) AS valor'))->where('data_vencimento','LIKE',"{$ano}-%-%")->groupby(DB::raw('MONTH(data_vencimento)'))
            ->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'custo',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('dataFuncionario')->cargo == 'S'):
                $custos = \App\Custo::select('custos.id','condominio_id','custos.nome','categoria','custos.data_registro','condominios.nome AS nomeCondominio','custos.observacao')->leftJoin('condominios','condominios.id','custos.condominio_id')->where('custos.nome','LIKE',"%{$nome}%")->where('custos.data_registro','LIKE',"%{$dataRegistro}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('custos.id','desc')->paginate(10);
            else:
                $custos = \App\Custo::select('custos.id','condominio_id','custos.nome','categoria','custos.data_registro','condominios.nome AS nomeCondominio','custos.observacao')->leftJoin('condominios','condominios.id','custos.condominio_id')->where('custos.nome','LIKE',"%{$nome}%")->where('custos.data_registro','LIKE',"%{$dataRegistro}%")->orderBy('custos.id','desc')->paginate(10);
            endif;

            $index = base64_encode('true');
            return ['custos' => $custos,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->cargo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $custo = new Custo();
            $custo->condominio_id = $input['condominio'];
            $custo->nome = $input['nome'];
            $custo->categoria = $input['categoria'];
            $custo->data_registro = $dataRegistro;
            $custo->observacao = $input['observacao'];
            $custo->save();

            $request->request->add(['custo_id' => $custo->id]);
            $itemCustoController = new ItemCustoController();
            $itemCustoController->store($request);

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $custo = \App\Custo::select('custos.id','condominio_id','custos.nome','categoria','custos.data_registro','custos.observacao')->leftJoin('condominios','condominios.id','custos.condominio_id')->where('custos.id',$id)->first();
            $itensCusto = \App\ItemCusto::select('id','nome','data_vencimento','data_pagamento','numero_documento','forma_pagamento','valor','observacao')->where('custo_id',$custo->id)->orderBy('item_custos.id','desc')->get();
            if(!empty($custo->data_registro)):
                $dataRegistroArr = explode('-',$custo->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;
            return ['condominios' => $condominios,'custo' => $custo,'itensCusto' => $itensCusto,'dataRegistro' => $dataRegistro];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            $custo = \App\Custo::find($id);
            $custo->id = $id;
            $custo->condominio_id = $input['condominio'];
            $custo->nome = $input['nome'];
            $custo->categoria = $input['categoria'];
            $custo->data_registro = $dataRegistro;
            $custo->observacao = $input['observacao'];
            $custo->save();

            $itemCustoController = new ItemCustoController();
            $itemCustoController->update($request,$custo->id);

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $custo = \App\Custo::select('nome')->where('id',$id)->first();
        $nomeBase = base64_encode($custo->nome);
        try {
            $itemCustoController = new ItemCustoController();
            $itemCustoController->destroy($id);

            \App\Custo::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
