<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

final class Email extends Model
{
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'emails';
    protected $primaryKey = 'id';
}
