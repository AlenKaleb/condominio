<?php

namespace App;

use App\Http\Controllers\Telefone\TelefoneController;
use App\Http\Controllers\User\UserController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Morador extends Model
{
    protected $fillable = ['id','user_id','apartamento_id','nome','cpf','rg','sexo','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'moradors';
    protected $primaryKey = 'id';

    public function usuario(){
        return $this->hasOne('\App\User','usuario_id')->where('tipo', 'M');
    }

    public function notificacoes(){
        return $this->hasMany('\App\Notifica','morador_id');
    }

    public function problemas(){
        return $this->hasMany('\App\Problema','morador_id');
    }

    public function recebimentos(){
        return $this->hasMany('\App\Recebimento','morador_id');
    }

    public function multas(){
        return $this->hasMany('\App\Multa','morador_id');
    }

    public function apartamento(){
        return $this->hasOne('\App\Apartamento','apartamento_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'nome',FILTER_DEFAULT);
            $cpf = FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT);
            $rg = FILTER_INPUT(INPUT_GET,'rg',FILTER_DEFAULT);
            if(session('dataFuncionario')->cargo == 'S'):
                $moradores = \App\Morador::select('moradors.id','moradors.nome AS nomeMorador','cpf','sexo','rg','user_id','apartamento_id','email','status','condominios.nome AS nomeCondominio','quadras.quadra','conjuntos.conjunto','lotes.lote','blocos.bloco','apartamentos.numero')->leftJoin('apartamentos','apartamentos.id','moradors.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->leftJoin('users','users.id','moradors.user_id')->where('users.tipo','M')->where('moradors.nome','LIKE',"%{$nome}%")->where('cpf','LIKE',"%{$cpf}%")->where('rg','LIKE',"%{$rg}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('moradors.id','desc')->paginate(10);
            else:
                $moradores = \App\Morador::select('moradors.id','moradors.nome AS nomeMorador','cpf','sexo','rg','user_id','apartamento_id','email','status','condominios.nome AS nomeCondominio','quadras.quadra','conjuntos.conjunto','lotes.lote','blocos.bloco','apartamentos.numero')->leftJoin('apartamentos','apartamentos.id','moradors.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->leftJoin('users','users.id','moradors.user_id')->where('users.tipo','M')->where('moradors.nome','LIKE',"%{$nome}%")->where('cpf','LIKE',"%{$cpf}%")->where('rg','LIKE',"%{$rg}%")->orderBy('moradors.id','desc')->paginate(10);
            endif;
            $index = base64_encode('true');
            return ['moradores' => $moradores,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            return ['condominios' => $condominios];
        }catch (PDOException $e){
            $index = base64_encode('false');;
            return ['index' => $index];
        }
    }

    public function validateCpf($cpf){
        $validar = new ValidaCpfCnpj($cpf);
        if($validar->valida() != 1):
            return false;
        else:
            return true;
        endif;
    }

    public function cpfExist($cpf){
        $cpfExist = \App\Morador::where('cpf',$cpf)->first();
        if(!empty($cpfExist->cpf)):
            return false;
        else:
            return true;
        endif;
    }

    public function verificarCpf(){
        $cpf = FILTER_INPUT(INPUT_GET,'cpf',FILTER_DEFAULT);
        $cpfAntigo = FILTER_INPUT(INPUT_GET,'cpfAntigo',FILTER_DEFAULT);
        if($this->validateCpf($cpf) && $this->cpfExist($cpf) || $cpfAntigo == $cpf):
            echo "true";
        else:
            echo "false";
        endif;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $userController = new UserController();
            $user_id = $userController->store($request);

            $morador = new Morador();
            $morador->user_id = $user_id;
            $morador->apartamento_id = $input['apartamento'];
            $morador->nome = $input['nome'];
            $morador->cpf = $input['cpf'];
            $morador->rg = $input['rg'];
            $morador->sexo = $input['sexo'];
            $morador->save();

            \App\User::where('id',$user_id)->update(['usuario_id' => $morador->id,'tipo' => "M"]);

            $telefoneController = new TelefoneController();
            $telefoneController->store($request,$morador->id,'M');
            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $morador = \App\Morador::select('moradors.id','moradors.nome','cpf','rg','sexo','email','condominio_id','quadra_id','conjunto_id','lote_id','bloco_id','apartamento_id','status')->leftJoin('apartamentos','apartamentos.id','moradors.apartamento_id')->leftJoin('blocos','blocos.id','apartamentos.bloco_id')->leftJoin('lotes','lotes.id','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('moradors.id',$id)->leftJoin('users','users.id','moradors.user_id')->where('users.tipo','M')->where('moradors.id',$id)->first();
            $index = base64_encode('true');
            return ['condominios' => $condominios,'morador' => $morador,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['nome']);
        try {
            $morador = \App\Morador::find($id);
            $morador->id = $id;
            $morador->apartamento_id = $input['apartamento'];
            $morador->nome = $input['nome'];
            $morador->cpf = $input['cpf'];
            $morador->rg = $input['rg'];
            $morador->sexo = $input['sexo'];
            $morador->save();

            $telefoneController = new TelefoneController();
            $telefoneController->update($request,$morador->id,'M');

            $userController = new UserController();
            $userController->update($request,$morador->user_id);
            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function excluir($id)
    {
        $morador = \App\Morador::select('id','nome','user_id')->where('id',$id)->first();
        $nomeBase = base64_encode($morador->nome);
        try {
            $telefoneController = new TelefoneController();
            $telefoneController->destroy($morador->id,'M');

            \App\Morador::where('id',$morador->id)->delete();

            $userController = new UserController();
            $userController->destroy($morador->user_id);

            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }

}
