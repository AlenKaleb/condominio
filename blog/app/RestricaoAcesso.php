<?php
/**
 * Created by AK Projetos.
 * User: Alen Kaleb
 * Date: 22/02/2017
 * Time: 12:20
 */

namespace App;


class RestricaoAcesso
{

    public function validarPermissao($nomeVariavel){
        return session($nomeVariavel);
    }

}