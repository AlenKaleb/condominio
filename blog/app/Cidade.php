<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $fillable = ['id','estado_id','nome','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'cidades';
    protected $primaryKey = 'id';

    public function estado(){
        return $this->belongsTo('\App\Estado','estado_id');
    }

}
