<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ItemCusto extends Model
{
    protected $fillable = ['id','custo_id','data_vencimento','data_pagamento','numero_documento','valor','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'item_custos';
    protected $primaryKey = 'id';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();

        foreach($input['numeroDocumentoItem'] as $key => $numeroDocumento):
            if(!empty($numeroDocumento)):
                if(!empty($input['dataVencimentoItem'][$key])):
                    $dataVencimentoArr = explode('/',$input['dataVencimentoItem'][$key]);
                    $dataVencimento = "{$dataVencimentoArr[2]}-{$dataVencimentoArr[1]}-{$dataVencimentoArr[0]}";
                endif;

                if(!empty($input['dataPagamentoItem'][$key])):
                    $dataPagamentoArr = explode('/',$input['dataPagamentoItem'][$key]);
                    $dataPagamento = "{$dataPagamentoArr[2]}-{$dataPagamentoArr[1]}-{$dataPagamentoArr[0]}";
                endif;

                if(!empty($input['valorItem'][$key])):
                    $valor = str_replace(array('R$','.',','),array('','','.'),$input['valorItem'][$key]);
                else:
                    $valor = null;
                endif;

                $itemCusto = new ItemCusto();
                $itemCusto->custo_id = $input['custo_id'];
                $itemCusto->nome = $input['nomeItem'][$key];
                $itemCusto->data_vencimento = $dataVencimento;
                $itemCusto->data_pagamento = $dataPagamento;
                $itemCusto->numero_documento = $input['numeroDocumentoItem'][$key];
                $itemCusto->forma_pagamento = $input['formaPagamentoItem'][$key];
                $itemCusto->valor = $valor;
                $itemCusto->observacao = $input['observacaoItem'][$key];
                $itemCusto->save();
            endif;
        endforeach;
    }

    /**
     * @param Request $request
     * @param ItemCusto $itemCusto
     */
    public function alterar(Request $request, $custo_id)
    {
        $input = $request->all();

        $this->excluir($custo_id);
        foreach($input['numeroDocumentoItem'] as $key => $numeroDocumento):
            if(!empty($numeroDocumento)):
                if(!empty($input['dataVencimentoItem'][$key])):
                    $dataVencimentoArr = explode('/',$input['dataVencimentoItem'][$key]);
                    $dataVencimento = "{$dataVencimentoArr[2]}-{$dataVencimentoArr[1]}-{$dataVencimentoArr[0]}";
                endif;

                if(!empty($input['dataPagamentoItem'][$key])):
                    $dataPagamentoArr = explode('/',$input['dataPagamentoItem'][$key]);
                    $dataPagamento = "{$dataPagamentoArr[2]}-{$dataPagamentoArr[1]}-{$dataPagamentoArr[0]}";
                endif;

                if(!empty($input['valorItem'][$key])):
                    $valor = str_replace(array('R$','.',','),array('','','.'),$input['valorItem'][$key]);
                else:
                    $valor = null;
                endif;

                $itemCusto = new ItemCusto();
                $itemCusto->custo_id = $custo_id;
                $itemCusto->nome = $input['nomeItem'][$key];
                $itemCusto->data_vencimento = $dataVencimento;
                $itemCusto->data_pagamento = $dataPagamento;
                $itemCusto->numero_documento = $input['numeroDocumentoItem'][$key];
                $itemCusto->forma_pagamento = $input['formaPagamentoItem'][$key];
                $itemCusto->valor = $valor;
                $itemCusto->observacao = $input['observacaoItem'][$key];
                $itemCusto->save();
            endif;
        endforeach;
    }

    /**
     * @param $custo_id
     */
    public function excluir($custo_id)
    {
        \App\ItemCusto::select('id')->where('custo_id',$custo_id)->delete();
    }
}
