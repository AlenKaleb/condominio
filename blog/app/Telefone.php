<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Telefone extends Model
{
    protected $fillable = ['id','telefone','usuario_id','usuario_tipo','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'telefones';
    protected $primaryKey = 'id';
    /**
     * @param Request $request
     * @param $usuario_id
     * @param $usuario_tipo
     */
    public function cadastrar(Request $request,$usuario_id,$usuario_tipo)
    {
        $input = $request->all();
        foreach($input['telefone'] as $inputTelefone):
            if(!empty($inputTelefone)):
                $telefone = new Telefone();
                $telefone->usuario_id = $usuario_id;
                $telefone->usuario_tipo = $usuario_tipo;
                $telefone->telefone = $inputTelefone;
                $telefone->save();
            endif;
        endforeach;
    }

    /**
     * @param Request $request
     * @param $usuario_id
     * @param $usuario_tipo
     * @throws \Exception
     */
    public function alterar(Request $request, $usuario_id,$usuario_tipo)
    {
        $this->destroy($usuario_id,$usuario_tipo);
        $input = $request->all();
        foreach($input['telefone'] as $inputTelefone):
            if(!empty($inputTelefone)):
                $telefone = new Telefone();
                $telefone->usuario_id = $usuario_id;
                $telefone->usuario_tipo = $usuario_tipo;
                $telefone->telefone = $inputTelefone;
                $telefone->save();
            endif;
        endforeach;
    }

    /**
     * @param $usuario_id
     * @param $usuario_tipo
     * @throws \Exception
     */
    public function excluir($usuario_id,$usuario_tipo)
    {
        \App\Telefone::where('usuario_id',$usuario_id)->where('usuario_tipo',$usuario_tipo)->delete();
    }

}
