<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Multa extends Model
{
    protected $fillable = ['id','condominio_id','morador_id','titulo','numero_documento','forma_pagamento','data_registro','data_pagamento','valor','motivo','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'multas';
    protected $primaryKey = 'id';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $nome = FILTER_INPUT(INPUT_GET,'multa',FILTER_DEFAULT);
            $dataRegistro = FILTER_INPUT(INPUT_GET,'data_registro',FILTER_DEFAULT);
            $valor = FILTER_INPUT(INPUT_GET,'valor',FILTER_DEFAULT);
            if(!empty($dataRegistro)):
                $dataRegistroArr = explode('/',$dataRegistro);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            endif;
            if(session('dataFuncionario')->cargo == 'S'):
                $multas = \App\Multa::select('multas.id','titulo','numero_documento','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','multas.data_registro','data_pagamento','forma_pagamento','valor','motivo')->leftJoin('condominios','condominios.id','multas.condominio_id')->leftJoin('moradors','moradors.id','multas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('multas.data_registro','LIKE',"%{$dataRegistro}%")->where('valor','LIKE',"%{$valor}%")->where('condominios.id',session('dataFuncionario')->condominio_id)->orderBy('multas.id','desc')->paginate(10);
            else:
                $multas = \App\Multa::select('multas.id','titulo','numero_documento','condominio_id','condominios.nome AS nomeCondominio','moradors.id AS idMorador','moradors.nome AS nomeMorador','multas.data_registro','data_pagamento','forma_pagamento','valor','motivo')->leftJoin('condominios','condominios.id','multas.condominio_id')->leftJoin('moradors','moradors.id','multas.morador_id')->where('titulo','LIKE',"%{$nome}%")->where('multas.data_registro','LIKE',"%{$dataRegistro}%")->where('valor','LIKE',"%{$valor}%")->orderBy('multas.id','desc')->paginate(10);
            endif;

            $index = base64_encode('true');
            return ['multas' => $multas,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        if(session('dataFuncionario')->cargo == 'S'):
            $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
        else:
            $condominios = \App\Condominio::select('id','nome')->get();
        endif;
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input =  $request->all();
        $nomeBase = base64_encode($input['titulo']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            if(!empty($input['dataPagamento'])):
                $dataPagamentoArr = explode('/',$input['dataPagamento']);
                $dataPagamento = "{$dataPagamentoArr[2]}-{$dataPagamentoArr[1]}-{$dataPagamentoArr[0]}";
            else:
                $dataPagamento = null;
            endif;
            $multa = new Multa();
            $multa->condominio_id = $input['condominio'];
            $multa->morador_id = $input['morador'];
            $multa->data_registro = $dataRegistro;
            $multa->data_pagamento = $dataPagamento;
            $multa->titulo = $input['titulo'];
            $multa->numero_documento = $input['numeroDocumento'];
            $multa->forma_pagamento = $input['formaPagamento'];
            $multa->valor = $input['valor'];
            $multa->motivo = $input['motivo'];
            $multa->save();

            $cadastrar = base64_encode('true');
        }catch (PDOException $e){
            $cadastrar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
    }

    public function editar($id)
    {
        try{
            if(session('dataFuncionario')->cargo == 'S'):
                $condominios = \App\Condominio::select('id','nome')->where('condominios.id',session('dataFuncionario')->condominio_id)->get();
            else:
                $condominios = \App\Condominio::select('id','nome')->get();
            endif;
            $multa = \App\Multa::select('multas.id','titulo','numero_documento','condominio_id','morador_id','multas.data_registro','data_pagamento','forma_pagamento','valor','motivo')->leftJoin('condominios','condominios.id','multas.condominio_id')->leftJoin('moradors','moradors.id','multas.morador_id')->where('multas.id',$id)->first();
            if(!empty($multa->data_registro)):
                $dataRegistroArr = explode('-',$multa->data_registro);
                $dataRegistro = "{$dataRegistroArr[2]}/{$dataRegistroArr[1]}/{$dataRegistroArr[0]}";
            else:
                $dataRegistro = null;
            endif;
            if(!empty($multa->data_pagamento)):
                $dataPagamentoArr = explode('-',$multa->data_pagamento);
                $dataPagamento = "{$dataPagamentoArr[2]}/{$dataPagamentoArr[1]}/{$dataPagamentoArr[0]}";
            else:
                $dataPagamento = null;
            endif;
            return ['condominios' => $condominios,'multa' => $multa,'dataRegistro' => $dataRegistro,'dataPagamento' => $dataPagamento];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function alterar(Request $request, $id)
    {
        $input = $request->all();
        $nomeBase = base64_encode($input['titulo']);
        try {
            if(!empty($input['dataRegistro'])):
                $dataRegistroArr = explode('/',$input['dataRegistro']);
                $dataRegistro = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";
            else:
                $dataRegistro = date('Y-m-d');
            endif;
            if(!empty($input['dataPagamento'])):
                $dataPagamentoArr = explode('/',$input['dataPagamento']);
                $dataPagamento = "{$dataPagamentoArr[2]}-{$dataPagamentoArr[1]}-{$dataPagamentoArr[0]}";
            else:
                $dataPagamento = null;
            endif;
            $multa = \App\Multa::find($id);
            $multa->id = $id;
            $multa->condominio_id = $input['condominio'];
            $multa->morador_id = $input['morador'];
            $multa->data_registro = $dataRegistro;
            $multa->data_pagamento = $dataPagamento;
            $multa->titulo = $input['titulo'];
            $multa->numero_documento = $input['numeroDocumento'];
            $multa->forma_pagamento = $input['formaPagamento'];
            $multa->valor = $input['valor'];
            $multa->motivo = $input['motivo'];
            $multa->save();

            $alterar = base64_encode('true');
        }catch (PDOException $e){
            $alterar = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'alterar' => $alterar];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $multa = \App\Multa::select('titulo')->where('id',$id)->first();
        $nomeBase = base64_encode($multa->titulo);
        try {
            \App\Multa::select('id')->where('id',$id)->delete();
            $excluir = base64_encode('true');
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
