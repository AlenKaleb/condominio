<?php

namespace App;

use App\Http\Controllers\Bloco\BlocoController;
use App\Http\Controllers\Lote\LoteController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Lote extends Model
{
    protected $fillable = ['id','funcionario_id','conjunto_id','lote','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'lotes';
    protected $primaryKey = 'id';

    public function funcionario(){
        return $this->hasOne('\App\Funcionario','funcionario_id');
    }

    public function conjunto(){
        return $this->belongsTo('\App\Conjunto','conjunto_id');
    }

    public function blocos(){
        return $this->hasMany('\App\Bloco','lote_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $lote = FILTER_INPUT(INPUT_GET,'lote',FILTER_DEFAULT);
            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
            $conjunto = FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT);
            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
            $lotes = \App\Lote::select('lotes.id','lotes.lote','lotes.conjunto_id','conjuntos.quadra_id','conjuntos.conjunto','quadras.quadra','quadras.condominio_id','condominios.nome')->leftJoin('conjuntos','lotes.conjunto_id','=','conjuntos.id')->leftJoin('quadras','quadras.id','=','conjuntos.quadra_id')->leftJoin('condominios','quadras.condominio_id','=','condominios.id')->where('lotes.lote','LIKE',"%{$lote}%")->where('conjuntos.conjunto','LIKE',"%{$conjunto}%")->where('quadras.quadra','LIKE',"%{$quadra}%")->where('condominios.nome','LIKE',"%{$condominio}%")->orderBy('lotes.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['lotes' => $lotes,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @return mixed
     */
    public function lotes(){
        $lote_id = (FILTER_INPUT(INPUT_GET,'lote',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'lote',FILTER_DEFAULT):null);
        $blocoCondominio = FILTER_INPUT(INPUT_GET,'blocoCondominio',FILTER_DEFAULT);
        $blocos = \App\Bloco::select('id','bloco')->where('lote_id',$lote_id)->get();
        $blocoArr[0] = '<option selected value=""> Selecione um bloco </option>';
        foreach ($blocos as $key => $bloco):
            $key++;
            if($blocoCondominio == $bloco->id):
                $blocoArr[$key] = '<option selected value="' . $bloco->id . '">' . $bloco->bloco . '</option>';
            else:
                $blocoArr[$key] = '<option value="' . $bloco->id . '">' . $bloco->bloco . '</option>';
            endif;
        endforeach;
        return $blocoArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $condominios = \App\Condominio::select('id','nome')->get();
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        if(is_array($input['lote'])):
            foreach($input['lote'] as $inputLote):
                if(!empty($inputLote)):
                    $lote = new Lote();
                    $lote->conjunto_id = $input['conjunto_id'];
                    $lote->lote = $inputLote;
                    $lote->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['lote']);
            try {
                $lote = new Lote();
                $lote->conjunto_id = $input['conjunto'];
                $lote->funcionario_id = $input['funcionario'];
                $lote->lote = $input['lote'];
                $lote->save();

                $request->request->add(['lote_id' => $lote->id]);
                $blocoController = new BlocoController();
                $blocoController->store($request);
                $cadastrar = base64_encode('true');
            }catch (PDOException $e){
                $cadastrar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
        endif;
    }

    public function editar($id)
    {
        try{
            $condominios = \App\Condominio::select('id','nome')->get();
            $lote = \App\Lote::select('lotes.id','lote','condominio_id','quadra_id','conjunto_id','lotes.funcionario_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('lotes.id',$id)->first();
            return ['condominios' => $condominios,'lote' => $lote];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lote  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id = null)
    {
        $input = $request->all();

        if(is_array($input['lote'])):
            \App\Lote::where('conjunto_id',$input['conjunto_id'])->delete();
            $this->cadastrar($request);
        else:
            $nomeBase = base64_encode($input['lote']);
            try {
                $lote = \App\Lote::find($id);
                $lote->id = $id;
                $lote->conjunto_id = $input['conjunto'];
                $lote->funcionario_id = $input['funcionario'];
                $lote->lote = $input['lote'];
                $lote->save();

                $request->request->add(['lote_id' => $lote->id]);
                $blocoController = new BlocoController();
                $blocoController->update($request);
                $alterar = base64_encode('true');
            }catch (PDOException $e){
                $alterar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'alterar' => $alterar];
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $lote = \App\Lote::select('lote')->where('id',$id)->first();
        $nomeBase = base64_encode($lote->lote);
        try {
            $blocos = \App\Bloco::where('lote_id',$id)->count();
            if($blocos == 0):
                \App\Lote::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }

}
