<?php

namespace App;

use App\Http\Controllers\Apartamento\ApartamentoController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PDOException;

class Bloco extends Model
{
    protected $fillable = ['id','funcionario_id','lote_id','bloco','created_at','updated_at'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'blocos';
    protected $primaryKey = 'id';

    public function lote(){
        return $this->belongsTo('\App\Lote','lote_id');
    }

    public function apartamentos(){
        return $this->hasMany('\App\Apartamento','bloco_id');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $bloco = FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT);
            $quadra = FILTER_INPUT(INPUT_GET,'quadra',FILTER_DEFAULT);
            $conjunto = FILTER_INPUT(INPUT_GET,'conjunto',FILTER_DEFAULT);
            $lote = FILTER_INPUT(INPUT_GET,'lote',FILTER_DEFAULT);
            $condominio = FILTER_INPUT(INPUT_GET,'condominio',FILTER_DEFAULT);
            $blocos = \App\Bloco::select('blocos.id','blocos.bloco','lotes.conjunto_id','conjuntos.quadra_id','conjuntos.conjunto','lotes.lote','quadras.quadra','quadras.condominio_id','condominios.nome')->leftJoin('lotes','lotes.id','=','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','=','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('blocos.bloco','LIKE',"%{$bloco}%")->where('conjuntos.conjunto','LIKE',"%{$conjunto}%")->where('lotes.lote','LIKE',"%{$lote}%")->where('quadras.quadra','LIKE',"%{$quadra}%")->where('condominios.nome','LIKE',"%{$condominio}%")->orderBy('blocos.id','desc')->paginate(10);
            $index = base64_encode('true');
            return ['blocos' => $blocos,'index' => $index];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * @return mixed
     */
    public function blocos(){
        $bloco_id = (FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT) != 0?FILTER_INPUT(INPUT_GET,'bloco',FILTER_DEFAULT):null);
        $apartamentoCondominio = FILTER_INPUT(INPUT_GET,'apartamentoCondominio',FILTER_DEFAULT);
        $apartamentos = \App\Apartamento::select('id','numero')->where('bloco_id',$bloco_id)->get();
        $apartamentoArr[0] = '<option selected value=""> Selecione um apartamento </option>';
        foreach ($apartamentos as $key => $apartamento):
            $key++;
            if($apartamentoCondominio == $apartamento->id):
                $apartamentoArr[$key] = '<option selected value="' . $apartamento->id . '">' . $apartamento->numero . '</option>';
            else:
                $apartamentoArr[$key] = '<option value="' . $apartamento->id . '">' . $apartamento->numero . '</option>';
            endif;
        endforeach;
        return $apartamentoArr;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function criar()
    {
        $condominios = \App\Condominio::select('id','nome')->get();
        return ['condominios' => $condominios];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cadastrar(Request $request)
    {
        $input = $request->all();
        if(is_array($input['bloco'])):
            foreach($input['bloco'] as $inputBloco):
                if(!empty($inputBloco)):
                    $bloco = new Bloco();
                    $bloco->lote_id = $input['lote_id'];
                    $bloco->bloco = $inputBloco;
                    $bloco->save();
                endif;
            endforeach;
        else:
            $nomeBase = base64_encode($input['bloco']);
            try {
                $bloco = new Bloco();
                $bloco->lote_id = $input['lote'];
                $bloco->funcionario_id = $input['funcionario'];
                $bloco->bloco = $input['bloco'];
                $bloco->save();

                $request->request->add(['bloco_id' => $bloco->id]);
                $apartamentoController = new ApartamentoController();
                $apartamentoController->store($request);
                $cadastrar = base64_encode('true');
            }catch (PDOException $e){
                $cadastrar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'cadastrar' => $cadastrar];
        endif;
    }

    public function editar($id)
    {
        try{
            $condominios = \App\Condominio::select('id','nome')->get();
            $bloco = \App\Bloco::select('blocos.id','bloco','condominio_id','quadra_id','conjunto_id','lote_id','blocos.funcionario_id')->leftJoin('lotes','lotes.id','=','blocos.lote_id')->leftJoin('conjuntos','conjuntos.id','lotes.conjunto_id')->leftJoin('quadras','quadras.id','=','conjuntos.quadra_id')->leftJoin('condominios','condominios.id','quadras.condominio_id')->where('blocos.id',$id)->first();
            return ['condominios' => $condominios,'bloco' => $bloco];
        }catch (PDOException $e){
            $index = base64_encode('false');
            return ['index' => $index];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bloco  $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request $request, $id = null)
    {
        $input = $request->all();

        if(is_array($input['bloco'])):
//            dd($input);
            \App\Bloco::where('lote_id',$input['lote_id'])->delete();
            $this->cadastrar($request);
        else:
            $nomeBase = base64_encode($input['bloco']);
            try {
                $bloco = \App\Bloco::find($id);
                $bloco->id = $id;
                $bloco->lote_id = $input['lote'];
                $bloco->funcionario_id = $input['funcionario'];
                $bloco->bloco = $input['bloco'];
                $bloco->save();

                $request->request->add(['bloco_id' => $bloco->id]);
                $apartamentoController = new ApartamentoController();
                $apartamentoController->update($request);
                $alterar = base64_encode('true');
            }catch (PDOException $e){
                $alterar = base64_encode('false');
            }
            return ['nomeC' => $nomeBase,'alterar' => $alterar];
        endif;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $bloco = \App\Bloco::select('bloco')->where('id',$id)->first();
        $nomeBase = base64_encode($bloco->bloco);
        try {
            $apartamentos = \App\Apartamento::where('bloco_id',$id)->count();
            if($apartamentos == 0):
                \App\Bloco::select('id')->where('id',$id)->delete();
                $excluir = base64_encode('true');
            else:
                $excluir = base64_encode('false');
            endif;
        }catch (PDOException $e){
            $excluir = base64_encode('false');
        }
        return ['nomeC' => $nomeBase,'excluir' => $excluir];
    }
}
