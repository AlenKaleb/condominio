<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Notifica;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

Route::get('/login', function () {
    return view('painel.login.login');
});

// Rota de Autenticação do usuário
Route::post('login',['as' => 'login','uses' => 'Auth\LoginController@authenticate']);

// Rota para verificação de email do usuário
Route::get('usuario/emailExist',['as' => 'usuario.emailExist','uses' => 'User\UserController@emailExist']);

/**
 * Rotas para usuários logados com permissão
 */
Route::group(['middleware' => 'auth'],function() {

    Route::get('/', function () {
        $notifica = new Notifica();
        $notificas = $notifica->index();
        return view('painel.index',compact('notificas'));
    });

    Route::get('/inicio', function () {
        $notifica = new Notifica();
        $notificas = $notifica->index();
        return view('painel.index',compact('notificas'));
    });

    Route::get('logout',['as' => '','uses' => 'Auth\LoginController@logout']);

    // Rotas do Manter Usuário
    Route::group(['prefix' => 'funcionario'],function(){
        Route::get('index',['as' => 'funcionario.index','uses' => 'Funcionario\FuncionarioController@index']);
        Route::get('perfilEdit',['as' => 'funcionario.perfilEdit','uses' => 'Funcionario\FuncionarioController@perfilEdit']);
        Route::get('perfil/{id}',['as' => 'funcionario.perfil','uses' => 'Funcionario\FuncionarioController@perfil']);
        Route::get('relatorio/{id}',['as' => 'funcionario.perfil','uses' => 'Funcionario\FuncionarioController@relatorio']);
        Route::get('verificarCpf',['as' => 'funcionario.verificarCpf','uses' => 'Funcionario\FuncionarioController@verificarCpf']);
        Route::get('validatePassword',['as' => 'funcionario.validatePassword','uses' => 'User\UserController@validatePassword']);
        Route::get('create',['as' => 'funcionario.create','uses' => 'Funcionario\FuncionarioController@create']);
        Route::get('edit/{id}',['as' => 'funcionario.edit','uses' => 'Funcionario\FuncionarioController@edit']);
        Route::post('store',['as' => '','uses' => 'Funcionario\FuncionarioController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Funcionario\FuncionarioController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Funcionario\FuncionarioController@destroy']);
    });

    // Rotas do Manter Condominio
    Route::group(['prefix' => 'condominio'],function(){
        Route::get('index',['as' => 'condominio.index','uses' => 'Condominio\CondominioController@index']);
        Route::get('verificarCnpj',['as' => 'condominio.verificarCnpj','uses' => 'Condominio\CondominioController@verificarCnpj']);
        Route::get('create',['as' => 'condominio.create','uses' => 'Condominio\CondominioController@create']);
        Route::get('edit/{id}',['as' => 'condominio.edit','uses' => 'Condominio\CondominioController@edit']);
        Route::post('store',['as' => '','uses' => 'Condominio\CondominioController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Condominio\CondominioController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Condominio\CondominioController@destroy']);

        Route::get('condominios',['as' => 'condominio.condominios','uses' => 'Condominio\CondominioController@condominios']);
        Route::get('condominioMoradores',['as' => 'condominio.condominioMoradores','uses' => 'Condominio\CondominioController@condominioMoradores']);
        Route::get('condominioFuncionarios',['as' => 'condominio.condominioFuncionarios','uses' => 'Condominio\CondominioController@condominioFuncionarios']);
        Route::get('quadras',['as' => 'quadra.quadras','uses' => 'Quadra\QuadraController@quadras']);
        Route::get('conjuntos',['as' => 'conjunto.conjuntos','uses' => 'Conjunto\ConjuntoController@conjuntos']);
        Route::get('lotes',['as' => 'lote.lotes','uses' => 'Lote\LoteController@lotes']);
        Route::get('blocos',['as' => 'bloco.blocos','uses' => 'Bloco\BlocoController@blocos']);
    });

    // Rotas do Manter Quadra
    Route::group(['prefix' => 'quadra'],function(){
        Route::get('index',['as' => 'quadra.index','uses' => 'Quadra\QuadraController@index']);
        Route::get('create',['as' => 'quadra.create','uses' => 'Quadra\QuadraController@create']);
        Route::get('edit/{id}',['as' => 'quadra.edit','uses' => 'Quadra\QuadraController@edit']);
        Route::post('store',['as' => '','uses' => 'Quadra\QuadraController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Quadra\QuadraController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Quadra\QuadraController@destroy']);
    });

    // Rotas do Manter Conjunto
    Route::group(['prefix' => 'conjunto'],function(){
        Route::get('index',['as' => 'conjunto.index','uses' => 'Conjunto\ConjuntoController@index']);
        Route::get('create',['as' => 'conjunto.create','uses' => 'Conjunto\ConjuntoController@create']);
        Route::get('edit/{id}',['as' => 'conjunto.edit','uses' => 'Conjunto\ConjuntoController@edit']);
        Route::post('store',['as' => '','uses' => 'Conjunto\ConjuntoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Conjunto\ConjuntoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Conjunto\ConjuntoController@destroy']);
    });

    // Rotas do Manter Lote
    Route::group(['prefix' => 'lote'],function(){
        Route::get('index',['as' => 'lote.index','uses' => 'Lote\LoteController@index']);
        Route::get('create',['as' => 'lote.create','uses' => 'Lote\LoteController@create']);
        Route::get('edit/{id}',['as' => 'lote.edit','uses' => 'Lote\LoteController@edit']);
        Route::post('store',['as' => '','uses' => 'Lote\LoteController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Lote\LoteController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Lote\LoteController@destroy']);
    });

    // Rotas do Manter Bloco
    Route::group(['prefix' => 'bloco'],function(){
        Route::get('index',['as' => 'bloco.index','uses' => 'Bloco\BlocoController@index']);
        Route::get('create',['as' => 'bloco.create','uses' => 'Bloco\BlocoController@create']);
        Route::get('edit/{id}',['as' => 'bloco.edit','uses' => 'Bloco\BlocoController@edit']);
        Route::post('store',['as' => '','uses' => 'Bloco\BlocoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Bloco\BlocoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Bloco\BlocoController@destroy']);
    });

    // Rotas do Manter Apartamento
    Route::group(['prefix' => 'apartamento'],function(){
        Route::get('index',['as' => 'apartamento.index','uses' => 'Apartamento\ApartamentoController@index']);
        Route::get('moradorExist',['as' => 'apartamento.moradorExist','uses' => 'Apartamento\ApartamentoController@moradorExist']);
        Route::get('create',['as' => 'apartamento.create','uses' => 'Apartamento\ApartamentoController@create']);
        Route::get('edit/{id}',['as' => 'apartamento.edit','uses' => 'Apartamento\ApartamentoController@edit']);
        Route::post('store',['as' => '','uses' => 'Apartamento\ApartamentoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Apartamento\ApartamentoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Apartamento\ApartamentoController@destroy']);
    });

    // Rotas do Manter Taxa
    Route::group(['prefix' => 'taxa'],function(){
        Route::get('index',['as' => 'taxa.index','uses' => 'Taxa\TaxaController@index']);
        Route::get('create',['as' => 'taxa.create','uses' => 'Taxa\TaxaController@create']);
        Route::get('edit/{id}',['as' => 'taxa.edit','uses' => 'Taxa\TaxaController@edit']);
        Route::post('store',['as' => '','uses' => 'Taxa\TaxaController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Taxa\TaxaController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Taxa\TaxaController@destroy']);
    });

    // Rotas do Manter Morador
    Route::group(['prefix' => 'morador'],function(){
        Route::get('index',['as' => 'morador.index','uses' => 'Morador\MoradorController@index']);
        Route::get('create',['as' => 'morador.create','uses' => 'Morador\MoradorController@create']);
        Route::get('edit/{id}',['as' => 'morador.edit','uses' => 'Morador\MoradorController@edit']);
        Route::post('store',['as' => '','uses' => 'Morador\MoradorController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Morador\MoradorController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Morador\MoradorController@destroy']);
        Route::get('verificarCpf',['as' => 'morador.verificarCpf','uses' => 'Morador\MoradorController@verificarCpf']);
    });

    // Rotas do Manter Recebimento
    Route::group(['prefix' => 'recebimento'],function(){
        Route::get('index',['as' => 'recebimento.index','uses' => 'Recebimento\RecebimentoController@index']);
        Route::get('create',['as' => 'recebimento.create','uses' => 'Recebimento\RecebimentoController@create']);
        Route::get('edit/{id}',['as' => 'recebimento.edit','uses' => 'Recebimento\RecebimentoController@edit']);
        Route::post('store',['as' => '','uses' => 'Recebimento\RecebimentoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Recebimento\RecebimentoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Recebimento\RecebimentoController@destroy']);
    });

    // Rotas do Manter Problema
    Route::group(['prefix' => 'problema'],function(){
        Route::get('index',['as' => 'problema.index','uses' => 'Problema\ProblemaController@index']);
        Route::get('create',['as' => 'problema.create','uses' => 'Problema\ProblemaController@create']);
        Route::get('edit/{id}',['as' => 'problema.edit','uses' => 'Problema\ProblemaController@edit']);
        Route::post('store',['as' => '','uses' => 'Problema\ProblemaController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Problema\ProblemaController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Problema\ProblemaController@destroy']);
    });

    // Rotas do Manter Multa
    Route::group(['prefix' => 'multa'],function(){
        Route::get('index',['as' => 'multa.index','uses' => 'Multa\MultaController@index']);
        Route::get('create',['as' => 'multa.create','uses' => 'Multa\MultaController@create']);
        Route::get('edit/{id}',['as' => 'multa.edit','uses' => 'Multa\MultaController@edit']);
        Route::post('store',['as' => '','uses' => 'Multa\MultaController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Multa\MultaController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Multa\MultaController@destroy']);
    });

    // Rotas do Manter Multa
    Route::group(['prefix' => 'custo'],function(){
        Route::get('index',['as' => 'custo.index','uses' => 'Custo\CustoController@index']);
        Route::get('create',['as' => 'custo.create','uses' => 'Custo\CustoController@create']);
        Route::get('edit/{id}',['as' => 'custo.edit','uses' => 'Custo\CustoController@edit']);
        Route::post('store',['as' => '','uses' => 'Custo\CustoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Custo\CustoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Custo\CustoController@destroy']);
    });

    // Rotas do Manter Visitante
    Route::group(['prefix' => 'visitante'],function(){
        Route::get('index',['as' => 'morador.index','uses' => 'Visitante\VisitanteController@index']);
        Route::get('create',['as' => 'morador.create','uses' => 'Visitante\VisitanteController@create']);
        Route::get('edit/{id}',['as' => 'morador.edit','uses' => 'Visitante\VisitanteController@edit']);
        Route::post('store',['as' => '','uses' => 'Visitante\VisitanteController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Visitante\VisitanteController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Visitante\VisitanteController@destroy']);
    });

    // Rotas do Manter Conjunto
    Route::group(['prefix' => 'notifica'],function(){
        Route::get('index',['as' => 'notifica.index','uses' => 'Notifica\NotificaController@index']);
        Route::get('create',['as' => 'notifica.create','uses' => 'Notifica\NotificaController@create']);
        Route::get('edit/{id}',['as' => 'notifica.edit','uses' => 'Notifica\NotificaController@edit']);
        Route::post('store',['as' => '','uses' => 'Notifica\NotificaController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'Notifica\NotificaController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'Notifica\NotificaController@destroy']);
    });

    // Rotas de Relatorio
    Route::group(['prefix' => 'relatorio'],function(){
        Route::get('financeiro',['as' => 'relatorio.financeiro','uses' => 'Relatorio\RelatorioController@financeiro']);
        Route::get('recebimento',['as' => 'relatorio.recebimento','uses' => 'Relatorio\RelatorioController@recebimento']);
    });

    // Rotas do Manter Cliente Fisico
    Route::group(['prefix' => 'endereco'],function(){
        Route::get('paises',['as' => 'fisico.paises','uses' => 'Pais\PaisController@paises']);
        Route::get('cidades',['as' => 'fisico.cidades','uses' => 'Cidade\CidadeController@cidades']);
    });

    // Rota para leitura de imagens
    Route::get('painel/{dir}/image/{filename}', function ($dir,$filename)
    {
        $path = storage_path() . "/app/$dir/$filename";

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
});
